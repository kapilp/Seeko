/**
 * Created by Sabiya on 09/11/16.
 */
(function(){
    angular.module('seekoApp.notification', [])
        .controller('notificationCtrl', ['$scope','$rootScope','facebookService','gmailService','$window','$cookies','$http','Alertify','base64','$state','shareData','deviceDetector','$filter','Notification',function($scope,$rootScope,facebookService,gmailService,$window,$cookies,$http,Alertify,base64,$state,shareData,deviceDetector,$filter,Notification) {
            $http.post('/api/notification',{id:shareData.getId()}).success(function(data){

                $scope.premiumClasses=data.notification.premiumClasses;
                $scope.newClasses=data.notification.newClasses;
                $scope.emailNotification=data.notification.emailNotification;
                $scope.reminder=data.notification.reminder;

                        if(data.notification.premiumClasses==false)

                        $scope.premiumClassesClass="switch switch-green";
                        else
                            $scope.premiumClassesClass="switch switch-green on off";




                        if(data.notification.newClasses==false)
                            $scope.newClassesClass="switch switch-green";
                        else
                            $scope.newClassesClass="switch switch-green on off";




                        if(data.notification.emailNotification==false)
                        $scope.emailNotificationClass="switch switch-green";
                    else
                        $scope.emailNotificationClass="switch switch-green on off";




                        if(data.notification.reminder==false)
                            $scope.reminderClass="switch switch-green";
                        else
                            $scope.reminderClass="switch switch-green on off";



            });

            $scope.classReminder=function(){
                $scope.reminder=!$scope.reminder;
                if($scope.reminder==false)
                    $scope.reminderClass="switch switch-green";
                else
                    $scope.reminderClass="switch switch-green on off";
                $http.post('/api/notificationUpdate',{id:shareData.getId(),notificationType:"reminder",notificationMode:$scope.reminder})
                    .success(function(data){

                    })

            };
        $scope.emailNotifications=function(){

            $scope.emailNotification=!$scope.emailNotification;
            if($scope.emailNotification==false)
                $scope.emailNotificationClass="switch switch-green";
            else
                $scope.emailNotificationClass="switch switch-green on off";
            $http.post('/api/notificationUpdate',{id:shareData.getId(),notificationType:"emailNotification",notificationMode:$scope.emailNotification})
                .success(function(data){

                })
           };
           $scope.newClassesReminder=function(){

               $scope.newClasses=!$scope.newClasses;
               if($scope.newClasses==false)
                   $scope.newClassesClass="switch switch-green";
               else
                   $scope.newClassesClass="switch switch-green on off";
               $http.post('/api/notificationUpdate',{id:shareData.getId(),notificationType:"newClasses",notificationMode:$scope.newClasses})
                   .success(function(data){

                   })
           };
            $scope.premiumClassesReminder=function(){

                $scope.premiumClasses=!$scope.premiumClasses;
                if($scope.premiumClasses==false)
                    $scope.premiumClassesClass="switch switch-green";
                else
                    $scope.premiumClassesClass="switch switch-green on off";
                $http.post('/api/notificationUpdate',{id:shareData.getId(),notificationType:"premiumClasses",notificationMode:$scope.premiumClasses})
                    .success(function(data){

                    })
            };


        }]);
}());


