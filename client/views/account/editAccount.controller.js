(function () {
    angular.module('seekoApp.editAccount', [])
        .controller('editAccountCtrl', ['$scope', '$rootScope', 'facebookService', 'gmailService', '$window', '$cookies', '$http', 'Alertify', 'base64', '$state', 'shareData', 'deviceDetector', '$filter', 'Notification', '$uibModal','localStorageService',
         function ($scope, $rootScope, facebookService, gmailService, $window, $cookies, $http, Alertify, base64, $state, shareData, deviceDetector, $filter, Notification, $uibModal, localStorageService) {
            var profile=shareData.getProfile();
           $scope.loginFromSeeko= localStorageService.get('loginFromSeeko');
            $scope.changeEmail={
                emailId:'',
                confirmEmail:''
            }
            $scope.changePassword={
                passwordId:'',
                confirmPassID:''
            }
            if(profile.isFbLogin == true){
                $scope.fbLoginClass = "switch switch-green on off";
                $scope.isFbLogin=true;
                $scope.fbLoginStatus=profile.isFbLogin;
            }
            else{
                $scope.fbLoginClass = "switch switch-green";
                $scope.isFbLogin=false;
                $scope.fbLoginStatus=false;
            }



            if(profile.isGoogleLogin == true){
                $scope.googleLoginClass = "switch switch-green on off";
                $scope.isGoogleLogin=true;
                $scope.googleLoginStatus=profile.isGoogLelogin;
            }
            else{
                $scope.googleLoginClass = "switch switch-green";
                $scope.isGoogleLogin=false;
                $scope.googleLoginStatus=false;
            }


            $scope.header = "Delete Account";

            $scope.deleteAccount = function () {
                $scope.message = "Are You Sure You Want To Delete This Account?";
                //Notification.primary({message:"Are You Sure You Want To Delete This Account",templateUrl: "../../views/messageNotifications/confirm.html",scope:$scope,delay:20000});
                var modalInstance = $uibModal.open({
                    templateUrl: '../../views/messageNotifications/confirm.html',
                    scope: $scope,
                    animation:true,
                    windowClass: 'modal modal-slide-in-right'
                });
                $scope.modalInstance = modalInstance;
            }

            $scope.confirm = function () {

                $http.post('/api/deleteAccount', {id: shareData.getId()}).success(
                    function (data) {
                        if (data.success) {
                       localStorageService.set('loginFromSeeko','');
                            $state.go('home');
                        }

                    }
                )
            };


            $scope.cancel = function () {
                $scope.modalInstance.close('ok');
            };


            $scope.Updatechange = function () {

                var data = {
                    "id": shareData.getId(),
                    "emailId": $scope.changeEmail.emailId
                }



                $http.post('/api/accountSetting', data)
                    .success(function (data) {
                        Alertify.success(data.message);


                    })
            }
            $scope.UpdatechangePass = function () {

                var data = {
                    "id": shareData.getId(),
                    "password": $scope.changePassword.passwordId
                }


                $http.post('/api/accountSetting', data)
                    .success(function (data) {

                        Alertify.success(data.message);


                    })
            }

            // $scope.fbLogin = function () {
            //
            //
            //     $scope.isFbLogin = !$scope.isFbLogin;
            //     $scope.fbLoginStatus=$scope.isFbLogin;
            //     if ($scope.isFbLogin == true){
            //         var loginCredentials = '';
            //         FB.login(function (response) {
            //             if (response.authResponse) {
            //                 var access_token = FB.getAuthResponse()['accessToken'];
            //
            //                 FB.api('/me', 'get', {fields: 'id,name,first_name,last_name,gender,email,picture'}, function (response) {
            //
            //
            //                     $scope.loginCredentials = {
            //                         'name': response.name,
            //                         'firstName': response.first_name,
            //                         'lastName': response.last_name,
            //                         'picture': response.picture,
            //                         'email': response.email,
            //                         'id': response.id,
            //                         'token': access_token,
            //                         'userType': $rootScope.teacher
            //                     };
            //
            //
            //                     $http.post('/fbLogin', $scope.loginCredentials)
            //                         .success(function (data) {
            //                             //  $scope.fbLoginClass = "switch switch-green";
            //                             $scope.fbLoginClass = "switch switch-green on off";
            //
            //                         });
            //
            //                 });
            //
            //
            //             } else {
            //                 console.log('User cancelled login or did not fully authorize.');
            //             }
            //         }, {scope: 'public_profile,email'});
            //     }
            //     else {
            //         // $scope.fbLoginClass = "switch switch-green on off";
            //         $scope.fbLoginClass = "switch switch-green";
            //     }
            // };
            //
            //
            // $scope.googleLogin = function () {
            //
            //     $scope.isGoogleLogin = !$scope.isGoogleLogin;
            //     $scope.googleLoginStatus=$scope.isGoogleLogin;
            //     if ($scope.isGoogleLogin == true){
            //         gmailService.gmailData().then(function (response) {
            //             $http.post('/googleLogin', response)
            //                 .success(function (data) {
            //
            //
            //                     $scope.googleLoginClass = "switch switch-green on off";
            //                 });
            //         })
            //     }
            //     else{
            //
            //
            //
            //         $scope.googleLoginClass = "switch switch-green";
            //     }
            //
            //
            // }


            $scope.googleLoginOn = function () {
            $scope.$on('event:google-plus-signin-success', function (event, authResult) {
                    $scope.googleLoginClass = "switch switch-green on off";
                    $scope.isGoogleLogin = true;
                    $scope.googleLoginStatus=$scope.isGoogleLogin;
                    if( $scope.isFbLogin==false &&  $scope.isGoogleLogin==false){
                        $scope.googleLoginStatus=$scope.isGoogleLogin;
                    }
                    else {
                        var googleData={
                            'id': shareData.getId(),
                            'googleStatusValue': $scope.isGoogleLogin
                        }
                        $http.post('/api/googleConnection', googleData).success(
                            function (data) {
                                if (data.success) {
                                    console.log("i will be true");
                                }
                                else {
                                    console.log("i will be false");
                                }

                            }
                        )
                    }

                });
            }

            $scope.googleLoginOff = function () {

             if( $scope.isFbLogin==false ){
                    $scope.googleLoginStatus=false;
                }else{
                 $scope.isGoogleLogin = false;
                 var googleData={
                     'id': shareData.getId(),
                     'googleStatusValue': $scope.isGoogleLogin
                 }
                 $http.post('/api/googleConnection', googleData).success(
                     function (data) {
                         if (data.success) {
                             console.log("i will be true");
                         }
                         else {
                             console.log("i will be false");
                         }

                     }
                 )
             }

                $scope.googleLoginClass = "switch switch-green";

            }

            $scope.fbLogin = function () {
                $scope.isFbLogin = !$scope.isFbLogin;

                if( $scope.isFbLogin==false &&  $scope.isGoogleLogin==false){
                    $scope.fbLoginStatus=$scope.isFbLogin;
                }

                else {
                    var fbData = {
                        'id': shareData.getId(),
                        'fbStatusValue': $scope.isFbLogin
                    }
                    if ($scope.isFbLogin) {
                        FB.login(function (response) {
                            if (response.authResponse) {
                                $scope.fbLoginClass = "switch switch-green on off";
                            } else {
                                console.log('User cancelled login or did not fully authorize.');
                            }
                        });
                    }
                    else {
                        $scope.fbLoginClass = "switch switch-green";
                        console.log("facebook logout working");
                    }

                    $http.post('/api/facebookConnection', fbData).success(
                        function (data) {
                            if (data.success) {
                                console.log("i will be true");
                            }
                            else {
                                console.log("i will be false");
                            }

                        }
                    )
                }
            }








        }]);
}());