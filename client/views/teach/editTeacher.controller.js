(function(){
    angular.module('seekoApp.editTeacher', ['ui.router','ui.bootstrap'])

    // .config(['$routeProvider', function($routeProvider) {
    //     $routeProvider.when('/login', {
    //         templateUrl: 'login/login.html',
    //         controller: 'loginCtrl'
    //     });
    // }])

        .controller('editTeacherCtrl', ['$scope','$uibModal','$state','shareData','$rootScope','Upload','$http','shareData','Alertify','Notification','$anchorScroll','$document','$location',function($scope,$uibModal,$state,shareData,$rootScope,Upload,$http,shareData,Alertify,Notification,$anchorScroll,$document,$location) {
            console.log("inside editTeacher controller");
            // $document.on('click',function(){
            //     console.log("inside body click");
            //     $scope.user.expYearSelect=false;
            //     $scope.user.monthSelect=false;
            //     $scope.user.daySelect=false;
            //     $scope.user.yearSelect=false;
            //     user.expMonthSelect=false;
            //     $scope.$apply();
            // });
            if ($location.hash() !== 'header') {
                $location.hash('header');
            } else {
                $anchorScroll();
            }
            $scope.isLoaded=false;
            $scope.dedecodedUri=[];
            $scope.credentialUrl=[];
            $scope.year='year';
            $scope.experienceYears=['yy','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20'];
            $scope.experienceMonths=['mm','0','1','2','3','4','5','6','7','8','9','10','11'];
            $scope.years=['yyyy','2007','2006','2005','2004','2003','2002','2001','2000','1999','1998','1997','1996','1995','1994','1993','1992','1991','1990','1989','1988','1987','1986','1985','1984','1983','1982','1981','1980','1979','1978','1977','1976','1975','1974','1973','1972','1971','1970','1969','1968','1967','1966','1965','1964','1963','1962','1961','1960','1959','1958','1957','1956','1955','1954','1953','1952','1951','1950','1949','1948','1947','1946','1945','1944','1943','1942','1941','1940','1939','1938','1937','1936','1935','1934','1933','1932','1931','1930','1929','1928','1927','1926','1925','1924','1923','1922','1921','1920','1919','1918','1917','1916','1915','1914','1913','1912','1911','1910','1909','1908','1907','1906','1905','1904','1903','1902','1901','1900'];
            $scope.months=['mm','January','February','March','April','May','June','July','August','September','October','November','December'];
            $scope.days=['dd','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
            $scope.showspin=false;
            var profile=shareData.getProfile();
            if(profile.isGoogleLogin==true || profile.isFbLogin==true)
            {
                $scope.uploadImage=false;
            }
            else
                $scope.uploadImage=true;

            $scope.firstName=profile.firstName;

            $scope.lastName=profile.lastName;

            $scope.location=profile.location;

            $scope.aboutMe=profile.aboutMe;
            $scope.description=profile.description;

      console.log(profile);
      if(profile.teacherProfile.credential.length > 0) {
    console.log("inside if");
    for(var i=0;i<profile.teacherProfile.credential.length;i++){
        console.log("inside loop");
        var url = profile.teacherProfile.credential[i].credential;
        $scope.credentialUrl.push(profile.teacherProfile.credential[i].originalFileName);

        var decodedUrl = decodeURIComponent(url);
        $scope.dedecodedUri.push(decodedUrl);

    }

    console.log($scope.credentialUrl);
}
else {
    $scope.credentialUrl="Not Mentioned";
}

            if(profile.teacherProfile.experience!= undefined && profile.teacherProfile.experience!=="0/0"){
                $scope.userExpYear=profile.teacherProfile.experience.split('/')[0];
                $scope.userExpMonth=profile.teacherProfile.experience.split('/')[1];
            }
            else {
                $scope.userExpYear='yy';
                $scope.userExpMonth='mm';
            }
            var userDate=new Date(profile.dob);


                if (profile.dob == undefined) {
                    $scope.userYear="yyyy";
                    $scope.userMonth="mm";

                    $scope.userDay="dd";
                }
                else {
                    $scope.userYear=userDate.getFullYear().toString();
                    $scope.userMonth=$scope.months[userDate.getMonth() + 1].toString();

                    $scope.userDay=userDate.getDate().toString();
                    console.log(typeof $scope.userYear);
                }






            if(profile.gender== undefined)
            {
                $scope.gender='Male';
            }
            else
            {
                $scope.gender=profile.gender;
            }


            $scope.getMyProfile=function(){
                //$state.go('profile',{id:shareData.getId()});
                var url = $state.href('profile',{id:shareData.getId()});
                window.open(url,'_blank');
            }
            $scope.updateProfile=function()
            {
                if ($scope.userForm.$valid) {
                    $scope.showspin=true;
                    if ($location.hash() !== 'header') {
                        $location.hash('header');
                    } else {
                        $anchorScroll();
                    }
                    console.log($scope.user);
                    console.log($scope.user.year + "," + $scope.user.month + "," + $scope.user.day);
                    if ($scope.user.year == undefined)
                        $scope.user.year = $scope.userYear;
                    if ($scope.user.month == undefined)
                        $scope.user.month = $scope.userMonth;
                    if ($scope.user.day == undefined)
                        $scope.user.day = $scope.$scope.userDay;
                    $scope.user.date = new Date($scope.user.year + "," + $scope.user.month + "," + $scope.user.day);
                    $scope.user.email = shareData.getUserProfile().email;


                    if ($scope.user.experienceYear == 'yy') {
                        $scope.user.experienceYear = 0;
                    }
                    if ($scope.user.experienceMonth == 'mm') {
                        $scope.user.experienceMonth = 0;
                    }
                    if ($scope.user.experienceYear == undefined) {
                        $scope.user.experienceYear = $scope.userExpYear;
                    }
                    if ($scope.user.experienceMonth == undefined) {
                        $scope.user.experienceMonth = $scope.userExpMonth;
                    }
                    $rootScope.user = $scope.user;
                    $rootScope.user.userType = true;
                    console.log($rootScope.user);
                    var image = $scope.user.image;
                    var doc = $scope.user.doc;
                    //console.log(image);


                    Upload.upload({url: '/api/teacherUpload', data: {image: image, doc: doc}}).success(function (data) {

                        console.log(data);
                        if (data.success = true) {
                            //check when profilePic or credential are available.
                            if (data.profilePic || data.credential) {
                                if (data.profilePic) {
                                    $rootScope.user.profilePic = data.profilePic;
                                }
                                if (data.credential) {
                                    $rootScope.user.credential = data.credential;
                                }


                                console.log("editTeacher userData" + $rootScope.user);
                                $http.post('/api/editProfile', $rootScope.user)
                                    .success(function (data) {

                                        if (data.success == true) {
                                            $scope.showspin=false;

                                            Notification.primary({
                                                message: "INFORMATION SAVED!",
                                                templateUrl: "../../views/messageNotifications/success.html"
                                            });
                                            $state.go('teacherDashboard');
                                        }
                                    })
                            }
                            else {
                                console.log("else");
                                $http.post('/api/editProfile', $rootScope.user)
                                    .success(function (data) {

                                        if (data.success == true) {
                                            $scope.isLoaded = false;

                                            Notification.primary({
                                                message: "INFORMATION SAVED!",
                                                templateUrl: "../../views/messageNotifications/success.html"
                                            });
                                            $state.go('teacherDashboard');
                                        }
                                    })
                            }
                        }


                    });
                }
                else{
                    $scope.userForm.submitted=true;
                    if ($location.hash() !== 'header') {
                        $location.hash('header');
                    } else {
                        $anchorScroll();
                    }

                }
            }

        }]);
}());
