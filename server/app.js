var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser=require('cookie-parser');
var mongoose=require('mongoose');

var passport = require('passport');
var flash    = require('connect-flash');
var cluster = require('cluster');

var session = require('express-session');

var methodOverride = require('method-override');
var urlencode = require('urlencode');
var fs = require('fs');

var app = express();
var https = require('https');
var http = require('http');
var numCPUs = 4;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

app.use(logger('dev'));

app.use(cookieParser()); // read cookies (needed for auth)


var options =
{
    key:  fs.readFileSync(__dirname+'/keys/seeko.key'),
    cert: fs.readFileSync(__dirname+'/keys/seeko.crt')
};


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(bodyParser.json());

app.use(cookieParser());

app.use(session({
    secret: 'Seeko_Cookie',
    resave: false,
    saveUninitialized: true,
    cookie: {
        httpOnly: true,
        secure: true,
        expires:5 * 60 * 1000
    }
}));

// The master's job to spawn workers initially and when they die
// if (cluster.isMaster) {
// // Get the number of processor cores
// var cpuCount = require('os').cpus().length;
// console.log("cpuCount"+cpuCount);
// // Create a worker for each CPU
// for (var i = 0; i < cpuCount; i += 1)
//   {
//     cluster.fork();
//   } // When a worker exits, fork a new one
//    cluster.on('exit', function(worker)
//    {
//      console.log('Worker %d died', worker.id);
//      cluster.fork();
//    });
//    } else if (cluster.isWorker) { // The real work here for the worker
//      http.createServer(function (request, response)
//      {
//       console.log('Ready to accept request'); // This will cause the server to crash
//       // var i = n.start;
//       response.writeHead(200,
//         { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin' : '*' });
//         response.end('Hello World\n'); }).listen(3000); // Exit the process when there is an uncaught exception
//         console.log('Worker %d running!', cluster.worker.id);
//         process.on('uncaughtException', function() {
//           console.log('Handled the exception here');
//            process.exit(1);
//          });
//        }



app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// var passRoutes=require('../client/config/passport')(passport); // pass passport for configuration
// mongoose.connect('mongodb://localhost:27017/seeko');
mongoose.connect('mongodb://prabhakar:password@54.70.107.91:27017/seeko');
var port = (process.env.PORT || '443');
app.set('port', port);

// if (cluster.isMaster) {
//     for (var i = 0; i < numCPUs; i++) {
//         cluster.fork();
//     }
// } else {
//    server =  http.createServer(app);
//    server.listen(port);
// }
// server.listen(port);

// var server = https.createServer(options, app).listen(port, function() {
//     console.log('Kurento Tutorial started');
//     // console.log('Open ' + url.format(asUrl) + ' with a WebRTC capable browser');
// });
var server = https.createServer(options, app).listen(port, function() {
    console.log('Seeko started');
});
 http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
}).listen(80);

var io = require('socket.io')(server);


app.use(express.static(path.join(__dirname, '../client')));

var routes = require('./routes/route')();
var mainRoute = require('./routes/mainRoute')();
var callingRoutes = require('./routes/callingRoutes')(server);
var chattingRoutes = require('./routes/chatRoutes')(io,server);

url = (path.join(__dirname, '../client'));;

app.use('/', express.static(url, { redirect: false }));

app.get('/*', function (req, res, next) {
    string = req.url
    if(string.indexOf("/api/") !== -1 ){
        next();
    }
    else {
        res.sendFile(path.resolve(url+"/index.html"));
    }
});
app.use('/api', routes);
app.use('/api', chattingRoutes);
app.use('/api', mainRoute);
app.use('/api', callingRoutes);

module.exports = app;
// module.exports.server=server;
// module.exports.io=io;