angular.module('confirmPasswordDirective',[])

.directive("passwordVerify", function() {
    return {
        require: "ngModel",
        scope: {
            passwordVerify: '='
        },
        link: function(scope, element, attrs, signUpCtrl) {
            scope.$watch(function() {
                var combined;

                if (scope.passwordVerify || signUpCtrl.$viewValue) {
                    combined = scope.passwordVerify + '_' + signUpCtrl.$viewValue;
                }
                return combined;
            }, function(value) {
                if (value) {
                    signUpCtrl.$parsers.unshift(function(viewValue) {
                        var origin = scope.passwordVerify;
                        if (origin !== viewValue) {
                            signUpCtrl.$setValidity("passwordVerify", false);
                            return undefined;
                        } else {
                            signUpCtrl.$setValidity("passwordVerify", true);
                            return viewValue;
                        }
                    });
                }
            });
        }
    };


});