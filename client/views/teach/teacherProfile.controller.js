(function(){
    angular.module('seekoApp.teacherProfile', ['ui.router','ui.bootstrap'])

    // .config(['$routeProvider', function($routeProvider) {
    //     $routeProvider.when('/login', {
    //         templateUrl: 'login/login.html',
    //         controller: 'loginCtrl'
    //     });
    // }])

        .controller('teacherProfileCtrl', ['$scope','$uibModal','$state','$rootScope','$cookies','base64','$state','deviceDetector','$http','shareData','$window','localStorageService','Notification','$location','$anchorScroll','screenSize','$stateParams',function($scope,$uibModal,$state,$rootScope,$cookies,base64,$state,deviceDetector,$http,shareData,$window,localStorageService,Notification,$location,$anchorScroll,screenSize,$stateParams) {
            console.log("inside teacerProfileCtrl");
            if($stateParams.id==''){
                if(!shareData.getCookies())
                    $state.go('home');
                else {
                    if(shareData.getProfile().userType==true)
                        $state.go('teacherDashboard');
                    else
                        $state.go('dashboard');
                }
            }
            $scope.showspin=true;
            // $rootScope.viewAllData='';

            $scope.currentUserDetail=shareData.getUserProfile();
            $rootScope.viewAllData='';
            $scope.postReviewButton=false;
            // if ($location.hash() !== 'header') {
            //     $location.hash('header');
            // } else {
            //     $anchorScroll();
            // }
            var  profile={};
            var email='';
            var id='';
            var teacherId='';
            screenSize.rules = {
                superJumbo: '(max-width: 640px)'

            };
            if(screenSize.is('superJumbo')){
                console.log("inside super jumbo");
            }
            if(screenSize.is('superJumbo')){
                var startTeacherIndex=0;
                var endTeacherIndex=4;
            }
            else {
                var startTeacherIndex=0;
                var endTeacherIndex=6;
            }

            var starCount=0;
            // $scope.isLogin=$state.current.data.isLogin.isLogin;
            if(shareData.getCookies()){
            $scope.user_name=shareData.getProfile().firstName;
                $scope.userProfilePic=shareData.getProfile().profilePic;
            }
            $scope.teacherReview={
                review:''
            }
            $scope.starArray=[{like:false},{like:false},{like:false},{like:false},{like:false}];
            $scope.stars=$scope.starArray;

            $scope.months=['month','January','February','March','April','May','June','July','August','September','October','November','December'];
            id=$stateParams.id;
            if(localStorageService.get('profile')=='profile')
            {
                console.log("inside if");
                $scope.favouriteButton=false;

            }
            else
            {
                console.log("inside else");

                $scope.favouriteButton=true;

           }
            $http.post('/api/viewEclasses', {id: id,userId:shareData.getId()}).success(function (data) {
                // console.log(JSON.stringify(data.profile));
                if(data.success==false){
                    if(!shareData.getCookies())
                        $state.go('home');
                    else {
                        if(shareData.getProfile().userType==true)
                            $state.go('teacherDashboard');
                        else
                            $state.go('dashboard');
                    }
                }
                if(data.isFavourite==true)
                    $scope.favouriteText="Added to favourite";
                else
                    $scope.favouriteText="Make Favourite";
                teacherId=data.profile._id;
                $scope.userType=data.profile.userData.userType;
                profile= data.profile.userData.profile;
                if(data.profile.userData.avgStarCount==undefined)
                $scope.starCountBefore=0;
                else
                    $scope.starCountBefore=data.profile.userData.avgStarCount;
                       var strNum=""+$scope.starCountBefore;
                         if(strNum!='0'){
                        var index=strNum.indexOf('.');
                        if(index<0){
                            strNum=strNum+'.0';
                           $scope.starCountBefore=strNum;
                        }
                      }
                       $scope.starCount= $scope.starCountBefore;
                $scope.reviews = data.profile.userData.reviews.reverse();
                console.log($scope.reviews);
                $scope.allClasses=data.classes.reverse();
                // $rootScope.viewAllData= $scope.allClasses;
                localStorageService.set('stateReview', $scope.allClasses);

                $scope.classes=$scope.allClasses.slice(startTeacherIndex, endTeacherIndex);
                 console.log(JSON.stringify(profile));
                 $scope.dedecodedUri=[];
                 $scope.credentialUrl=[];
                console.log(profile.teacherProfile);
                 if(profile.teacherProfile.credential.length > 0) {
                     console.log("inside if");
                     for(var i=0;i<profile.teacherProfile.credential.length;i++){
                         console.log("inside loop");
                         var url = profile.teacherProfile.credential[i].credential;
                         $scope.credentialUrl.push(profile.teacherProfile.credential[i].originalFileName);
                         var decodedUrl = decodeURIComponent(url);
                         $scope.dedecodedUri.push(decodedUrl);

                     }

                     console.log($scope.credentialUrl);
                 }
                 else {
                     $scope.credentialUrl="Not Mentioned";
                 }

                 if(shareData.getCookies()) {
                     console.log("cookies exist");
                     if (teacherId == shareData.getId()) {
                         console.log("inside teacherid==shareData.getId()");
                         $scope.isMyProfile = true;
                     }
                     else {
                         console.log("inside teacherid!=shareData.getId()");
                         if($scope.userType){
                             console.log("is teacher");
                         $scope.isMyProfile = false;}
                         else{
                             console.log("is student");
                             $scope.isMyProfile=true;}

                     }
                 }
                 else{
                     console.log("cookies expired");
                     $scope.isMyProfile = true;}


                 if(profile.location!= undefined)
                     $scope.location=profile.location;
                     else
                         $scope.location="not mentioned";

                if(profile.firstName!= undefined  && profile.lastName!= undefined)
                     $scope.name=profile.firstName+" "+profile.lastName;
                else if(profile.lastName== undefined)
                     $scope.name=profile.firstName;

                 if(profile.gender!=undefined)
                     $scope.gender=profile.gender;
                 else
                     $scope.gender="not mentioned";

                 if(profile.dob!=undefined){
                     userDate=new Date(profile.dob);
                     $scope.dob=userDate.getDate()+" "+$scope.months[userDate.getMonth() + 1]+" "+userDate.getFullYear();}
                     else
                 {
                     $scope.dob="not mentioned";
                 }

                 if(profile.aboutMe!= undefined)
                     $scope.aboutMe=profile.aboutMe;
                 else
                     $scope.aboutMe="not mentioned";


                 if(profile.teacherProfile.experience!=undefined)
                     $scope.experience=profile.teacherProfile.experience.split('/')[0]+"."+profile.teacherProfile.experience.split('/')[1]+" years";
                 else
                     $scope.experience="not mentioned";
                 $scope.profilePic=profile.profilePic;
                $scope.showspin=false;
            });
            $scope.getCredential=function(value)
            {

                // $http.post('/api/download',{url:$scope.dedecodedUri}).success(function(data){
                //     console.log(data);
                // });
                $window.open(value)
            }

            $scope.makeTeacherFavourite=function(){
                $http.post("/api/createFavourites",{id:shareData.getId(),classId:'',teacherId:teacherId})
                    .success(function (data) {
                        console.log("inside data");
                        $scope.favouriteText="Added to favourite";
                        Notification.primary({message:"Jedi is added as your favourite teacher.",templateUrl: "../../views/messageNotifications/success.html"});
                    })
            }

            $scope.postReview=function() {

                console.log($scope.teacherReview.review.length);
                if($scope.teacherReview.review.length>0){
                    $scope.postReviewButton=true;
                $http.post('/api/teacherReviews', {
                    "teacherId": teacherId,
                    "teacherName": $scope.name,
                    "userId": shareData.getId(),
                    "comment": $scope.teacherReview.review,
                    "date": new Date(),
                    "starCount": starCount,
                    "userName": shareData.getProfile().firstName,
                    "profilePic": shareData.getProfile().profilePic,
                    "userType": shareData.getProfile().userType
                })
                    .success(function (data) {
                        console.log("data is " + data);
                        for (i = 0; i < $scope.reviews.length; i++) {
                            if ($scope.reviews[i].userId == shareData.getId()) {
                                $scope.reviews[i].count = data.count;
                            }
                        }

                        var review = {
                            starCount: starCount,
                            comment: $scope.teacherReview.review,
                            date: new Date(),
                            profilePic: shareData.getProfile().profilePic,
                            userName: shareData.getProfile().firstName,
                            count: data.count,
                            userId: shareData.getId()
                        }
                        console.log(review);
                        $scope.teacherReview.review = "";
                        $scope.starArray = [{like: false}, {like: false}, {like: false}, {like: false}, {like: false}];
                        $scope.stars = $scope.starArray;
                        $scope.reviews.splice(0, 0, review);
                        starCount = 0;
                        $scope.postReviewButton=false;

                    });
            }

            }

            $scope.getUserReviews=function(review)
            {

                (function(review) {
                    console.log("id is "+review.userId);
                    $http.post('/api/getUserReviews',{id:review.userId}).success(function(data){
                        count=data.count;
                        review.count=count;
                    });

                })(review);

            }

            $scope.getNextClasses=function(type)
            {
                    if(($scope.allClasses.length>endTeacherIndex) && ($scope.allClasses.length>5) || ($scope.allClasses.length>endTeacherIndex) && ($scope.allClasses.length>3)) {
                        if(screenSize.is('superJumbo')) {
                            startTeacherIndex += 4;
                            endTeacherIndex += 4;
                        }
                        else{
                                startTeacherIndex += 6;
                                endTeacherIndex += 6;
                        }

                        $scope.classes=$scope.allClasses.slice(startTeacherIndex, endTeacherIndex);
                    }
            }

            $scope.getPrevClasses=function(type) {

                    if (startTeacherIndex > 0) {
                        if(screenSize.is('superJumbo')) {
                            startTeacherIndex -= 4;
                            endTeacherIndex -= 4;
                        }
                        else{
                            startTeacherIndex -= 6;
                            endTeacherIndex -= 6;
                        }

                        $scope.classes=$scope.allClasses.slice(startTeacherIndex, endTeacherIndex);

                    }

            }
            $scope.getStarCount=function(count){
                console.log(count);
                starCount=0;
                for(i=0;i<count+1;i++){
                    $scope.starArray[i].like=true;
                    starCount++;
                }
                for(i=count+1;i<6;i++)
                    $scope.starArray[i].like=false;
                console.log(JSON.stringify($scope.starArray));
                $scope.stars=$scope.starArray;
                console.log(JSON.stringify($scope.stars));
            }
            $scope.getClassDetail=function(value){
                console.log(value);
                var request={"label":value.classData.className,"value":value._id,"teacherId":value.classData.teacherId};

                localStorageService.set('searchString',request);
                localStorageService.set('searchFrom', 'searchBox');
                // $rootScope.searchString=result.name;
                // if($state.current.data.isLogin.isLogin) {
                //     if (shareData.getProfile().userType == false) {
                //         $state.go('dashboard.classDetails', {}, {reload: "dashboard.classDetails"});
                //     }
                //     else {
                //         $state.go('teacherDashboard.classDetails', {}, {reload: "teacherDashboard.classDetails"});
                //     }
                // }
                // else
                //     $state.go('home.classDetails', {}, {reload: "home.classDetails"});

                     $state.go('classDetails', {id:value._id});

            }

            $scope.changeState = function (value) {
                var request={"label":value.classData.className,"value":value._id,"teacherId":value.classData.teacherId};
                console.log('request');
                localStorageService.set('searchString',request);
                localStorageService.set('searchFrom', 'searchBox');

                $state.go('classDetails',{id:value._id});


            }

            $scope.viewAllClasses=function () {
                    $state.go('viewall');
            }

            $scope.changeCssOnHover=function () {
                $scope.textClr= {color:'#479fcd', 'text-decoration': 'underline'};
                $scope.underline={};
            }
            $scope.changeCssOnLeave=function () {
                $scope.textClr={color:' #808080'};
            }

            $scope.getReviewedUser=function(id,userType){
                $state.go('profile',{id:id});
            }
            //     console.log(userType);
            //     console.log("inside get Reviewed user "+id);
            //     if(shareData.getCookies()) {
            //         if (shareData.getId() != id)
            //             localStorageService.set('profile', id);
            //         else
            //             localStorageService.set('profile', 'profile');
            //         if (shareData.getProfile().userType == true) {
            //             console.log("inside if");
            //             console.log(userType);
            //             if (userType)
            //                 $state.go('teacherDashboard.profile', {}, {reload: 'teacherDashboard.profile'});
            //             else
            //                 $state.go('teacherDashboard.studentProfile');
            //         }
            //
            //         else {
            //             console.log("inside else");
            //             console.log(userType == true);
            //             if (userType == true) {
            //                 console.log(userType);
            //                 $state.go('dashboard.profile');
            //             }
            //
            //             else
            //                 $state.go('dashboard.studentProfile');
            //         }
            //
            //     }
            //     else
            //         localStorageService.set('profile', id);
            //     $state.go('home.profile',{},{reload:'home.profile'});
            //
            // }

        }]);
}());
