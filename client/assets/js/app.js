angular.module('seekoApp.controller', ['angular-svg-round-progressbar', 'angular-accordion', 'angularjs-datetime-picker', 'uiSlider', 'ngSanitize', 'ngDropdowns', 'file-model', 'widget.scrollbar','500tech.simple-calendar','duScroll']).controller('progressCtrl', ['$scope', '$interval', '$timeout', '$window', 'roundProgressService',
function($scope, $interval, $timeout, $window, roundProgressService) {

	$scope.current = 67;
	$scope.max = 100;
	$scope.offset = 0;
	$scope.timerCurrent = 0;
	$scope.uploadCurrent = 0;
	$scope.stroke = 15;
	$scope.radius = 125;
	$scope.isSemi = false;
	$scope.rounded = false;
	$scope.responsive = false;
	$scope.clockwise = true;
	$scope.currentColor = '#45ccce';
	$scope.bgColor = '#eaeaea';
	$scope.duration = 800;
	$scope.currentAnimation = 'easeOutCubic';
	$scope.animationDelay = 0;
	$scope.getStyle = function() {
		var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

		return {
			'top' : $scope.isSemi ? 'auto' : '50%',
			'bottom' : $scope.isSemi ? '5%' : 'auto',
			'left' : '50%',
			'transform' : transform,
			'-moz-transform' : transform,
			'-webkit-transform' : transform,
			'font-size' : $scope.radius / 3.5 + 'px'

		};
	};
	$scope.getColor = function() {
		return $scope.gradient ? 'url(#gradient)' : $scope.currentColor;
	};
	$scope.showPreciseCurrent = function(amount) {
		$timeout(function() {
			if (amount <= 0) {
				$scope.preciseCurrent = $scope.current;
			} else {
				var math = $window.Math;
				$scope.preciseCurrent = math.min(math.round(amount), $scope.max);
			}
		});
	};
}]).controller('TabsCtrl', ['$scope',
function($scope) {
	$scope.tabs = [{
		title : 'Categories',
		url : 'one.tpl.html'
	}, {
		title : 'Featured',
		url : 'two.tpl.html'
	}, {
		title : 'Locations',
		url : 'three.tpl.html'
	}];

	$scope.currentTab = 'one.tpl.html';

	$scope.onClickTab = function(tab) {
		$scope.currentTab = tab.url;
	}

	$scope.isActiveTab = function(tabUrl) {
		return tabUrl == $scope.currentTab;
	}
}]).controller('accordionBehaviorController', ['$scope',
function($scope) {
	var enabled = false;

	$scope.paneEnabled = function() {
		return enabled;
	};

	$scope.enablePane = function() {
		enabled = !enabled;
	}
}]).controller('accordionRouteController', ['$scope', '$location', '$routeParams', '$rootScope',
function($scope, $location, $routeParams, $rootScope) {
	var accordionPane = 'header-1';

	if ( typeof ($routeParams.accordionPane) !== 'undefined') {
		accordionPane = $routeParams.accordionPane;
	}

	$rootScope.$broadcast('expand', accordionPane);

	$rootScope.$on('angular-accordion-expand', function(event, eventArguments) {
		$location.path(eventArguments);
	});
}]).run(function($rootScope) {
	$rootScope.gmtDate = new Date('2015-01-01');
}).controller('PositionCtrl', ['$scope',
function($scope) {
	$scope.position = {
		name : 'Potato Master',
		minAge : 0,
		maxAge : 100
	};
	$scope.currencyFormatting = function(value) {
		return value.toString() + " $";
	};
}]).controller('myctrl', function($scope) {
	$scope.$on('scrollbar.show', function() {
		console.log('Scrollbar show');
	});

	$scope.$on('scrollbar.hide', function() {
		console.log('Scrollbar hide');
	});

}).controller('AppCtrl', function($scope) {
	$scope.ddSelectOptions = [{
		text : 'Drop down item',
	}, {
		text : 'Drop down item',
		value : 'one',
		iconCls : 'someicon'
	}, {
		text : 'Drop down item',
		someprop : 'somevalue'
	}, {
		text : 'Drop down item',
		someprop : 'somevalueq'
	}];

	$scope.ddSelectSelected = {
		text : "Select an Option"
	};

}).controller('DemoCtrl', function($scope) {
	$scope.file = null;

	$scope.$watch('file', function(newVal) {
		if (newVal)
			console.log(newVal);
	})
}).controller('demonController', ['$scope',
function($scope) {

	$scope.items = [];

	(function($scope) {
		for (var i = 0; i < 20; i++) {
			$scope.items.push({
				id : i,
				name : 'item' + i,
				introduction : 'welcome to use the ngScrollbar directive and give some feedback'
			});
		}
	})($scope);

	$scope.addItem = function() {
		var index = $scope.items.length;
		$scope.items.push({
			id : index,
			name : 'item' + index,
			introduction : 'welcome to use the ngScrollbar directive and give some feedback'
		});
	};

	$scope.removeItem = function() {
		$scope.items.pop();
	};

	// $scope.scrollbarConfig = function(directive, autoResize, show) {
	// 	config.direction = direction;
	//        config.autoResize = autoResize;
	//        config.scrollbar = {
	//            show: !!show
	//        };
	//        return config;
	// }
}]).controller('UsersIndexController', ['$scope',
function($scope) {

}]).directive("headerBar", [
function() {
	return {
		restrict : "AE",
		templateUrl : "header.html",
	}
}]).controller('scrollctrl', function($scope, $document){
    $scope.toTheTop = function() {
      $document.scrollTop(0, 500);
    }
 }).directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
             if (this.pageYOffset >= 100) {
                 scope.boolChangeClass = true;

                 scope.boolChangeClass = false;

             }
            scope.$apply();
        });
    };
}).controller('loginOpen', function($scope){
    $scope.myClass = [];
    $scope.addClass = function() {
      $scope.myClass.push('overlay-open');
     
    }
      $scope.removeClass = function() {
      $scope.myClass.pop('overlay-open');
    }
 })