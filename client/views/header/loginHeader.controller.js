(function(){
    angular.module('seekoApp.loginHeader', ['ui.router','ui.bootstrap'])

    // .config(['$routeProvider', function($routeProvider) {
    //     $routeProvider.when('/login', {
    //         templateUrl: 'login/login.html',
    //         controller: 'loginCtrl'
    //     });
    // }])

        .controller('loginHeaderCtrl', ['$http','$scope','$uibModal','$state','$rootScope','$cookies','base64','$state','shareData','$window','localStorageService','$document','$location','$anchorScroll',function($http,$scope,$uibModal,$state,$rootScope,$cookies,base64,$sate,shareData,$window,localStorageService,$document,$location,$anchorScroll) {
            $rootScope.isUserLogin=true;
            // if ($location.hash() !== 'header') {
            //     $location.hash('header');
            // } else {
            //     $anchorScroll();
            // }
            shareData.getClassCategories().success(function(data){
                $scope.Supercategories=data.categories;
                console.log($scope.Supercategories);

            });
            $scope.categoryFlag=false;
            $scope.getCategory=function(){
                if($scope.categoryFlag)
                    $scope.hideMenu(event);
                else
                    $scope.getMenu();
            }

            console.log("data from state is"+$state.current.data.hover);
            $scope.classHover=$state.current.data.hover;
            $scope.dashboardHover=$state.current.data.dashboardHover;
            $scope.calendarHover=$state.current.data.calendarHover;

            $scope.getMenu=function(){
                $scope.categoryFlag=true;

            }
            $document.on('click',function(){
                console.log("inside body click");

                $scope.$apply();
            });
            $scope.hideMenu=function(event){

                $scope.categoryFlag=false;
                event.stopPropagation();
            }


            // console.log(profile);
            $scope.ImgSrc=shareData.getProfile().profilePic;
            $scope.userName=shareData.getProfile().firstName;
            //console.log(shareData.getProfile().firstName);
            $scope.isLearner=shareData.getProfile().userType;
            console.log($scope.isLearner);
            $rootScope.isALearner=$scope.isLearner;
            $scope.verifyUser=function () {
                return $scope.isLearner;

            };
            $scope.logout=function () {
                $http.post('/api/logout',{"browserToken":shareData.getToken()}).success(function(data){
                if(data.success==true)
               localStorageService.set('loginFromSeeko','');
                    $state.go('home');
                });
             };

            $scope.fetchRecordingUrl=function()
            {
                $http.post('/getS3fsContent')
                    .success(function (data) {

                        console.log(data);
                        $rootScope.recordings=data.recordingUrl;
                        var modalInstance = $uibModal.open({
                            templateUrl: '../../views/header/recording.html',
                            controller: 'recordingCtrl as recording',
                            backdrop: false
                        });

                    })
            }
            // $scope.joinClass=function() {
            //     var userProfile=shareData.getUserProfile(shareData.getToken());
            //     $http.post('/api/joinClass',userProfile).success(function(data)
            //     {
            //         console.log(data);
            //     });
            // };
            // $scope.startClass=function() {
            //     var userProfile=shareData.getUserProfile(shareData.getToken());
            //     console.log("inside start class"+JSON.stringify(userProfile));
            //     $http.post('/api/startClass',userProfile).success(function(data)
            //     {
            //         console.log(data);
            //     });
            // };

            $scope.applyToTeach=function(){
                    console.log("inside teach");
                    $state.go($state.current);
                    var modalInstance = $uibModal.open({
                        templateUrl: '../../views/learn/applyToTeach.html',
                        controller:'ApplyToTeachCtrl',
                        animation:true,
                        windowClass: 'modal modal-slide-in-right'

                    });
                  }

            // search functionality
            $scope.getSearchResult=function(value){
                console.log("inside change data");
                if(value==''){
                    $scope.searchList=false;
                }
                else {
                    $http.post('/api/searchClasses',{key:value}).success(function(data){
                        console.log(data);
                        if(data.success==true) {
                            $scope.searchMsgLi=false;
                            $scope.searchResult = data.classData;
                            $scope.searchList = true;
                        }
                        else {
                            $scope.searchList = true;
                            $scope.searchMsgLi=true;
                            $scope.searchMsg="No Results Found";
                        }
                    })
                }

            }

            $scope.getExactSearchResult=function(result)
            {
                console.log(result);
                $scope.searchList = false;
                 $rootScope.searchString=result.name;
                shareData.setSearchData(result);
                localStorageService.set('searchString',result);
                if(result.type=='className'){
                    localStorageService.set('searchFrom', 'searchBox');
                        // $rootScope.searchString=result.name;
                         $state.go('dashboard.classDetails',{},{reload:"dashboard.classDetails"});

                }
                else if(result.type=='grandParentCategoryName' || result.type=='parentCategoryName' || result.type=='categoryName'){
                    $state.go('dashboard',{'type':'search'},{reload: true});
                }

            };

            $scope.getSearchCategory=function(value,type){
                localStorageService.set('topJediStatus','');
                if(value.name=='All Categories'){
                    localStorageService.set('stateReview', '');
                    $state.go('viewall');
                }else {

                    localStorageService.set('visitType', 'search');
                    console.log(type);
                    var result = {};
                    if (type == 'grandParentCategoryName')
                        result = {value: shareData.getId(), label: value.name, type: "grandParentCategoryName"};
                    else if (type == 'categoryName')
                        result = {value: shareData.getId(), label: value.name, type: "categoryName"};
                    else
                        result = {value: shareData.getId(), label: value.name, type: "parentCategoryName"};
                    console.log(result);
                    shareData.setSearchData(result);
                    localStorageService.set('searchString', result);
                    localStorageService.set('trendingStatus', result);

                    $state.go('dashboard', {'type': 'search'}, {reload: true});
                }

            }

            angular.element(document).on('click', function () {
                console.log("called");
                $scope.categoryFlag=false;
                $scope.searchList = false;
                $scope.searchMsgLi=false;
                $scope.searchString='';
                $scope.$apply();
            });

            //navigation methods
            $scope.getMyClassView=function(){
                $scope.dashboardHover=false;
                $scope.classHover=true;
                $scope.calendarHover=false;
                localStorageService.set('visitType','login');
                $state.go("dashboard.myClasses");
            }
            $scope.getMyProfile=function(){
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=false;
                localStorageService.set('profile','profile');
                localStorageService.set('visitType','login');
                $state.go("profile",{id:shareData.getId()});
            }
            $scope.getMyAccount=function(){
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=false;
                localStorageService.set('visitType','login');
                $state.go('dashboard.editAccount');
            }
            $scope.getMyNotifications=function(){
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=false;
                localStorageService.set('visitType','login');
                $state.go('dashboard.notifications');
            }
            $scope.editMyProfile=function(){
                console.log("inside editProfile");
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=false;
                localStorageService.set('visitType','login');
                $state.go('editStudent');
            }
            $scope.getMyFavouriteClasses=function(){
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=false;
                localStorageService.set('visitType','login');
                $state.go('dashboard.favourites');
            }
            $scope.goToDashboard=function(){
                $scope.dashboardHover=true;
                localStorageService.set('visitType','login');
                // $state.go('dashboard');
                var currentType={
                    type:'all'
                }
                localStorageService.set('trendingStatus',currentType);
                $window.location.href='dashboard';
            }
            $scope.getCalendar=function(){
                $scope.calendarHover=true;
                $scope.dashboardHover=false;
                $scope.classHover=false;
                localStorageService.set('visitType','login');
                $state.go('dashboard.calendar');
            }

        }]);
}());
