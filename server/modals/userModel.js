var mongoose=require('../node_modules/mongoose');
var bcrypt = require('../node_modules/bcrypt');
var textSearch = require('mongoose-text-search');

var Schema=mongoose.Schema;
var Profile=new Schema({
  //classData:[classDetail],
  email : {type: String ,index:true},
  userType: Boolean,
  password : String,
  role : Number,
  token : String,
  resetToken:String,
  isLogin: Boolean,
  isVerify:Boolean,
  favStudent:{type: Array, "default" : []},
  cuurentUserType:Number,
  profile : {
    firstName : {type: String ,index:true},
    lastName : String,
    dob : Date,
    gender : String,
    aboutMe : String,
    location : String,
    profilePic  :  String ,
    screenName  :  String ,
    description  :  String ,
    teacherProfile  : {
      category  :  String ,
      nameOfClass  :  String ,
      greatJedi  :  String ,
      classDescription  :  String ,
      experience  :  String ,
      credential : [{credential:String,originalFileName:String}],
      teacherClasses  : { type : Array , "default" : [] }
    }
  },

  myClasses:{classId:[]},
  avgStarCount: Number,
  favourites:{favouriteClasses:[],favouriteTeachers:[]},
  myTeachingClasses:{classIds:[]},
  reviews:[{comment:String,date:Date,starCount:String,userName:String,profilePic:String,userId:String,profilePic:String,userType:Boolean}],
  myReviews:[{comment:String,date:Date,starCount:String,teacherName:String,classId:String,className:String,profilePic:String,userId:String,userType:Boolean}],
  isFbLogin:Boolean,
  isGoogleLogin:Boolean,

  learnerClasses  : { type : Array , "default" : [] }, //array
  fbProfile  : {
    id  :  String,
    name: String,
    profilePic:String,
    token: String
  },
  googleProfile  : {
    id  :  String,
    name: String,
    profilePic:String,
    token: String
  },
  notification:{
    reminder:Boolean,
    emailNotification:Boolean,
    newClasses:Boolean,
    premiumClasses:Boolean
  }

});



var classDetail=new Schema({
  className  :  String ,
  category  : {
                grandParentCategoryName:String,
                parentCategoryName:String,
                categoryName:String
              },
  summary  :  String,
  reoccuringClasses  : Boolean ,
  reoccuringClassData:{repeats:String,repeatEvery:Number,repeatOn:{type : Array , "default" : []},startsOn:String,ends:{never:Boolean,after:Number,on:Date}},
  toDate  :  Date ,
  fromDate: Date,
  time  :  Date ,
  description: String,
  classYoutubeLink:String,
  location:String,
  timeDuration  :  String ,
  classDocument :  [{classDocName:String ,classDocUrl:String}],
  classImageVideo : [{classDocName:String ,classDocUrl:String}],
  uploadVideo  :  { type : Array , "default" : [] },
  studentId  : { type : Array , "default" : [] },
  teacherId  :  String ,
  classCost  :  Number
});


var paymentDetail=new Schema({
  classId  :  String ,
  studentId  :  String ,
  paymentId  : String  ,
  transactionId  :  String ,
  amount  :  Number ,
  date  :  { type: Date, default: Date.now },
  success: Boolean
});



var userSchema = new Schema({
  userData:Profile,
  classInfo:[classDetail],
  paymentInfo:[paymentDetail]
});
Profile.plugin(textSearch);
Profile.index({email:'text','profile.firstName':'text'});
Profile.index({'$**': 'text'});
Profile.pre('save', function(next) {
  var user = this;
  console.log("this:"+this);
  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(10, function(err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      user.password = hash;

      next();
    });
  });
});

userSchema.methods.comparePassword = function (passw,cb) {
  console.log(JSON.stringify(this));
  console.log("pass"+passw);

 console.log("userData----"+this.userData.password);
  bcrypt.compare(passw,this.userData.password,function (err, isMatch) {

    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });


};
module.exports=mongoose.model('userProfileSchema',userSchema);
