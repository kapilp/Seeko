(function(){
    angular.module('seekoApp.viewAllClasses', [])
        .controller('viewClassesDashboard',['$scope','$rootScope','$http','shareData','$stateParams','localStorageService','$state','screenSize','Socialshare',function($scope,$rootScope,$http,shareData,$stateParams,localStorageService,$state,screenSize,Socialshare) {
            $scope.showspin=true;
            $scope.inviteeEmail='';
            var startFrom=0;
            var endFrom=18;
            screenSize.rules = {
                superJumbo: '(max-width: 640px)'

            };
            // if(screenSize.is('superJumbo')){
            // }
            $scope.disableJoin=true;
            if(screenSize.is('superJumbo')){
                var startTeacherIndex=0;
                var endTeacherIndex=3;
                var startClassIndex=0;
                var endClassIndex=3;
                var startLiveNow=0;
                var endLiveNow=3;
            }
            else {
                var startTeacherIndex = 0;
                var endTeacherIndex = 6;
                var startClassIndex = 0;
                var endClassIndex = 6;
                var startLiveNow = 0;
                var endLiveNow = 6;
            }


            var firstCondition ;
            $http.get('https://ipinfo.io/json').
            success(function(data) {
                localStorageService.set('country',data);
            }).error(function(data){

            });
         
            if(localStorageService.get('stateReview')!=undefined && localStorageService.get('stateReview')!=''  ){
                console.log("===========");
                $scope.intialData= localStorageService.get('stateReview');
                $scope.intialData1= localStorageService.get('stateReview');
                console.log( $scope.intialData.length);
               // $scope.dashboardClassesTrending= localStorageService.get('stateReview');
                $scope.showBoxView= localStorageService.get('topJediStatus');
                if( $scope.intialData1.length>18){
                    $scope.dashboardClassesTrending=$scope.intialData.splice(startFrom,endFrom);
                    $scope.loadMore=true;
                }else{
                    $scope.dashboardClassesTrending= $scope.intialData;
                    $scope.loadMore=false;
                }
                $scope.showspin=false;
              }
            else if(localStorageService.get('visitType')=='login' || localStorageService.get('visitType')==undefined){
                $http.post('/api/dashboardClasses',{'index':shareData.getId()}).success(function(data){
                    console.log("@@@@@@@@@@@@@@@@@");
                     for(var i=0;i<data.data.length;i++){
                        var strNum=""+data.data[i].classData.avgStarCount;
                         if(strNum!='0'){
                        var index=strNum.indexOf('.');
                        if(index<0){
                            strNum=strNum+'.0';
                            data.data[i].classData.avgStarCount=strNum;
                        }
                      }
                    }
                    $scope.intialData = data.data.reverse();
                    $scope.intialData1 = data.data.reverse();
                    console.log($scope.intialData1.length);
                    // $scope.dashboardClassesTrending=$scope.classData.slice(startClassIndex, endClassIndex);
                    // $scope.dashboardClassesLive=$scope.classData.slice(startClassIndex, endClassIndex);
                    // $scope.dashboardClasses=$scope.classData.slice(startClassIndex, endClassIndex);
                  //  $scope.dashboardClassesTrending= $scope.classData;
                    if(  $scope.intialData1.length>18){
                        $scope.dashboardClassesTrending=$scope.intialData.splice(startFrom,endFrom);
                        $scope.loadMore=true;
                    }else{
                        $scope.dashboardClassesTrending= $scope.intialData;
                        $scope.loadMore=false;
                    }
                    $scope.showspin=false;
                });
            }
            else {
                console.log("################");
                $rootScope.searchString=localStorageService.get('searchString').name;
                var requestJson={"index":shareData.getId(),"name":localStorageService.get('searchString').label,"type":localStorageService.get('searchString').type};
                $http.post('/api/searchByClassIndex',requestJson).success(function(data){
                     for(var i=0;i<data.data.length;i++){
                        var strNum=""+data.data[i].classData.avgStarCount;
                         if(strNum!='0'){
                        var index=strNum.indexOf('.');
                        if(index<0){
                            strNum=strNum+'.0';
                            data.data[i].classData.avgStarCount=strNum;
                        }
                      }
                    }
                    $scope.intialData = data.data.reverse();
                    $scope.intialData1 = data.data.reverse();
                    // $scope.dashboardClassesTrending=$scope.classData.slice(startClassIndex, endClassIndex);
                    // $scope.dashboardClassesLive=$scope.classData.slice(startClassIndex, endClassIndex);
                    // $scope.dashboardClasses=$scope.classData.slice(startClassIndex, endClassIndex);
                 //   $scope.dashboardClassesTrending= $scope.classData;
                    if(  $scope.intialData1.length>18){
                        $scope.dashboardClassesTrending=$scope.intialData.splice(startFrom,endFrom);
                        $scope.loadMore=true;
                    }else{
                        $scope.dashboardClassesTrending= $scope.intialData;
                        $scope.loadMore=false;
                    }
                    $scope.showspin=false;

                });
            }

            // $scope.getNextClasses=function(type)
            // {
            //     if(type== 'trending') {
            //         console.log("isnide trending");
            //         if((($scope.classData.length>endClassIndex) && ($scope.classData.length>5)) ||  (($scope.classData.length>endClassIndex) && ($scope.classData.length>2))) {
            //             if(screenSize.is('superJumbo')){
            //                 startClassIndex += 3;
            //                 endClassIndex += 3;
            //             }
            //             else{
            //                 startClassIndex += 6;
            //                 endClassIndex += 6;
            //             }
            //
            //             $scope.dashboardClassesTrending=$scope.classData.slice(startClassIndex, endClassIndex);
            //         }
            //     }
            //     else if(type== 'liveNow') {
            //         console.log("isnide liveNow");
            //         if( (($scope.classData.length>endLiveNow)&& ($scope.classData.length>5))||(($scope.classData.length>endLiveNow) && ($scope.classData.length>2)) ) {
            //             if(screenSize.is('superJumbo')){
            //                 startLiveNow += 3;
            //                 endLiveNow += 3;
            //             }
            //             else{
            //                 startLiveNow += 6;
            //                 endLiveNow += 6;
            //             }
            //
            //             $scope.dashboardClassesLive = $scope.classData.slice(startLiveNow, endLiveNow);
            //         }
            //     }
            //     else {
            //         console.log("isnide else if");
            //         if( (($scope.classData.length>endTeacherIndex) && ($scope.classData.length>5))||(($scope.classData.length>endTeacherIndex) && ($scope.classData.length>2)) ) {
            //             if(screenSize.is('superJumbo')){
            //                 startTeacherIndex += 3;
            //                 endTeacherIndex += 3;
            //             }
            //             else {
            //                 startTeacherIndex += 6;
            //                 endTeacherIndex += 6;
            //             }
            //
            //             $scope.dashboardClasses = $scope.classData.slice(startTeacherIndex, endTeacherIndex);
            //         }
            //     }
            //
            //
            //
            //
            // }

            // $scope.getPrevClasses=function(type) {
            //     if (type == 'trending') {
            //         if (startClassIndex > 0) {
            //             if(screenSize.is('superJumbo')){
            //                 startClassIndex -= 3;
            //                 endClassIndex -= 3;
            //             }
            //             else{
            //                 startClassIndex -= 6;
            //                 endClassIndex -= 6;
            //             }
            //
            //             $scope.dashboardClassesTrending = $scope.classData.slice(startClassIndex, endClassIndex);
            //         }
            //
            //     }
            //     else if (type == 'liveNow') {
            //         if (startLiveNow > 0) {
            //             if(screenSize.is('superJumbo')){
            //                 startLiveNow -= 3;
            //                 endLiveNow -= 3;
            //             }
            //             else{
            //                 startLiveNow -= 6;
            //                 endLiveNow -= 6;
            //             }
            //
            //             $scope.dashboardClassesLive = $scope.classData.slice(startLiveNow, endLiveNow);
            //         }
            //
            //     }
            //     else {
            //         if (startTeacherIndex > 0) {
            //             if(screenSize.is('superJumbo')){
            //                 startTeacherIndex -= 3;
            //                 endTeacherIndex -= 3;
            //             }
            //             else
            //             {
            //                 startTeacherIndex -= 6;
            //                 endTeacherIndex -= 6;
            //             }
            //
            //             $scope.dashboardClasses = $scope.classData.slice(startTeacherIndex, endTeacherIndex);
            //
            //         }
            //     }
            // }

            $scope.getClassDetail=function(value){
                console.log(value);
                var request={"label":value.classData.className,"value":value._id,"teacherId":value.classData.teacherId};
                localStorageService.set('searchString',request);
                localStorageService.set('searchFrom', 'searchBox');
                $state.go('classDetails', {id:value._id});

            }


            $scope.joinClass=function(value){
                var data={teacherName:value.profile.userData.profile.firstName,classId:value.data._id,studentId:shareData.getId(),teacherId:value.profile._id,date:value.data.classData.fromDate,className:value.data.classData.className};
                $http.post('/api/joinClass',data).success(function(data){
                    value.isJoined=true;

                });
            }

            $scope.getRating=function(rate){
                var starCount=
                    {
                        "upperCount":Math.floor(rate),
                        "halfCount":rate-Math.floor(rate),
                        "lowerCount":Math.floor(5-rate)
                    };
            }

            $scope.getTeacherProfile = function (profileDeatil) {
                // console.log(profileDeatil);
                localStorageService.set('topJediStatus','');
                 $state.go('profile',{id:profileDeatil._id});
  
              }

              $scope.loadMoreFn=function(){
                  console.log(endFrom+18)
                  console.log($scope.intialData1.length)
                  console.log(endFrom)
                  if((endFrom+18)<$scope.intialData1.length){
                     startFrom=endFrom;
                    endFrom=endFrom+18;
                    var spliceArray=$scope.intialData1;
                     var spliceArray1=spliceArray.splice(startFrom,endFrom);
                    $scope.dashboardClassesTrending=$scope.dashboardClassesTrending.concat(spliceArray1);
                  }else{
                    startFrom=endFrom;
                    endFrom=$scope.intialData1.length;
                    var spliceArray=$scope.intialData1;
                    var spliceArray1=spliceArray.splice(startFrom,endFrom);
                    $scope.dashboardClassesTrending=$scope.dashboardClassesTrending.concat(spliceArray1);
                    $scope.loadMore=false;
                  }

              }

        }]);
}());
