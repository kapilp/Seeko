(function(){
angular.module('seekoApp.header', ['ui.router','ui.bootstrap'])

    // .config(['$routeProvider', function($routeProvider) {
    //     $routeProvider.when('/login', {
    //         templateUrl: 'login/login.html',
    //         controller: 'loginCtrl'
    //     });
    // }])

    .controller('headerCtrl', ['$scope','$uibModal','$state','$rootScope','$cookies','base64','$state','deviceDetector','$http','shareData','$document','$location','$anchorScroll','localStorageService','$window',function($scope,$uibModal,$state,$rootScope,$cookies,base64,$state,deviceDetector,$http,shareData,$document,$location,$anchorScroll,localStorageService,$window) {
        $rootScope.isUserLogin=false;
        shareData.getClassCategories().success(function(data){
            $scope.Supercategories=data.categories;

            console.log($scope.Supercategories);
        });

        // if ($location.hash() !== 'header') {
        //
        // } else {
        //     $anchorScroll();
        // }

        $scope.categoryFlag=false;
        $scope.getCategory=function(){
            if($scope.categoryFlag)
                $scope.hideMenu(event);
            else
                $scope.getMenu();
        }
        console.log("inside header controller");


        $scope.getMenu=function(){
            $scope.categoryFlag=true;
        }
        $scope.getLi=function(event){
            console.log(event);
            if(event.keyCode==40 || event.which==40)
                $scope.abc1=true;
        }
        $document.on('click',function(){
            $scope.categoryFlag=false;

            $scope.$apply();
        });



        var browser_osDetails={
            "browserName":  deviceDetector.browser,
            "os": deviceDetector.os,
            "browserVersion":deviceDetector.browser_version
        };
        $http.post('/api/getBrowserDetails',browser_osDetails).success(function(data){
            console.log('data'+JSON.stringify(data));
        });
        $rootScope.teacher=false;
        $scope.myClass = [];

        $scope.addLogin = function() {
            var modalInstance = $uibModal.open({
                templateUrl: '../../views/login/login.html',
                controller:'loginCtrl',
                animation:true,
                windowClass: 'modal modal-slide-in-right'

            });
        };
        $scope.addSignUp=function () {

            var modalInstance = $uibModal.open({
                templateUrl: '../../views/signup/signup.html',
                controller:'signupCtrl as signup',
                animation:true,
                windowClass: 'modal modal-slide-in-right'
            });
        };
         $scope.status=function (value) {
          var data=$cookies.get('seeko');
            if(data)
            {
                var encodedProfile = data.split('.')[1];
                var profile =base64.decode(encodedProfile);
                $rootScope.ImgSrc="assets/images/profile-img.png";
                $rootScope.status=false;
            }
            else {
                $rootScope.status=true;
            }
            return $rootScope.status
        }

        $scope.getSearchResult=function(value){
            console.log("inside change data");
            if(value==''){
                $scope.searchList=false;
            }
            else {
                $http.post('/api/searchClasses',{key:value}).success(function(data){
                    console.log(data);
                    if(data.success==true) {
                        $scope.searchMsgLi=false;
                        $scope.searchResult = data.classData;
                        $scope.searchList = true;
                    }
                    else {
                        $scope.searchList = true;
                        $scope.searchMsgLi=true;
                        $scope.searchMsg="No Results Found";
                    }
                })
            }

        }

        $scope.searchByClassName =function (value) {

            var result = {
                'index':'','type':'categoryName','name':value
            }
            localStorageService.set('searchString',result);
            $state.go('teacherDashboard',{'type':'search'},{reload: true});


        }

        $scope.getExactSearchResult=function(result)
        {
            console.log(result);
            $scope.searchList = false;
            $rootScope.searchString=result.name;
            shareData.setSearchData(result);
            localStorageService.set('searchString',result);
            console.log(result);
            if(result.type=='className'){
                localStorageService.set('searchFrom', 'searchBox');
                // $rootScope.searchString=result.name;
                $state.go('classDetails',{},{reload:"classDetails"});
            }
            else if(result.type=='grandParentCategoryName' || result.type=='parentCategoryName' || result.type=='categoryName'){
                $state.go('home.dashboard',{'type':'search'},{reload: 'home.dashboard'});
            }

        };
        $scope.getSearchCategory=function(value,type){
            localStorageService.set('topJediStatus','');
            if(value.name=='All Categories'){
                localStorageService.set('stateReview', '');
                $state.go('viewall');
            }else {
                localStorageService.set('visitType', 'search');
                console.log(type);
                var result = {};
                if (type == 'grandParentCategoryName')
                    result = {value: "no", label: value.name, type: "grandParentCategoryName"};
                else if (type == 'categoryName')
                    result = {value: "no", label: value.name, type: "categoryName"};
                else
                    result = {value: "no", label: value.name, type: "parentCategoryName"};
                console.log(result);
                shareData.setSearchData(result);
                localStorageService.set('trendingStatus', result);
                localStorageService.set('searchString', result);
                $window.location.href = 'home/dashboard';
                // $state.go('home.dashboard',{reload: 'home.dashboard'});
            }

        }

        angular.element(document).on('click', function () {
            console.log("called");
            $scope.searchList = false;
            $scope.searchMsgLi=false;
            $rootScope.searchString='';
            $scope.$apply();
        });

       $scope.hideMenu=function(event){

           console.log($scope.Supercategories);
           $scope.categoryFlag=false;

       }


    }]);
}());
