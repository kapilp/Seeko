(function () {
    angular.module('seekoApp.footer', [])

            .controller('footerCtrl', ['$scope', '$rootScope','$http','$location','$anchorScroll','$state','Notification','$window','shareData','$uibModal', function ($scope, $rootScope ,$http,$location,$anchorScroll,$state,Notification,$window,shareData,$uibModal)

            {
                // console.log($state.current.data.isLogin);
                    // $scope.isLogin=$state.current.data.isLogin.isLogin;
                if(shareData.getCookies()){
                    $scope.isLogin=true;
                }
                else {
                    $scope.isLogin=false;
                }
                    $scope.gotoAnchor = function(x) {
                        console.log(x);
                        var newHash = x;
                        if ($location.hash() !== newHash) {
                            // set the $location.hash to `newHash` and
                            // $anchorScroll will automatically scroll to it
                            $location.hash(x);
                        } else {
                            // call $anchorScroll() explicitly,
                            // since $location.hash hasn't changed
                            $anchorScroll();
                        }
                    };


                $http.post('/api/twitter').success(function (res) {
                    $scope.status =[];
                    var status = JSON.parse(res.data.body);

                    for(i=0;i<status.length;i++)
                    {
                       time = new Date(status[i].created_at);
                       $scope.status.push({
                           status:status[i],
                           time:timeDifference(new Date(),time)

                    })
                    }
                    console.log($scope.status);
                })

                function timeDifference(current, previous) {

                    var msPerMinute = 60 * 1000;
                    var msPerHour = msPerMinute * 60;
                    var msPerDay = msPerHour * 24;
                    var msPerMonth = msPerDay * 30;
                    var msPerYear = msPerDay * 365;

                    var elapsed = current - previous;

                    if (elapsed < msPerMinute) {
                        return Math.round(elapsed/1000) + ' seconds ago';
                    }

                    else if (elapsed < msPerHour) {
                        return Math.round(elapsed/msPerMinute) + ' minutes ago';
                    }

                    else if (elapsed < msPerDay ) {
                        return Math.round(elapsed/msPerHour ) + ' hours ago';
                    }

                    else if (elapsed < msPerMonth) {
                        return Math.round(elapsed/msPerDay) + ' days ago';
                    }

                    else if (elapsed < msPerYear) {
                        return Math.round(elapsed/msPerMonth) + ' months ago';
                    }

                    else {
                        return + Math.round(elapsed/msPerYear ) + ' years ago';
                    }
                }

                $scope.getContactPage=function(){

                    if(shareData.getCookies()){
                        if(shareData.getProfile().userType)
                            $state.go('teacherDashboard.contact');
                        else
                            $state.go('dashboard.contact');
                     }
                    else {
                        $state.go('home.contact');
                    }
                }

                $scope.getTermsAndCondition=function(){
                    if($state.current.data.isLogin.isLogin){
                        if($state.current.data.isLogin.userType)
                            $state.go('teacherDashboard.terms&condition');
                        else
                            $state.go('dashboard.terms&condition');
                    }
                    else {
                        $state.go('home.terms&condition');
                    }
                }

                $scope.sendEmail=function(email){
                    console.log(email);
                    $http.post('/api/newsLetter',{emailId:email}).success(function(data)
                    {
                        $scope.email='';
                        console.log(data);
                        if(data.success)
                        Notification.primary({message:"Thanks for signing up. Keep an eye on your inbox.",templateUrl: "../../views/messageNotifications/success.html"});
                        else
                            if(data.status='Member Exists')
                                Notification.primary({message:"You are already subscribed to our newsletter.",templateUrl: "../../views/messageNotifications/error.html"});
                                else
                            Notification.primary({message:"There is some problem with subscription,try after some time",templateUrl: "../../views/messageNotifications/error.html"});
                    })
                }
                $scope.addLogin = function() {

                    var modalInstance = $uibModal.open({
                        templateUrl: '../../views/login/login.html',
                        controller:'loginCtrl',
                        animation:true,
                        windowClass: 'modal modal-slide-in-right'

                    });
                };
                $scope.addSignUp=function () {

                    var modalInstance = $uibModal.open({
                        templateUrl: '../../views/signup/signup.html',
                        controller:'signupCtrl as signup',
                        animation:true,
                        windowClass: 'modal modal-slide-in-right'
                    });
                };
                $scope.openFbPage=function(){
                    $window.open('https://www.facebook.com/seekolearning/');
                }
                $scope.openTwitterPage=function(){
                    $window.open('https://twitter.com/Seeko00386394');
                }

                $scope.openLinkedIn=function(){
                    $window.open('https://www.linkedin.com/in/seeko-app-051290143/');
                }

            }]);

}());
