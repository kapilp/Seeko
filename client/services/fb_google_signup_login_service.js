
(function(){
    angular.module('signUpService',[])


        .factory('facebookService', function($q) {
            return {
                facebookData: function() {
                    var deferred = $q.defer();
                    FB.api('/me', {
                        fields: 'last_name'
                    }, function(response) {
                        if (!response || response.error) {
                            deferred.reject('Error occured');
                        } else {
                            deferred.resolve(response);
                        }
                    });
                    return deferred.promise;
                }
            }
        })
        .factory('gmailService', function($q,shareData,$rootScope) {
            return{
                gmailData: function() {


                    var deferred = $q.defer();
                    var loginCredentials='';

                    var auth2={};
                    gapi.load('auth2', function(){

                        // Retrieve the singleton for the GoogleAuth library and set up the client.
                        gapi.auth2.init({
                            client_id: '736633620025-bj24nbi1ka40ih1q0v46rnaaa4k39jlv.apps.googleusercontent.com',//for client
                           // client_id: '1078609277605-f6l3197ecm98ald671j85d7piln1pt3i.apps.googleusercontent.com',//for server
                            cookiepolicy: 'single_host_origin'
                            // Request scopes in addition to 'profile' and 'email'
                            //scope: 'additional_scope'
                        });

                        auth2 = gapi.auth2.getAuthInstance();
                        console.log("auth2 gapi startApp");
                        attachSignin(document.getElementById('googleSignup'));
                        attachSignin(document.getElementById('googleSignIn'));

                    });



                    function attachSignin(element) {

                        /*onSignIn(googleUser);*/
                        auth2.attachClickHandler(element, {},function(googleUser) {
                            if (!googleUser || googleUser.error) {
                                deferred.reject('Error occured');
                            } else {

                                var profile = googleUser.getBasicProfile();
                                console.log(profile);
                                loginCredentials={
                                    'firstName':profile.getGivenName(),
                                    'lastName':profile.getFamilyName(),
                                    'name':profile.getName(),
                                    'email':profile.getEmail(),
                                    'id':profile.getId(),
                                    'picture':profile.getImageUrl(),
                                    'token':googleUser.getAuthResponse().id_token,
                                    'userType':$rootScope.teacher
                                };
                                deferred.resolve(loginCredentials);
                            }


                        });

                    }


                    return deferred.promise;
                }
            }
        })

        .factory('AuthToken', function($window){
            var authTokenFactory = {};

            authTokenFactory.getToken = function(){
                return $window.localStorage.getItem('token');
            }

            authTokenFactory.setToken = function(token){
                if(token)
                    $window.localStorage.setItem('token',token);
                else
                    $window.localStorage.removeItem('token');

            }

            return authTokenFactory;
        })

        .factory('AuthInterceptor', function($q,$location,AuthToken){
            var interceptorFactory = {};
            interceptorFactory.request = function(config){
                var token = AuthToken.getToken();
                if(token){
                    config.headers[X-access-token] = token;
                }
                return config;

            };
            interceptorFactory.responseError = function(response){
                if(response.status == 403){
                    $location.path('/home');
                    return $q.reject(response);
                }
            }

            return interceptorFactory;

        });


}());
