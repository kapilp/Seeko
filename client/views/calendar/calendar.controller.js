(function(){
    angular.module('seekoApp.calendar', [])
        .controller('calendarCtrl', ['$scope','$rootScope','facebookService','gmailService','$window','$cookies','$http','Alertify','base64','$state','shareData','deviceDetector','$filter','Notification','calendarHelper','$q','localStorageService','$document','$window',function($scope,$rootScope,facebookService,gmailService,$window,$cookies,$http,Alertify,base64,$state,shareData,deviceDetector,$filter,Notification,calendarHelper,$q,localStorageService,$document,$window) {
            $scope.showspin=true;
            $scope.choice='all';
            $scope.classData=[];
            $scope.inviteBox=false;
            $scope.userType=shareData.getProfile().userType;

            $scope.calendar = {
                year: new Date().getFullYear(),
                month: new Date().getMonth(),
                monthName: calendarHelper.getMonthName(new Date().getMonth()),
                isPreviousYear:false,
                isPreviousMonth:false
            };

            getCalendarDays(new Date().getFullYear(), new Date().getMonth(),$scope.choice).then(function(days){
                $scope.days=days;
            });

            $scope.nextMonth = function () {
            calendarHelper.incrementCalendarMonth($scope.calendar);
            $scope.calendar.monthName = calendarHelper.getMonthName($scope.calendar.month);
            getCalendarDays($scope.calendar.year, $scope.calendar.month,$scope.choice).then(function(days){
                $scope.days=days;
            });

                if((parseInt($scope.calendar.month)<parseInt(new Date().getMonth())) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){

                    $scope.calendar.isPreviousYear = false;
                }
                else if((parseInt($scope.calendar.month)>=parseInt(new Date().getMonth())) ){

                    $scope.calendar.isPreviousYear = true;
                }

            }

            $scope.previousMonth = function () {
                if(parseInt($scope.calendar.year)==parseInt(new Date().getFullYear())) {
                if (parseInt($scope.calendar.month) == parseInt(new Date().getMonth()+1)) {
                        $scope.calendar.isPreviousYear = false;
                        $scope.calendar.isPreviousMonth = false;
                    }
                }
                if((parseInt($scope.calendar.month)<parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear())){

                    $scope.calendar.isPreviousYear = false;
                }
                else if((parseInt($scope.calendar.month)>=parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){
                    $scope.calendar.isPreviousYear = true;
                }
                calendarHelper.decrementCalendarMonth($scope.calendar);
                $scope.calendar.monthName = calendarHelper.getMonthName($scope.calendar.month);
                getCalendarDays($scope.calendar.year, $scope.calendar.month,$scope.choice).then(function(days){
                    $scope.days=days;
                });

            }

            $scope.nextYear = function () {
                $scope.calendar.isPreviousYear=true;
                $scope.calendar.isPreviousMonth=true;
                $scope.calendar.year++;
                getCalendarDays($scope.calendar.year, $scope.calendar.month,$scope.choice).then(function(days){
                    $scope.days=days;
                });
            }

            $scope.previousYear = function () {
                $scope.calendar.year--;
                getCalendarDays($scope.calendar.year, $scope.calendar.month,$scope.choice).then(function(days){
                    $scope.days=days;
                });
                if(parseInt($scope.calendar.year)==parseInt(new Date().getFullYear())){
                    $scope.calendar.isPreviousYear=false;

                    if(parseInt($scope.calendar.month+1)<=parseInt(new Date().getMonth()+1)){
                        $scope.calendar.isPreviousMonth=false;}
                }
                if((parseInt($scope.calendar.month)<parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){

                    $scope.calendar.isPreviousYear = false;
                }
                else if((parseInt($scope.calendar.month)>=parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){
                    $scope.calendar.isPreviousYear = true;
                }
            }

             $scope.selectDate = function (day) {

                 $scope.inviteBox=false;

                     }
            $scope.getPreviousDate=function(day) {
                if(parseInt(new Date().getFullYear())== parseInt($scope.calendar.year)){
                    if (parseInt(new Date().getMonth() + 1) == parseInt($scope.calendar.month + 1)) {
                        if (parseInt(day) > parseInt(new Date().getDate())) {

                            return false;
                        }
                        else if (parseInt(day) < parseInt(new Date().getDate())) {

                            return true;
                        }
                    }
                }

            }

            $scope.getCalendarByChoice=function() {

                getCalendarDays($scope.calendar.year, $scope.calendar.month, $scope.choice).then(function (days) {
                    $scope.days = days;

                });
            }



            $scope.getClassDetail=function(value){

                 var request={"label":value.classData.className,"value":value._id,"teacherId":value.classData.teacherId};

                localStorageService.set('searchString',request);
                localStorageService.set('searchFrom', 'searchBox');

                    $state.go('classDetails',{id:value._id},{reload:"classDetails"});

            }
            function getCalendarDays(year, month,value) {
                var deferred = $q.defer();

                var monthStartDate = new Date(year, month, 1);

                var days = [];
                var count = 0;
                var totalCell = 42;
                var cellCount=1;
                var previousTotalMonthDays = 0;
                $http.post('/api/calenderClassData', {
                    searchType: value,
                    userId: shareData.getId(),
                    currentMonthDate: new Date(year, month, 5)
                })
                    .success(function (data) {


                        for (var idx = 1; idx <= new Date(year, month, 0).getDate(); idx++) {

                            previousTotalMonthDays++;
                        }

                        for (var idx = 0; idx < monthStartDate.getDay(); idx++) {
                            count++;
                        }

                        if (count > 0) {
                            var previousMonthDays = previousTotalMonthDays - (count - 1);


                            for (var idx = 0; idx < count; idx++) {
                                var dayJson = {};

                                if(cellCount== 1 ||cellCount== 2 || cellCount==3 || cellCount==4 ||cellCount== 8 ||cellCount== 9 || cellCount==10 || cellCount==11 || cellCount==15 || cellCount==16 || cellCount==17 || cellCount==18 || cellCount==22 || cellCount==23 || cellCount==24 || cellCount==25 || cellCount==29 || cellCount==30 || cellCount==31 || cellCount==32 || cellCount==36 || cellCount==37 || cellCount==38 ||cellCount==39 ){
                                    dayJson.isLeft=true;
                                }
                                else{
                                    dayJson.isRight=true;
                                }
                                dayJson.day = previousTotalMonthDays - (count - 1);
                                dayJson.isDisabled = true;
                                days.push(dayJson);
                                previousTotalMonthDays++;
                                totalCell--;
                                cellCount++;

                            }

                        }

                        if (data.success == true) {

                              for(var i=0;i<data.data.length;i++){
                                    for(var j=0;j<data.data[i].classes.length;j++){
                                      var strNum=""+data.data[i].classes[j].classData.avgStarCount;
                                            if(strNum!='0'){
                                            var index=strNum.indexOf('.');
                                            if(index<0){
                                                strNum=strNum+'.0';
                                                data.data[i].classes[j].classData.avgStarCount=strNum;
                                            }
                                        }
                                    }


                   
                    }

                            var classData = data.data;

                            for (var idx = 1; idx <= new Date(year, month + 1, 0).getDate(); idx++) {
                                var noOfClasses = 0;
                                var classes = [];
                                for (i = 0; i < classData.length; i++) {


                                    if ((year == new Date(classData[i].fromDate).getFullYear()) && (month == new Date(classData[i].fromDate).getMonth()) && (idx == new Date(classData[i].fromDate).getDate())) {

                                        noOfClasses = classData[i].classes.length;
                                        classes = classData[i].classes;

                                    }


                                }
                                var dayJson = {};
                                if(cellCount== 1 ||cellCount== 2 || cellCount==3 || cellCount==4 ||cellCount== 8 ||cellCount== 9 || cellCount==10 || cellCount==11 || cellCount==15 || cellCount==16 || cellCount==17 || cellCount==18 || cellCount==22 || cellCount==23 || cellCount==24 || cellCount==25 || cellCount==29 || cellCount==30 || cellCount==31 || cellCount==32 || cellCount==36 || cellCount==37 || cellCount==38 ||cellCount==39 ){
                                    dayJson.isLeft=true;
                                }
                                else{
                                    dayJson.isRight=true;
                                }
                                dayJson.day = idx;
                                dayJson.isDisabled = false;
                                dayJson.noOfClasses = noOfClasses;
                                dayJson.classes = classes;
                                days.push(dayJson);
                                totalCell--;
                                cellCount++;
                            }


                        }
                        else {

                            for (var idx = 1; idx <= new Date(year, month + 1, 0).getDate(); idx++) {
                                var dayJson = {};
                                if(cellCount== 1 ||cellCount== 2 || cellCount==3 || cellCount==4 ||cellCount== 8 ||cellCount== 9 || cellCount==10 || cellCount==11 || cellCount==15 || cellCount==16 || cellCount==17 || cellCount==18 || cellCount==22 || cellCount==23 || cellCount==24 || cellCount==25 || cellCount==29 || cellCount==30 || cellCount==31 || cellCount==32 || cellCount==36 || cellCount==37 || cellCount==38 ||cellCount==39 ){
                                    dayJson.isLeft=true;
                                }
                                else{
                                    dayJson.isRight=true;
                                }
                                dayJson.day = idx;
                                dayJson.isDisabled = false;
                                dayJson.noOfClasses = 0;
                                dayJson.classes = [];
                                days.push(dayJson);
                                totalCell--;
                                cellCount++;
                            }
                        }


                        for (var idx = 1; idx <= totalCell; idx++) {
                            var dayJson = {};
                            if(cellCount== 1 ||cellCount== 2 || cellCount==3 || cellCount==4 ||cellCount== 8 ||cellCount== 9 || cellCount==10 || cellCount==11 || cellCount==15 || cellCount==16 || cellCount==17 || cellCount==18 || cellCount==22 || cellCount==23 || cellCount==24 || cellCount==25 || cellCount==29 || cellCount==30 || cellCount==31 || cellCount==32 || cellCount==36 || cellCount==37 || cellCount==38 ||cellCount==39 ){
                                dayJson.isLeft=true;
                            }
                            else{
                                dayJson.isRight=true;
                            }
                            dayJson.day = idx;
                            dayJson.isDisabled = true;
                            days.push(dayJson);
                            cellCount++;

                        }

                        $scope.showspin=false;
                        deferred.resolve(days);

                    }).error(function (error) {

                    $scope.showspin=false;
                    deferred.reject(error);
                });


                return deferred.promise;
            }

            $scope.getClassTime=function(value){

                var time;

                var beginTime=value.split(':')[0];

                if(parseInt(beginTime)<=11){
                    if(parseInt(beginTime)==0)
                        time=12 +"AM";
                    else
                    time=parseInt(beginTime)+" AM";
                }
                else{
                    if(parseInt(beginTime)-12==0)
                        time=12 +" PM";
                    else
                    time=parseInt(beginTime)-12 +" PM";
                }

                return time;
            }


        }]);
}());
