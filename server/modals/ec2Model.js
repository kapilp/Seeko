var mongoose=require('../node_modules/mongoose');
var Schema=mongoose.Schema;

var elasticIpDetail=new Schema({
  elasticIp : String,
  instanceId : String,
  issuedAt : String,
  releasedAt : String

});


var ec2Detail=new Schema({
  elasticIpInfo : [elasticIpDetail],
  instanceId : String,
  classId : String,
  privateIp : String,
  publicIp : String,
  allocationId : String,
  launchingTime : { type: Date },
  timeStamp : { type: Date, default: Date.now }
});


var ec2InfoSchema = new Schema({
  ec2Info:[ec2Detail]
});

module.exports=mongoose.model('ec2DataSchema',ec2InfoSchema);
