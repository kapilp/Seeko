(function () {
    angular.module('seekoApp.favourites', ['ui.router', 'ui.bootstrap'])
        .controller('favouritesCtrl', ['$scope', '$rootScope', 'shareData', '$http', 'TS', 'Notification', '$state', 'localStorageService','screenSize', function ($scope, $rootScope, shareData, $http, TS, Notification, $state, localStorageService,screenSize) {
            $scope.showspin=true;
          // $scope.showTeachers = true;
            screenSize.rules = {
                retina: 'only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx)',
                superJumbo: '(max-width: 640px)',

            };

            $scope.teacherFavourites = [];
            if(screenSize.is('superJumbo')){
                var startTeacherIndex=0;
                var endTeacherIndex=3;
                var startClassIndex=0;
                var endClassIndex=3;
            }
            else
            {
                var startTeacherIndex=0;
                var endTeacherIndex=6;
                var startClassIndex=0;
                var endClassIndex=6;
            }

            console.log($rootScope.isALearner);

            $http.post('/api/favouriteTeacher',{id:shareData.getId()}).success(function (data) {
                if(data.success == true) {
                    $scope.allTeacherFavourites=data.userData.reverse();
                    $scope.teacherFavourites = $scope.allTeacherFavourites.slice(startTeacherIndex, endTeacherIndex);
                }
                else
                    $scope.showTeacherMsg=true;

            });
            $http.post('/api/favouriteClass', {id: shareData.getId()}).success(function (data) {

                if(data.success == true){
                    $scope.allClassesFavourites=data.classData.reverse();
                    $scope.favoritesClasses= $scope.allClassesFavourites.slice(startClassIndex, endClassIndex);
                }
                else
                    $scope.showClassMsg=true;

                $scope.showspin=false;
            });

            $scope.getNext=function(type)
            {

                if(type== 'teachers') {
                    if( (($scope.allTeacherFavourites.length>endTeacherIndex) && ($scope.allTeacherFavourites.length>5)) || (($scope.allTeacherFavourites.length>endTeacherIndex) && ($scope.allTeacherFavourites.length>2)) ) {
                        if(screenSize.is('superJumbo')){
                            startTeacherIndex += 3;
                            endTeacherIndex += 3;
                        }
                        else
                        {
                            startTeacherIndex += 6;
                            endTeacherIndex += 6;
                        }
                        $scope.teacherFavourites = $scope.allTeacherFavourites.slice(startTeacherIndex, endTeacherIndex);
                    }
                }

                else {
                    if( (($scope.allClassesFavourites.length>endClassIndex) && ($scope.allClassesFavourites.length>5)) || (($scope.allClassesFavourites.length>endClassIndex) && ($scope.allClassesFavourites.length>2)) ) {
                        if(screenSize.is('superJumbo')){
                            startClassIndex += 3;
                            endClassIndex += 3;
                        }
                        else
                        {
                            startClassIndex += 6;
                            endClassIndex += 6;
                        }

                        $scope.favoritesClasses= $scope.allClassesFavourites.slice(startClassIndex, endClassIndex);
                    }
                }

            }

            $scope.getPrev=function(type) {
                if (type == 'teachers') {
                    if (startTeacherIndex > 0) {
                        if(screenSize.is('superJumbo')){
                            startTeacherIndex -= 3;
                            endTeacherIndex -= 3;
                        }
                        else
                        {
                            startTeacherIndex -= 6;
                            endTeacherIndex -= 6;
                        }

                        $scope.teacherFavourites = $scope.allTeacherFavourites.slice(startTeacherIndex, endTeacherIndex);
                    }

                }
                else {
                    if (startClassIndex > 0) {
                        if(screenSize.is('superJumbo')){
                            startClassIndex -= 3;
                            endClassIndex -= 3;
                        }
                        else
                        {
                            startClassIndex -= 6;
                            endClassIndex -= 6;
                        }

                        $scope.favoritesClasses= $scope.allClassesFavourites.slice(startClassIndex, endClassIndex);

                    }
                }
            }

            $scope.changeState = function (value) {
                var request={"label":value.data.classData.className,"value":value.data._id,"teacherId":value.data.classData.teacherId};
                console.log('request');
                localStorageService.set('searchString',request);
                localStorageService.set('searchFrom', 'searchBox');

                $state.go('classDetails',{id:value.data._id});


            }
            $scope.getTeacherClasses=function(teacher){

                $state.go('profile',{id:teacher._id});
            }

            $scope.findFavClass = function () {
                localStorageService.set('stateReview', '');
                $state.go('viewall');
            }
        }])
}());
