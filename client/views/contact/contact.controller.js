(function(){
    angular.module('seekoApp.contact',[]).
        controller('contactCtrl',['$scope','$location','$anchorScroll','$http',function($scope,$location,$anchorScroll,$http){
        if ($location.hash() !== 'header') {
            $location.hash('header');
        } else {
            $anchorScroll();
        }
            $scope.user={
                name:'',
                email:'',
                message:'',
                subjectSelect:false,
                subject:"Seeko Contact Request"
                };

                $scope.subjectList=['Class','Time','Payment'];

                $scope.sendMessage=function(value){
                    if ($scope.userContact.$valid) {
                        $http.post('/api/contactUs',{"user":value}).success(function(data){
                            console.log(data);
                            $scope.user.name='';
                            $scope.user.email='';
                            $scope.user.message='';
                            $scope.userContact.submitted=false0;

                        })
                    }else {
                        console.log("error something is missing here");
                        $scope.userContact.submitted=true;

                    }


                }

    }]);

}
());