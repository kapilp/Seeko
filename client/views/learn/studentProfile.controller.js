(function(){
    angular.module('seekoApp.StudentProfile', ['ui.router','ui.bootstrap'])

        .controller('studentProfileCtrl', ['$scope','$uibModal','$state','$rootScope','$cookies','base64','$state','deviceDetector','$http','shareData','$window','localStorageService','screenSize','$stateParams',function($scope,$uibModal,$state,$rootScope,$cookies,base64,$state,deviceDetector,$http,shareData,$window,localStorageService,screenSize,$stateParams) {
            var  profile={};
            $scope.showspin=true;
            screenSize.rules = {
                superJumbo: '(max-width: 640px)'

            };
            if(screenSize.is('superJumbo')){
                console.log("inside super jumbo");
            }
            if(screenSize.is('superJumbo')){
                var startTeacherIndex=0;
                var endTeacherIndex=4;
            }
            else {
                var startTeacherIndex=0;
                var endTeacherIndex=6;
            }

                id=$stateParams.id;






            $scope.getCredential=function(value)
            {
                $http.post('/api/download',{url:$scope.dedecodedUri}).success(function(data){
                    console.log(data);
                });
            }
            $scope.months=['month','January','February','March','April','May','June','July','August','September','October','November','December'];
            console.log("id is "+id);
            $http.post('/api/viewEclasses', {id: id}).success(function (data) {
                console.log(JSON.stringify(data));
                teacherId=data.id;
                profile= data.profile.userData.profile;
                $scope.allClasses=data.classes.reverse();
                $scope.classes=$scope.allClasses.slice(startTeacherIndex, endTeacherIndex);
                console.log(data.classes);
                $scope.reviews = data.profile.userData.myReviews.reverse();
                console.log($scope.reviews);
                console.log(JSON.stringify(profile));
                $scope.dedecodedUri=[];
                $scope.credentialUrl=[];
                console.log(profile.teacherProfile);
                if(profile.teacherProfile.credential.length > 0) {
                    console.log("inside if");
                    for(var i=0;i<profile.teacherProfile.credential.length;i++){
                        console.log("inside loop");
                        var url = profile.teacherProfile.credential[i].credential;
                        $scope.credentialUrl.push(profile.teacherProfile.credential[i].originalFileName);
                        var decodedUrl = decodeURIComponent(url);
                        $scope.dedecodedUri.push(decodedUrl);

                    }

                    console.log($scope.credentialUrl);
                }
                else {
                    $scope.credentialUrl="Not Mentioned";
                }



                if(profile.location!= undefined)
                    $scope.location=profile.location;
                else
                    $scope.location="not mentioned";

                if(profile.firstName!= undefined  && profile.lastName!= undefined)
                    $scope.name=profile.firstName+" "+profile.lastName;
                else if(profile.lastName== undefined)
                    $scope.name=profile.firstName;

                if(profile.gender!=undefined)
                    $scope.gender=profile.gender;
                else
                    $scope.gender="not mentioned";

                if(profile.dob!=undefined){
                    userDate=new Date(profile.dob);
                    $scope.dob=userDate.getDate()+" "+$scope.months[userDate.getMonth() + 1]+" "+userDate.getFullYear();}
                else
                {
                    $scope.dob="not mentioned";
                }

                if(profile.aboutMe!= undefined)
                    $scope.aboutMe=profile.aboutMe;
                else
                    $scope.aboutMe="not mentioned";


                if(profile.teacherProfile.experience!=undefined)
                    $scope.experience=profile.teacherProfile.experience.split('/')[0]+"."+profile.teacherProfile.experience.split('/')[1]+" years";
                else
                    $scope.experience="not mentioned";
                $scope.profilePic=profile.profilePic;
                $scope.showspin=false;
            });
            $scope.getClassDetail=function(value){
                console.log(value);
                var request={"label":value.classData.className,"value":value._id,"teacherId":value.classData.teacherId};

                localStorageService.set('searchString',request);
                localStorageService.set('searchFrom', 'searchBox');
                // $rootScope.searchString=result.name;
                // if($state.current.data.isLogin.isLogin) {
                //     if (shareData.getProfile().userType == false) {
                //         $state.go('dashboard.classDetails', {}, {reload: "dashboard.classDetails"});
                //     }
                //     else {
                //         $state.go('teacherDashboard.classDetails', {}, {reload: "teacherDashboard.classDetails"});
                //     }
                // }
                // else
                //     $state.go('home.classDetails', {}, {reload: "home.classDetails"});

                     $state.go('classDetails', {id:value._id}, {reload: "classDetails"});

            }

            $scope.getUserReviews=function(review)
            {

                (function(review) {
                    console.log("id is "+review.userId);
                    $http.post('/api/getUserReviews',{id:review.userId}).success(function(data){
                        count=data.count;
                        review.count=count;
                    });

                })(review);

            }
            $scope.getNextClasses=function()
            {
                console.log("inside next clsses");
                if(($scope.allClasses.length>endTeacherIndex) && ($scope.allClasses.length>5) || ($scope.allClasses.length>endTeacherIndex) && ($scope.allClasses.length>3)) {
                    if(screenSize.is('superJumbo')) {
                        startTeacherIndex += 4;
                        endTeacherIndex += 4;
                    }
                    else{
                        startTeacherIndex += 6;
                        endTeacherIndex += 6;
                    }

                    $scope.classes=$scope.allClasses.slice(startTeacherIndex, endTeacherIndex);
                }
            }

            $scope.getPrevClasses=function() {
                console.log("inside prev clsses");
                $scope.btnPrevious = false;
                if (startTeacherIndex > 0) {
                    if(screenSize.is('superJumbo')) {
                        startTeacherIndex -= 4;
                        endTeacherIndex -= 4;
                    }
                    else{
                        startTeacherIndex -= 6;
                        endTeacherIndex -= 6;
                    }

                    $scope.classes=$scope.allClasses.slice(startTeacherIndex, endTeacherIndex);

                }

            }

            $scope.changeState = function (value) {
                var request={"label":value.classData.className,"value":value._id,"teacherId":value.classData.teacherId};
                console.log('request');
                localStorageService.set('searchString',request);
                localStorageService.set('searchFrom', 'searchBox');

                $state.go('classDetails',{id:value._id});


            }



        }]);
}());



