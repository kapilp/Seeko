(function(){
    angular.module('seekoApp.repeatClass', ['ui.router','ui.bootstrap'])



        .controller('repeatClassCtrl', ['$scope','$uibModal','$state','shareData','$rootScope','$uibModalInstance',function($scope,$uibModal,$state,shareData,$rootScope,$uibModalInstance) {
            console.log("inside repeat controller");
            $rootScope.classSummaryDetail='';
            $scope.monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            $scope.comma='';
            $scope.repeatData = {
                repeats: null,
                availableOptions: [
                    {value: 'Daily', name: 'Daily'},
                    {value: 'Every WeekDay(Monday To Friday)', name: 'Every WeekDay(Monday To Friday)'},
                    {value: 'Every Monday ,Wednesday ,and Friday', name: 'Every Monday ,Wednesday ,and Friday'},
                    {value: 'Every Tuesday and Thursday', name: 'Every Tuesday and Thursday'},
                    {value: 'Weekly', name: 'Weekly'}
                ]
            };
            var date=new Date();
            $scope.day=date.getDay();
            $scope.repeatOn=[];
            $scope.repeatOnSummary=[];
            $scope.dayArray=[
                {name:"Sunday",abbr:'s',selected:($scope.day==0),value:0},
                {name:"Monday",abbr:'m',selected:($scope.day==1),value:1},
                {name:"Tuesday",abbr:'t',selected:($scope.day==2),value:2},
                {name:"Wednesday",abbr:'w',selected:($scope.day==3),value:3},
                {name:"Thursday",abbr:'t',selected:($scope.day==4),value:4},
                {name:"Friday",abbr:'f',selected:($scope.day==5),value:5},
                {name:"Saturday",abbr:'s',selected:($scope.day==6),value:6}
            ];
            console.log($scope.dayArray);
            $scope.getinitialSummary=function() {
                console.log("inside getImitial");
                for (i = 0; i < $scope.dayArray.length; i++) {

                    if ($scope.dayArray[i].selected === true) {

                        $scope.repeatOn.push($scope.dayArray[i].value);
                        $scope.repeatOnSummary.push($scope.dayArray[i]);
                        for (j = 0; j < $scope.repeatOnSummary.length; j++)
                            $rootScope.classSummaryDetail = "Weekly On " + $scope.repeatOnSummary[j].name;

                    }

                }
                ;
            }
            $scope.hello=function(){
                console.log("Hello");
            }
            $scope.ends="never";
            $scope.showOccurences=false;




            $scope.startDate=(shareData.getStartClassDate().month)+"/"+shareData.getStartClassDate().day+"/"+shareData.getStartClassDate().year;
            $scope.untilDate="until "+$scope.monthNames[shareData.getStartClassDate().month-1]+" "+(shareData.getStartClassDate().day+1)+", "+shareData.getStartClassDate().year;



            $scope.hideOccurencesAndDate=function(){
                $scope.showOccurences=false;
                $scope.occurences='';
                $scope.fromDate='';
            }
            $scope.getOccurences=function(){
                $scope.occurences=35;
                $scope.showOccurences=! $scope.showOccurences;
                $scope.fromDate='';
            }
            $scope.getOccurenceDate=function(){
                console.log($scope);
                $scope.showOccurences=false;
                $scope.occurences='';
                $scope.fromDate=(shareData.getStartClassDate().month)+"/"+(shareData.getStartClassDate().day+1)+"/"+shareData.getStartClassDate().year;
            }

            $scope.closePopUp=function(){
                $uibModalInstance.close('ok');
                 $rootScope.toggleClass="switch switch-green";
            }

            $scope.summary=function(value){
                $rootScope.classSummaryDetail=value;
            }

            $scope.makeClassReoccuring=function() {
                if($scope.repeatOn.length==0 && $scope.repeatData.repeats == 'Weekly') {
                    $scope.errorMsg="Please select one day";
                }
                else{
                    console.log($scope.ends);
                    console.log($rootScope.classSummaryDetail);
                    if ($scope.ends == 'after')
                        $rootScope.classSummaryDetail = $rootScope.classSummaryDetail + ", " + $scope.occurences + " times ";
                    else if ($scope.ends == 'on')
                        $rootScope.classSummaryDetail = $rootScope.classSummaryDetail + ", " + $scope.untilDate;

                    $uibModalInstance.close('ok');
                    $rootScope.reoccuringDone = true;
                    $rootScope.showReoccuring = false;
                    $rootScope.toggleClass = "switch switch-green on off";
                    console.log($scope);
                    if ($scope.ends == 'never') {
                        $scope.ends = {
                            never: true,
                            after: '',
                            on: ''
                        };

                    }
                    else if ($scope.ends == 'after') {
                        $scope.ends = {
                            after: $scope.occurences,
                            never: false,
                            on: ''
                        };
                    }
                    else {
                        $scope.ends = {
                            on: new Date($scope.fromDate),
                            never: false,
                            after: ''
                        };
                    }
                    if ($scope.repeatData.repeats == 'Weekly') {
                        $scope.reoccuringClassData = {
                            repeats: $scope.repeatData.repeats,
                            startsOn: new Date(shareData.getStartClassDate().year, shareData.getStartClassDate().month - 1, shareData.getStartClassDate().day),
                            ends: $scope.ends,
                            repeatOn: $scope.repeatOn

                        }
                    }
                    else if ($scope.repeatData.repeats == 'Daily') {
                        $scope.reoccuringClassData = {
                            repeats: $scope.repeatData.repeats,
                            startsOn: new Date(shareData.getStartClassDate().year, shareData.getStartClassDate().month - 1, shareData.getStartClassDate().day),
                            ends: $scope.ends,
                            repeatOn: [0, 1, 2, 3, 4, 5, 6]


                        }
                    }
                    else if ($scope.repeatData.repeats == 'Every WeekDay(Monday To Friday)') {
                        $scope.reoccuringClassData = {
                            repeats: $scope.repeatData.repeats,
                            startsOn: new Date(shareData.getStartClassDate().year, shareData.getStartClassDate().month - 1, shareData.getStartClassDate().day),
                            ends: $scope.ends,
                            repeatOn: [1, 2, 3, 4, 5]


                        }
                    }
                    else if ($scope.repeatData.repeats == 'Every Monday ,Wednesday ,and Friday') {
                        $scope.reoccuringClassData = {
                            repeats: $scope.repeatData.repeats,
                            startsOn: new Date(shareData.getStartClassDate().year, shareData.getStartClassDate().month - 1, shareData.getStartClassDate().day),
                            ends: $scope.ends,
                            repeatOn: [1, 3, 5]


                        }
                    }
                    else if ($scope.repeatData.repeats == 'Every Tuesday and Thursday') {
                        $scope.reoccuringClassData = {
                            repeats: $scope.repeatData.repeats,
                            startsOn: new Date(shareData.getStartClassDate().year, shareData.getStartClassDate().month - 1, shareData.getStartClassDate().day),
                            ends: $scope.ends,
                            repeatOn: [2, 4]
                        }
                    }
                    console.log($scope.reoccuringClassData);
                    shareData.setReoccuringClasses($scope.reoccuringClassData);
                }



            }
            $scope.createDaysArray=function(value){
                var found = $scope.repeatOnSummary.some(function (el) {
                    return el.name === value.name;
                });
                if (!found) {
                    console.log("inside found");
                    console.log($rootScope.classSummaryDetail);
                    $scope.repeatOnSummary.push(value);
                    $scope.repeatOn.push(value.value);
                    console.log($scope.repeatOnSummary);
                    $scope.repeatOnSummary.sort(function(a, b){return a.value - b.value});
                    $rootScope.classSummaryDetail="Weekly On ";
                    console.log($scope.repeatOnSummary);
                    $scope.makeSummary($scope.repeatOnSummary);

                console.log($rootScope.classSummaryDetail);

                }
                else {
                    console.log($rootScope.classSummaryDetail);
                    var index = $scope.repeatOnSummary.indexOf(value);
                    $scope.repeatOn.splice(index, 1);
                    $scope.repeatOnSummary.splice(index, 1);
                    console.log($scope.repeatOnSummary);
                    $scope.repeatOnSummary.sort(function (a, b) {
                        return a.value - b.value
                    });
                    console.log($scope.repeatOnSummary);
                    $rootScope.classSummaryDetail="Weekly On ";
                    $scope.makeSummary($scope.repeatOnSummary);

                    console.log($rootScope.classSummaryDetail);
                }

            }

            $scope.makeSummary=function(repeatOnSummary){
                for (i = 0; i < repeatOnSummary.length; i++) {

                    if (i < repeatOnSummary.length - 1){
                        console.log($rootScope.classSummaryDetail);

                        $rootScope.classSummaryDetail = $rootScope.classSummaryDetail + repeatOnSummary[i].name + ", ";
                        console.log($rootScope.classSummaryDetail);
                    }
                    else{
                        $rootScope.classSummaryDetail = $rootScope.classSummaryDetail + repeatOnSummary[i].name;
                        console.log($rootScope.classSummaryDetail)
                    }
                }
            }
        }]);
}());


