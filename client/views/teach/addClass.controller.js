(function(){
    angular.module('seekoApp.addClass', ['ui.router','ui.bootstrap','angularTrix'])
        .controller('addClassCtrl', ['$scope','$rootScope','facebookService','gmailService','$window','$cookies','$http','Alertify','base64','$state','shareData','deviceDetector','$filter','Upload','localStorageService','$uibModal','$document','$anchorScroll','$location','$timeout',function($scope,$rootScope,facebookService,gmailService,$window,$cookies,$http,Alertify,base64,$state,shareData,deviceDetector,$filter,Upload,localStorageService,$uibModal,$document,$anchorScroll,$location,$timeout) {
            $scope.showSpin=true;
            if ($location.hash() !== 'header') {
                $location.hash('header');
            } else {
                $anchorScroll();
            }
            $scope.youtubeLinkError=false;
            shareData.getClassCategories().success(function(data){
                $scope.Supercategories=data.categories;
                console.log($scope.Supercategories);

            });

            $scope.timeError=false;
            $scope.categoryFlag=false;
            $document.on('click',function(){
                console.log("inside body click");

                $scope.$apply();
            });
            $rootScope.reoccuringDone=false;
            $rootScope.user={
                reoccuringClasses:false,
                summary:''
            };
            $scope.getCategory=function(){
                if($scope.categoryFlag)
                    $scope.hideMenu(event);
                else
                    $scope.getMenu();
            }
            shareData.setReoccuringClasses(false);

            // $scope.anotherGoodOne = 'https://www.youtube.com/watch?v=18-xvIjH8T4';

            $scope.isLoaded = false;
            $scope.openTo = false;
            $rootScope.openDate = false;
            $rootScope.toggleClass = "switch switch-green";
            $scope.bottomCss = '152px';
            $scope.selectedHour = "hh";
            $scope.minutes = ['mm', '10', '20', '30', '40', '50'];
            $scope.selectedMinutes = "mm";
            if(localStorageService.get('edit')!=false)
            {
                console.log('inside if');
                var result = localStorageService.get('edit');
                var requestJson = {
                    "index": shareData.getId(),
                    "name": result.name,
                    "type": "className",
                    "classId": result.index
                };

                console.log(requestJson);
                $http.post('/api/searchByClassIndex',requestJson).success(function(data){

                    console.log(data);
                    $rootScope.reoccuringDone=true;
                    $rootScope.classSummaryDetail=data.data.data.classData.summary;
                    $scope.categoryText=data.data.data.classData.category.categoryName;
                    $scope.category=data.data.data.classData.category;
                    console.log(data.data.data.classData.classDocument[0].classDocName);
                    $scope.anotherGoodOne=data.data.data.classData.classYoutubeLink;
                    $scope.editDuration=data.data.data.classData.timeDuration;
                    $scope.fromHour=getTimeString(00,'onEdit');
                    $scope.startTime=getEditStartTimeDetails(data.data.data.classData.timeDuration.split('-')[0],data.data.data.classData.timeDuration.split('-')[1]);
                    $scope.startToTime=getEditStopTimeDetails(data.data.data.classData.timeDuration.split('-')[0],data.data.data.classData.timeDuration.split('-')[1]);
                   // $scope.fromHour = getTimeString(parseInt(duration.split('-')[0].split(':')[1]), 'onEdit');
                    $http.get('/api/countries').success(function(countryData){
                        console.log("inside api countries");
                        $scope.countryList=countryData.countryList;
                        for(i=0;i<$scope.countryList.length;i++){
                            $scope.countryList[i].id=i;
                        }
                        $scope.countries={
                            countryOptions:$scope.countryList,
                            selectedOption:getCountryNameByName(data.data.data.classData.location)
                        };
                        $scope.user.location=$scope.countries.selectedOption.name;
                        console.log($scope.countries);
                    });

                    $scope.user={

                        classId:data.data.data._id,
                        edit:true,
                        className:data.data.data.classData.className,
                        createFromDate:(new Date(data.data.data.classData.fromDate).getMonth()+1)+"/"+(new Date(data.data.data.classData.fromDate).getDate())+"/"+new Date(data.data.data.classData.fromDate).getFullYear(),
                        description:data.data.data.classData.description,
                        DocName:data.data.data.classData.classDocument[0].classDocName,
                        youtubeVideo:data.data.data.classData.classYoutubeLink,
                        classHour:$scope.startTime,
                        classMinute:$scope.startToTime,
                        DocumentName:data.data.data.classData.classImageVideo[0].classDocName
                    };
                    console.log($scope.user);
                    $scope.showspin = false;
                });
            }
            else
            {
                $scope.create=true;
                $scope.selectedCountry = {};
                $scope.fromHour = getTimeString(00, 'onStart');
                $scope.categoryText = "Select Category";
                $http.get('/api/countries').success(function(data){
                    $scope.countryList=data.countryList;
                    for(i=0;i<$scope.countryList.length;i++){
                        $scope.countryList[i].id=i;
                    }
                    $scope.countries={
                        countryOptions:$scope.countryList,
                        selectedOption:getCountryName(localStorageService.get('country').country)
                    };
                    $scope.user.location=$scope.countries.selectedOption.name;
                    console.log($scope.countries);
                    $scope.showspin = false;
                });
            }



            function getCountryName(abbr){
            console.log("inside get country name");
            for(i=0;i<$scope.countryList.length;i++){
                if($scope.countryList[i].code==abbr)
                {
                    console.log($scope.countryList[i]);
                    return {id:$scope.countryList[i].id,code:abbr,name:$scope.countryList[i].name};
                }
            }
            }
            function getCountryNameByName(name){
                console.log(name);
                for(i=0;i<$scope.countryList.length;i++){
                    if($scope.countryList[i].name==name)
                    {
                        console.log($scope.countryList[i]);
                        return {id:$scope.countryList[i].id,code:$scope.countryList[i].code,name:name};
                    }
                }
            }




            $scope.getMenu=function(){
                console.log("inside show");
                $scope.categoryFlag=true;
            }
            $scope.hideMenu=function(event){
                console.log("inside hide");

                $scope.categoryFlag=false;
                //event.stopPropagation();
            }


            $scope.getCategories=function(categoryName,parentCategoryName,grandParentCategoryName){
                $scope.categoryText=categoryName;
              console.log("dwdwwdww"+grandParentCategoryName);
                $scope.category={
                    categoryName:categoryName,
                    parentCategoryName:parentCategoryName,
                    grandParentCategoryName:grandParentCategoryName
                };
                console.log($scope.category);
            }

            console.log($scope);

console.log("222222222222222222222"+ $scope.user.createFromDate);
            $scope.openEdit=function(){
                console.log('inside open edit');
                shareData.setReoccuringClasses(false);
                console.log(shareData.getReoccuringClasses());
                $rootScope.reoccuringDone=false;
                $rootScope.user.reoccuringClasses=false;
                $rootScope.user.summary='';
            }
            $scope.dataToggle=function(event) {
                console.log("inside toggle");
                console.log($rootScope.user.createFromDate);
                console.log("weereeeeeeeeeeeeeeeeeeeeinside toggle");
                $rootScope.openDate=true;
                console.log($rootScope.openDate==true);

                if($rootScope.openDate==true && $rootScope.user.createFromDate!=undefined) {
                    $rootScope.toggleClass = "switch switch-green on off";
                    var modalInstance = $uibModal.open({
                        templateUrl: '../../views/uiComponents/repeatClass.html',
                        controller: 'repeatClassCtrl'

                    });
                    $scope.modalInstance=modalInstance;
                }
                else {
                    $scope.modalInstance.close('ok');
                   //s $rootScope.toggleClass = "switch switch-green";
                }
            }
            function matchYoutubeUrl(url) {
                var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
                var matches = url.match(p);
                if(matches){
                    return matches[1];
                }
                return false;
            }


            function check(url){

                var id = matchYoutubeUrl(url);
                if(id!=false){
                    console.log("valid url");
                    $scope.youtubeLinkError=false;
                    return true
                }else{
                    console.log("invalid url");
                    $scope.youtubeLinkError=true;
                   return false;
                }
            }
            $scope.validUrl=function(url){
                if(url.length>0)
                check(url);
                else
                    $scope.youtubeLinkError=false;
            }
            function checkTime(toTime,fromTime){
                console.log("inside check time");
                if((toTime.split(':')[1] !=30) && (fromTime.split(':')[1]!=30)) {
                    console.log(toTime.split(':')[0] - fromTime.split(':')[0]);
                    if ((toTime.split(':')[0] - fromTime.split(':')[0] <= 2) &&(toTime.split(':')[0] - fromTime.split(':')[0] >0))
                    {
                        $scope.timeError=false;
                        return true;}

                    else
                        if((toTime.split(':')[0] - fromTime.split(':')[0] <=-22) && (toTime.split(':')[0] - fromTime.split(':')[0] >=-23.3))
                        {

                            $scope.timeError=false;
                            return true;
                        }
                        else{
                        $scope.timeError=true;
                        console.log("greater than 2 hrs");}
                }
                else if(toTime.split(':')[1]==30 && fromTime.split(':')[1]!=30){

                    console.log(parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]));
                    if(((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))<=2) && ((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))>0))
                    {$scope.timeError=false;return true;}
                    else{
                        if(((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))<=-22) && ((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))>=-23.3))
                        {
                            $scope.timeError=false;
                            return true;
                        }
                        else{
                            $scope.timeError=true;
                        return false;}
                    }
                }
                else if(fromTime.split(':')[1]==30 && toTime.split(':')[1]!=30)
                {
                    console.log(parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]));
                    if(((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))<=2) && ((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))>0))
                    {$scope.timeError=false; return true;}
                    else{
                        if(((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))<=-22) && ((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))>=-23.3))
                        {
                            $scope.timeError=false;
                            return true;
                        }
                        else{
                            $scope.timeError=true;
                            return false;
                        }
                            }
                }
                else if(fromTime.split(':')[1]==30 && toTime.split(':')[1]==30){
                    console.log(parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]));
                    if(((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))<=2) && ((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))>0))
                    {$scope.timeError=false;return true;}
                    else{
                        if(((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))<=-22) && ((parseFloat(toTime.split(':')[0]+"."+toTime.split(':')[1])-parseFloat(fromTime.split(':')[0]+"."+fromTime.split(':')[1]))>=-23.3))
                        {
                            $scope.timeError=false;
                            return true;
                        }
                           else{
                               $scope.timeError=true;
                        return false;
                           }
                    }
                }
            }
            $scope.addDetail=function () {
                console.log($scope.user);
                if ($scope.addForm.$valid && $scope.category!=undefined && check($scope.user.youtubeVideo) && checkTime($scope.user.classMinute,$scope.user.classHour)) {
                    $scope.showspin=true;
                    if ($location.hash() !== 'header') {
                        $location.hash('header');
                    } else {
                        $anchorScroll();
                    }
                    $scope.bottomCss = '199px';
                    console.log($scope.selectedOption);
                    if ($scope.user.classHour != undefined && $scope.user.classMinute != undefined)
                        $scope.user.timeDuration = $scope.user.classHour + "-" + $scope.user.classMinute;
                    $rootScope.user = $scope.user;
                    $rootScope.user.fromDate = new Date($rootScope.user.createFromDate);
                    $rootScope.user.teacherName = shareData.getProfile().firstName;
                    if (shareData.getReoccuringClasses() != undefined && shareData.getReoccuringClasses() != false) {
                        $rootScope.user.reoccuringClasses = true;
                        $rootScope.user.reoccuringClassData = shareData.getReoccuringClasses();
                        $rootScope.user.summary = $rootScope.classSummaryDetail;

                    }
                    else {
                        var str = $scope.user.classHour;
                        var test = str.split(":");
                        if (test[0] > '12') {
                            test[0] = test[0] - 12;
                            if (test[1] == '00') {
                                $scope.getTime = "@ " + test[0] + " PM";
                            }
                            if (test[1] == '30') {
                                $scope.getTime = "@ " + test[0] + ":" + test[1] + " PM";
                            }
                        }
                        else {
                            $scope.getTime = "@ " + test[0] + ":" + test[1] + " AM";
                        }
                        var str1 = "" + $rootScope.user.fromDate;
                        var test1 = str1.split(" ");
                        var test2 = test1[0] + " " + test1[1] + " " + test1[2] + ", " + test1[3] + " ";
                        $scope.getDate = test2;
                        $rootScope.user.summary = $scope.getDate + $scope.getTime;
                        console.log("final summary " + $rootScope.user.summary);
                    }


                    var image = $rootScope.user.image;
                    var doc = $rootScope.user.doc;
                    console.log(doc);
                    if (doc == undefined && image == undefined) {
                        console.log("inside if");
                        console.log($rootScope.user);
                        console.log("inside if 12345678");
                        $rootScope.user.document = '';
                        $rootScope.user.classImage='';
                        $rootScope.user.id = shareData.getId();
                        $rootScope.user.category = $scope.category;

                        //console.log($rootScope.user.classImage);
                        console.log("==========" + JSON.stringify($rootScope.user));
                        $rootScope.user.id = shareData.getId();
                        console.log("id is " + JSON.stringify($rootScope.user));
                        $http.post('/api/createClass', $rootScope.user)
                            .success(function (data) {
                                $scope.user = {};
                                $rootScope.user = {};
                                $rootScope.classSummaryDetail = '';
                                $rootScope.reoccuringDone = false;
                                console.log(data);
                                if (data.success == true) {
                                    $scope.showspin=false;
                                    var request = {
                                        "label": data.classData.data.classData.className,
                                        "value": data.classData.data._id,
                                        "teacherId": shareData.getId()
                                    };
                                    console.log(request);
                                    localStorageService.set('searchString', request);
                                    localStorageService.set('searchFrom', 'searchBox');
                                    $state.go('classDetails',{id:data.classData.data._id});
                                }
                            })
                    }

                    else {
                        Upload.upload({
                            url: '/api/teacherUpload',
                            data: {doc: doc, image:image}
                        }).success(function (data) {

                            console.log(shareData);
                            console.log("========data" + JSON.stringify(data));
                            console.log("profile pic is ====" + JSON.stringify(data.profilePic));
                            console.log("credential is ====" + JSON.stringify(data.credential));

                            if (data.success == true) {
                                if(data.credential!=undefined)
                                $rootScope.user.document = data.credential;
                                else
                                    $rootScope.user.document='';
                                if(data.profilePic!=undefined){
                                $rootScope.user.classImage = data.profilePic;
                                }
                                else{
                                    $rootScope.user.classImage='';
                                }
                                $rootScope.user.id = shareData.getId();
                                $rootScope.user.category = $scope.category;

                                //console.log($rootScope.user.classImage);
                                console.log("==========" + JSON.stringify($rootScope.user));
                                $rootScope.user.id = shareData.getId();
                                console.log("id is " + JSON.stringify($rootScope.user));
                                $http.post('/api/createClass', $rootScope.user)
                                    .success(function (data) {
                                        $scope.user = {};
                                        $rootScope.user = {};
                                        $rootScope.classSummaryDetail = '';
                                        $rootScope.reoccuringDone = false;
                                        console.log(data);
                                        if (data.success == true) {

                                            $scope.showspin=false;
                                            var request = {
                                                "label": data.classData.data.classData.className,
                                                "value": data.classData.data._id,
                                                "teacherId": shareData.getId()
                                            };
                                            console.log(request);
                                            localStorageService.set('searchString', request);
                                            localStorageService.set('searchFrom', 'searchBox');
                                            $state.go('classDetails',{id:data.classData.data._id});
                                        }
                                    })
                            }

                        })
                    }


                }
                else {
                    $scope.addForm.submitted = true;
                    if ($location.hash() !== 'header') {
                        $location.hash('header');
                    } else {
                        $anchorScroll();
                    }

                }
            }
            $scope.editDetail=function () {
                console.log("editDetail");
                console.log(shareData.getReoccuringClasses());
                console.log($scope.user);
                if ($scope.addForm.$valid && $scope.category!=undefined && check($scope.user.youtubeVideo) && checkTime($scope.user.classMinute,$scope.user.classHour)) {
                    $scope.showspin=true;
                    if ($location.hash() !== 'header') {
                        $location.hash('header');
                    } else {
                        $anchorScroll();
                    }
                    $scope.bottomCss = '199px';
                    console.log($scope.selectedOption);
                    if ($scope.user.classHour != undefined && $scope.user.classMinute != undefined)
                        $scope.user.timeDuration = $scope.user.classHour + "-" + $scope.user.classMinute;
                    $rootScope.user = $scope.user;
                    $rootScope.user.fromDate = new Date($rootScope.user.createFromDate);
                    $rootScope.user.teacherName = shareData.getProfile().firstName;
                    if (shareData.getReoccuringClasses() != undefined && shareData.getReoccuringClasses() != false) {
                        $rootScope.user.reoccuringClasses = true;
                        $rootScope.user.reoccuringClassData = shareData.getReoccuringClasses();
                        $rootScope.user.summary = $rootScope.classSummaryDetail;

                    }
                    else {
                        var str = $scope.user.classHour;
                        var test = str.split(":");
                        if (test[0] > '12') {
                            test[0] = test[0] - 12;
                            if (test[1] == '00') {
                                $scope.getTime = "@ " + test[0] + " PM";
                            }
                            if (test[1] == '30') {
                                $scope.getTime = "@ " + test[0] + ":" + test[1] + " PM";
                            }
                        }
                        else {
                            $scope.getTime = "@ " + test[0] + ":" + test[1] + " AM";
                        }
                        var str1 = "" + $rootScope.user.fromDate;
                        var test1 = str1.split(" ");
                        var test2 = test1[0] + " " + test1[1] + " " + test1[2] + ", " + test1[3] + " ";
                        $scope.getDate = test2;
                        $rootScope.user.summary = $scope.getDate + $scope.getTime;
                        console.log("final summary " + $rootScope.user.summary);
                    }


                    var image = $rootScope.user.image;
                    var doc = $rootScope.user.doc;
                    console.log(doc);
                    if (doc == undefined && image == undefined) {
                        console.log($rootScope.user);
                        console.log("inside if 12345678");
                        $rootScope.user.id = shareData.getId();
                        $rootScope.user.category = $scope.category;

                        //console.log($rootScope.user.classImage);
                        console.log("==========" + JSON.stringify($rootScope.user));
                        $rootScope.user.id = shareData.getId();
                        console.log("id is " + JSON.stringify($rootScope.user));
                        $http.post('/api/editClass',{data:$rootScope.user})
                            .success(function (data) {
                                $scope.user = {};
                                $rootScope.user = {};
                                $rootScope.classSummaryDetail = '';
                                $rootScope.reoccuringDone = false;
                                console.log(data);
                                if (data.success == true) {
                                    $scope.showspin=false;
                                    var request = {
                                        "label": data.className,
                                        "value": data.classId,
                                        "teacherId": shareData.getId()
                                    };
                                    console.log(request);
                                    localStorageService.set('searchString', request);
                                    localStorageService.set('searchFrom', 'searchBox');
                                    $state.go('classDetails',{id:data.classId});
                                }
                            })
                    }

                    else {
                        console.log("inside else");
                        Upload.upload({
                            url: '/api/teacherUpload',
                            data: {doc: doc, image:image}
                        }).success(function (data) {

                            console.log(shareData);
                            console.log("========data" + JSON.stringify(data));
                            console.log("profile pic is ====" + JSON.stringify(data.profilePic));
                            console.log("credential is ====" + JSON.stringify(data.credential));

                            if (data.success == true) {
                                if(data.credential!=undefined)
                                    $rootScope.user.document = data.credential;
                                else
                                    $rootScope.user.document='';
                                if(data.profilePic!=undefined){
                                    $rootScope.user.classImage = data.profilePic;
                                }
                                else{
                                    $rootScope.user.classImage='';
                                }
                                $rootScope.user.id = shareData.getId();
                                $rootScope.user.category = $scope.category;

                                //console.log($rootScope.user.classImage);
                                console.log("==========" + JSON.stringify($rootScope.user));
                                $rootScope.user.id = shareData.getId();
                                console.log("id is " + JSON.stringify($rootScope.user));
                                $http.post('/api/editClass',{data:$rootScope.user})
                                    .success(function (data) {
                                        $scope.user = {};
                                        $rootScope.user = {};
                                        $rootScope.classSummaryDetail = '';
                                        $rootScope.reoccuringDone = false;
                                        console.log(data);
                                        if (data.success == true) {

                                            $scope.showspin=false;
                                            var request = {
                                                "name": data.className,
                                                "index": data.classId,
                                                "teacherId": shareData.getId()
                                            };
                                            console.log(request);
                                            localStorageService.set('searchString', request);
                                            localStorageService.set('searchFrom', 'searchBox');
                                            $state.go('teacherDashboard.classDetails');
                                        }
                                    })
                            }

                        })
                    }


                }
                else {
                    $scope.addForm.submitted = true;
                    if ($location.hash() !== 'header') {
                        $location.hash('header');
                    } else {
                        $anchorScroll();
                    }

                }
            }

    function getTimeString(minutes,type,time) {
            time = new Date().getHours();


            console.log((time * 60) + 30);
            var hh = Math.floor((time * 60) / 60); // getting hours of day in 0-24 format
            var mm = ((time * 60) % 60); // getting minutes of the hour in 0-55 format
            if(type == 'onStart') {
                $scope.startTime = ("0" + (hh)).slice(-2) + ':' + ("0" + mm).slice(-2);
                $scope.startToTime = ("0" + ((Math.floor(((time * 60) + 60) / 60)))).slice(-2) + ':' + ("0" + ((((time * 60) + 60) % 60))).slice(-2);
                console.log($scope.startTime);
                console.log($scope.startToTime);
            }
            var x = 30; //minutes interval
            var fromTimes = []; // time array
            var tt = minutes; // start time


            for (var i = 0; tt < 24 * 60; i++) {
                var hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
                var mm = (tt % 60); // getting minutes of the hour in 0-55 format
                fromTimes[i] = ("0" + (hh)).slice(-2) + ':' + ("0" + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
                tt = tt + x;
            }
            $scope.toTimes = fromTimes;
            return fromTimes;

      }

      function getEditStartTimeDetails(startTime,stopTime){
          console.log(startTime);
          console.log(stopTime);
          var hh = Math.floor((parseInt(startTime.split(':')[0]) * 60) / 60);
          var mm = (parseInt(startTime.split(':')[1]) % 60);
          $scope.startTime = ("0" + (hh)).slice(-2) + ':' + ("0" + mm).slice(-2);
          console.log($scope.startTime);
          return $scope.startTime;
      }
      function getEditStopTimeDetails(startTime,stopTime){
                var hh = Math.floor((parseInt(stopTime.split(':')[0]) * 60) / 60);
                var mm = (parseInt(stopTime.split(':')[1]) % 60);
                $scope.startToTime = ("0" + (hh)).slice(-2) + ':' + ("0" + mm).slice(-2);
                console.log($scope.startToTime);
                return $scope.startToTime;
            }
            $scope.trix;
            //= '<div>Lorem ipsum dolor sit amet,<strong>consectetur</strong>adipiscing elit<del>Praesent lacus diam</del>, fermentum et venenatis quis, suscipit sed nisi. In pharetra sem eget orci posuere pretium.<em>Integer</em>non eros<strong><em>scelerisque</em></strong>, consequat lacus id, rutrum felis. Nulla elementum felis urna, at placerat arcu ultricies in.</div><ul><li>Proin elementum sollicitudin sodales.</li><li>Nam id erat nec nibh dictum cursus.</li></ul><blockquote>In et urna eros. Fusce molestie, orci vel laoreet tempus, sem justo blandit magna, at volutpat velit lacus id turpis.<br>Quisque malesuada sem at interdum congue. Aenean dapibus fermentum orci eu euismod.</blockquote><div></div>';


            var events = ['trixInitialize', 'trixChange', 'trixSelectionChange', 'trixFocus', 'trixBlur', 'trixFileAccept', 'trixAttachmentAdd', 'trixAttachmentRemove'];

            for (var i = 0; i < events.length; i++) {
                $scope[events[i]] = function(e) {
                    console.info('Event type:', e.type);
                }
            };

            var createStorageKey, host, uploadAttachment;

            $scope.trixAttachmentAdd = function(e) {
                var attachment;
                attachment = e.attachment;
                if (attachment.file) {
                    return uploadAttachment(attachment);
                }
            }

            host = "https://d13txem1unpe48.cloudfront.net/";

            uploadAttachment = function(attachment) {
                var file, form, key, xhr;
                file = attachment.file;
                key = createStorageKey(file);
                form = new FormData;
                form.append("key", key);
                form.append("Content-Type", file.type);
                form.append("file", file);
                xhr = new XMLHttpRequest;
                xhr.open("POST", host, true);
                xhr.upload.onprogress = function(event) {
                    var progress;
                    progress = event.loaded / event.total * 100;
                    return attachment.setUploadProgress(progress);
                };
                xhr.onload = function() {
                    var href, url;
                    if (xhr.status === 204) {
                        url = href = host + key;
                        return attachment.setAttributes({
                            url: url,
                            href: href
                        });
                    }
                };
                return xhr.send(form);
            };

            createStorageKey = function(file) {
                var date, day, time;
                date = new Date();
                day = date.toISOString().slice(0, 10);
                time = date.getTime();
                return "tmp/" + day + "/" + time + "-" + file.name;
            };
        }]);
}());
