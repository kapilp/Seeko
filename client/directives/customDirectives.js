angular.module('customDirectives',[])

    .directive("selectBox", function($timeout) {
        return {
            restrict: 'E',
            templateUrl: '../../views/uiComponents/selectBox.html',
            scope: {
                initValue:'=initialValue',
                selectType: '=type',
                selectedValue: "=ngModel"
            }
            ,
            link: function(scope, element, attrs,ctrl) {



                scope.$watch('selectedValue', function(item) {
                      //scope.item=item;
                    console.log("currentItem is123", item);
                      $timeout(function() {
                        console.log("currentItem is", item);

                      });
                    });
                // if ($(".select").length) {
                //     console.log("123");
                //     $(".select").selectbox();
                // }


            }
        };


    })
.directive('stectStyle',function($timeout){
    return {
        restrict: 'E',
        templateUrl: '../../views/uiComponents/selectBox.html',
     scope:{},
        link: function(scope, element, attrs,ctrl) {

         scope.$watch(function () {
             if ($(".select").length) {
                 console.log("123");
                 $(".select").selectbox();
             }

         })



        }
    };
})
    .directive('shiftFocus', function() {
    return {
        restrict: 'A',
        link: function(scope,elem,attrs) {
console.log("inside focus");
            elem.bind('keydown', function(e) {
                console.log(e);
                var code = e.keyCode || e.which;
                if (code === 13) {
                    console.log("inside 13");
                    e.preventDefault();
                    elem.parent('li').next().find('input')[0].focus();
                }
            });
        }
    }
})
    .directive('focus',
    function($timeout) {
        return {
            scope : {
                trigger : '@focus'
            },
            link : function(scope, element) {
                scope.$watch('trigger', function(value) {
                    console.log("inside watcher"+value);
                    if (value === "true") {
                        $timeout(function() {
                            element[0].focus();
                        });
                    }
                });
            }
        };
    })
.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                console.log("inside 13");
                scope.$apply(function (){
                    console.log(attrs.myEnter);
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
}).directive('offSet', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs,$document,$window) {
            console.log("inside directive");
            var box = element[0].getBoundingClientRect();
            var docEl = element[0].ownerDocument;
            var body = docEl.body;
                console.log(docEl.clientTop);
                console.log(box.top);
            console.log(docEl.clientLeft);
                console.log(box.left);


            // var scrollTop = getWindow(docEl).pageYOffset || docEl.scrollTop || body.scrollTop;
            // var scrollLeft = getWindow(docEl).pageXOffset || docEl.scrollLeft || body.scrollLeft;
            //
            // var clientTop = docEl.clientTop || body.clientTop || 0;
            // var clientLeft = docEl.clientLeft || body.clientLeft || 0;
            //
            // var top  = box.top +  scrollTop - clientTop;
            // var left = box.left + scrollLeft - clientLeft;
            console.log(box.top);
            console.log(box.left);
        }
    }
})
    .directive('starRate', function ($timeout) {
        return {
            restrict: "E",
            templateUrl: "../../views/uiComponents/starRate.html",
            scope: {avgStarCount: '=count'}
        };
    })
    .directive('autoCompleteIsolated',function($http,localStorageService,$rootScope,shareData,$state,$document,$window){
        return{
            restrict:'A',
            require:'ngModel',
            scope:{
                ngModel:'=',
                where:'@where'
            },
            link:function(scope,elem,attrs){
                $document.on('click',function(){
                    scope.ngModel='';
                    scope.$apply();
                });
                console.log(scope.where);
                console.log("inside element");
                console.log(elem);
                console.log($('#searchBox'));
                elem.autocomplete({
                    appendTo:$('#searchBox'),
                    source:function(request,response){
                        $http.post('/api/searchClasses',{key:request.term}).success(function(data){
                            console.log(data.classData);
                            response(data.classData);
                        })

                    },
                    select:function(event,ui){
                        event.preventDefault();
                        scope.$apply(function(){
                            scope.ngModel=ui.item.label;
                        });
                        $rootScope.searchString=ui.item.label;
                        shareData.setSearchData(ui.item);
                        localStorageService.set('searchString',ui.item);
                        console.log(ui.item);
                        if(ui.item.type=='className'){
                            localStorageService.set('searchFrom', 'searchBox');
                            scope.ngModel='';
                            // $rootScope.searchString=result.name;
                            // if(scope.where=='home')
                            // $state.go('home.classDetails',{},{reload:"home.classDetails"});
                            // else if(scope.where=='dashboard'){
                            //     $state.go('dashboard.classDetails',{},{reload:"dashboard.classDetails"});
                            // }
                            // else{
                            data = shareData.getSearchData();
                                $state.go('classDetails',{id:data.value},{reload:"classDetails"});
                            // }
                        }
                        else if(ui.item.type=='grandParentCategoryName' ||ui.item.type=='parentCategoryName' || ui.item.type=='categoryName'){
                            localStorageService.set('visitType','search');
                            scope.ngModel='';
                            if(scope.where=='home')
                            $window.location.href='home/dashboard';
                            else if(scope.where=='dashboard')
                                $window.location.href='dashboard';
                            else
                               $window.location.href='teacherDashboard';
                        }
                        else{
                            localStorageService.set('profile', ui.item.value);
                            localStorageService.set('visitType','login');
                            scope.ngModel='';
                            $state.go('profile',{id:ui.item.value});
                        }
                    },
                    focus:function(event,ui){
                        event.preventDefault();
                        scope.$apply(function(){
                            scope.ngModel=ui.item.label;
                        });

                    },
                    open: function() {
                        $('.ui-menu').width(272);
                        $('.ui-menu').css("left","252px");

                    }
                })
            }
        }
    })
    .directive('getScroll',function(){
        return{
            restrict:'A',
            link:function(scope,elem,attr){

                    elem.mCustomScrollbar({
                        axis : "x",
                        advanced:{autoExpandHorizontalScroll:true},
                        callbacks:{
                            onCreate:function(){
                                console.log("inside init");
                                $('.mCSB_dragger').width(400);

                            }
                        }

                    });

            }
        }
    })
    .directive('chatScroll',function(){
        return{
            restrict:'A',
            link:function(scope,elem,attr){

                elem.mCustomScrollbar({
                    axis : "y",
                    advanced:{
                        updateOnContentResize:true
                    },
                    callbacks:{
                        onCreate:function(){
                            console.log("inside init group");
                            console.log(this.mcs);

                            $('.mCSB_scrollTools .mCSB_draggerRail').css('background-color','transparent');
                            $('.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background-color','rgba(0,0,0,0.14)');
                            $('.mCSB_scrollTools .mCSB_draggerContainer').css('width','25px');
                        }

                    },
                    scrollInertia:2
                });
                scope.$on('new Group Message',function(event,args){
                    console.log("inside on chat scroll");
                    elem.mCustomScrollbar("scrollTo","last");
                });


            }
        }
    })
    .directive('chatScrollIndividual',function(){
        return{
            restrict:'A',
            link:function(scope,elem,attr){

                elem.mCustomScrollbar({
                    axis : "y",
                    advanced:{
                        updateOnContentResize:true
                    },
                    callbacks:{
                        onCreate:function(){

                            console.log("inside init individual");

                            $('.mCSB_scrollTools .mCSB_draggerRail').css('background-color','transparent');
                            $('.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background-color','rgba(0,0,0,0.14)');
                           $('.mCSB_scrollTools .mCSB_draggerContainer').css('width','25px');
                        }

                    },
                    scrollInertia:2
                });
                scope.$on('new Individual Message',function(event,args){
                    console.log("inside on individual message");
                    elem.mCustomScrollbar("scrollTo","last");
                });


            }
        }
    })
    .directive('startBackground',function(){
        return{
            restrict:'C',
            link:function(scope,elem,attrs){
                console.log("inside element12234343");
                $('html').css('height','100%');
                $('#content').css('height','100vh');
                $('body').css({'background':'#f3f3f3 url(/assets/images/chatImages/bg.png) center center no-repeat', 'height':'100%'});
                $('#wrapper').css({'min-width': '1124'});
                scope.$on('back',function(event,args){
                    console.log("inside on+++++++++++");
                    $('body').css({'background':'none','background-color':'#f3f3f3'});
                    $('html').removeAttr('style');
                    //scope.$emit('hello',{'success':true});
                });

            }
        }
    })
    .directive('profileWidth',function(shareData){
        return{
            restrict:'C',
            link:function(scope,elem,attrs){
                if(shareData.getProfile().isFbLogin || shareData.getProfile().isGoogleLogin)
                $('#profile').css('width','50%');
                else
                    $('#profile').css('width','71%');

            }
        }
    })

    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })

    .directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9]/g, '');

                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    })
    .directive('simpleAccordion', function () {
    return {
        // attribute
        restrict: 'A',
        scope: {
            // default: '400ms'
            // options: 'milliseconds', 'slow', or 'fast'
            toggleSpeed: '@toggleSpeed',
            slideUpSpeed: '@slideUpSpeed',
            // default: 'swing'
            // options: 'swing', 'linear'
            toggleEasing: '@toggleEasing',
            slideUpEasing: '@slideUpEasing'
        },
        link: function (scope, element, attrs) {
            element.find('.blog-tittle').click(function () {
                var elem = $(this);
                elem.next().slideToggle(scope.toggleSpeed, scope.toggleEasing);
                $(".accordion-content").not($(this).next()).slideUp(scope.slideUpSpeed, scope.slideUpEasing);
            });
        }
    };
})
    // .directive('ngChangeBody', function () {
    //     return function (scope, element, attrs) {
    //         element.bind("click", function (event) {
    //             event.stopPropagation();
    //             $('body').css('overflow-x','hidden');
    //             $('body').css('overflow-y','auto');
    //
    //         });
    //
    //     };
    // })
;