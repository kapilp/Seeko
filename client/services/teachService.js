 angular.module('teachSrc',[])
     .factory("TS",function($http){
         return{
             getFavouriteData:function(){
                var request=$http({method:'POST',url:'api/favourites'});
                return request;
            }
         };
     });
