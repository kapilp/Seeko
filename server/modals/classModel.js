var mongoose=require('../node_modules/mongoose');
var bcrypt = require('../node_modules/bcrypt');
var textSearch = require('mongoose-text-search');


var Schema=mongoose.Schema;
var classDetail=new Schema({
  className  :  {type:String , index:true} ,
   category  : {
                grandParentCategoryName:{type:String , index:true},
                parentCategoryName:{type:String , index:true},
                categoryName:{type:String , index:true}
              },
  summary  :  {type:String , index:true},
  reoccuringClasses  : Boolean ,
  reoccuringClassData:{repeats:String,repeatEvery:Number,reoccuringCount:Number,repeatOn:{type : Array , "default" : []},startsOn:Date,ends:{never:Boolean,after:Number,on:Date}},
  toDate  :  Date ,
  fromDate: Date,
  time  :  Date ,
  description: {type:String , index:true},
  timeDuration  :  String ,
  classYoutubeLink:String,
  location:String,
  isClassStart: Boolean,
  classDocument :  [{classDocName:String ,classDocUrl:String}],
  classImageVideo : [{classDocName:String ,classDocUrl:String}],
  uploadVideo  :  { type : Array , "default" : [] },
  studentId  : { type : Array , "default" : [] },
  teacherId  :  String ,
  teacherName : {type:String , index:true},
  classCost  :  Number,
  reviewsCount:Number,
  avgStarCount: Number,
  reviews:[{comment:String,date:String,starCount:Number,userName:String,userId:String,profilePic:String,userType:Boolean}],
  cancelClass:Boolean,
  updatedAt : Date ,
  createdAt : Date

});
var classSchema = new Schema({
  classData:classDetail
});
classSchema.plugin(textSearch);
classSchema.index({'$** ': 'text'});

module.exports=mongoose.model('classSchema',classSchema);
