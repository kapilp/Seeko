/**
 * Created by Arpit on 08/04/17.
 */
var path = require('path');
var url = require('url');
var express = require('express');
// var minimist = require('minimist');
var router = express.Router();
var ws = require('ws');
var kurento = require('kurento-client');
var fs    = require('fs');
var https = require('https');
var _ = require('lodash');

// var server = require('../app.js').server;



module.exports = function (server) {

    console.log("in calling routes");
    var ws_uri = 'ws://52.221.176.236:8888/kurento';
    /*
     * Definition of global variables.
     */
    var idCounter = 0;
    // var candidatesQueue = {};
    var kurentoClient = null;
    var presenter = null;
    var viewers = [];
    var noPresenterMessage = 'No active presenter. Try again later...';

    /*
     * Server startup
     */
// var asUrl = url.parse(argv.as_uri);
// var port = asUrl.port;
// var server = https.createServer(options, app).listen(port, function() {
//     console.log('Kurento Tutorial started');
//     console.log('Open ' + url.format(asUrl) + ' with a WebRTC capable browser');
// });
    console.log("in calling routes");
    var wss = new ws.Server({
        server: server,
        path: '/one2many'
    });

    function nextUniqueId() {
        idCounter++;
        return idCounter.toString();
    }

    /*
     * Management of WebSocket messages
     */
    var classIds =[];
    var isTeacher ;
    wss.on('connection', function(ws) {

        var sessionId = nextUniqueId();
        console.log('Connection received with sessionId ' + sessionId);

        ws.on('error', function(error) {
            console.log('Connection ' + sessionId + ' error');
            stop(sessionId);
        });

        ws.on('close', function() {
            console.log('Connection ' + sessionId + ' closed');
            stop(sessionId);
        });

        ws.on('message', function(_message) {
            var message = JSON.parse(_message);
            console.log('Connection ' + sessionId + ' received message ', message);

            switch (message.id) {
                case 'switchScreenToVideo':
                    switchScreenToVideo(sessionId, ws, message.sdpOffer, message.classId, function(error, sdpAnswer) {
                        if (error) {
                            console.log(error);
                            return ws.send(JSON.stringify({
                                id : 'presenterResponse',
                                response : 'rejected',
                                message : error
                            }));
                        }
                        ws.send(JSON.stringify({
                            id : 'presenterResponse',
                            response : 'accepted',
                            sdpAnswer : sdpAnswer
                        }));
                    });
                    break;

                case 'screenPresenter':
                    startScreenPresenter(sessionId, ws, message.sdpOffer, message.classId, function(error, sdpAnswer) {
                        if (error) {
                            console.log(error);
                            return ws.send(JSON.stringify({
                                id : 'presenterResponse',
                                response : 'rejected',
                                message : error
                            }));
                        }
                        ws.send(JSON.stringify({
                            id : 'presenterResponse',
                            response : 'accepted',
                            sdpAnswer : sdpAnswer
                        }));
                    });
                    break;

                case 'presenter':
                    startPresenter(sessionId, ws, message.sdpOffer, message.classId, function(error, sdpAnswer) {
                        if (error) {
                            console.log(error);
                            return ws.send(JSON.stringify({
                                id : 'presenterResponse',
                                response : 'rejected',
                                message : error
                            }));
                        }
                        ws.send(JSON.stringify({
                            id : 'presenterResponse',
                            response : 'accepted',
                            sdpAnswer : sdpAnswer
                        }));
                    });
                    break;

                case 'viewer':
                    console.log("viewer calling");
                    startViewer(sessionId, ws, message.sdpOffer,message.classId, function(error, sdpAnswer) {
                        console.log("session id is "+sessionId);
                        if (error) {
                            return ws.send(JSON.stringify({
                                id : 'viewerResponse',
                                response : 'rejected',
                                message : error
                            }));
                        }

                        ws.send(JSON.stringify({
                            id : 'viewerResponse',
                            response : 'accepted',
                            sdpAnswer : sdpAnswer
                        }));
                    });
                    break;

                case 'stop':
                    stop(sessionId);
                    break;

                case 'onIceCandidate':

                    onIceCandidate(sessionId, message.candidate, message.classId);
                    break;

                default:
                    ws.send(JSON.stringify({
                        id : 'error',
                        message : 'Invalid message ' + message
                    }));
                    break;
            }
        });
    });

    /*
     * Definition of functions
     */

// Recover kurentoClient for the first time.
    function getKurentoClient(callback) {
        if (kurentoClient !== null) {
            return callback(null, kurentoClient);
        }

        kurento(ws_uri, function(error, _kurentoClient) {
            if (error) {
                console.log("Could not find media server at address " + ws_uri);
                return callback("Could not find media server at address" + ws_uri
                    + ". Exiting with error " + error);
            }

            kurentoClient = _kurentoClient;
            callback(null, kurentoClient);
        });
    }

    function switchScreenToVideo(sessionId, ws, sdpOffer,classId, callback) {

        clearCandidatesQueue(sessionId, classId);

        isTeacher = true;
        function callViewersToShareVideo() {
            for (var i = 0; i < classIds.length; i++) {

                // if user is teacher

                if (classIds[i].id == sessionId) {

                    for (var j = 0; j < classIds[i].viewers.length; j++) {

                        console.log("callViewersToShareVideo");


                        console.log("viewers is " + classIds[i].viewers[j]);
                        var viewer = classIds[i].viewers[j];
                        console.log("viewer is " + classIds[i].viewers[j].ws);
                        if (viewer.ws) {
                            console.log("calling viewers");
                            viewer.ws.send(JSON.stringify({
                                id: 'switchToVideo'
                            }));

                            console.log("classIds[i].viewers[j] "+classIds[i].viewers[j].length);
                            classIds[i].viewers[j]="x";
                            console.log("classIds[i].viewers[j] "+classIds[i].viewers[j].length);
                        }

                        // if(j == classIds[i].viewers.length-1){
                        //     classIds[i].viewers = [];
                        // }

                    }

                }
            }

        }

        try {

            var isClassPresent;
            if (classIds.length == 0) {
                console.log("length us 0");

                presenter = {
                    classId: classId,
                    id: sessionId,
                    pipeline: null,
                    webRtcEndpoint: null,
                    viewers: [],
                    candidatesQueue: []
                }
                classIds.push(presenter);
                console.log(classIds);


            } else {
                for (var i = 0; i < classIds.length; i++) {

                    if (classIds[i].classId == classId) {

                        console.log("class exist");
                        isClassPresent = true;
                    } else {
                        if (i == classIds.length - 1 && isClassPresent == undefined) {
                            presenter = {
                                classId: classId,
                                id: sessionId,
                                pipeline: null,
                                webRtcEndpoint: null,
                                viewers: [],
                                candidatesQueue: []
                            }
                            classIds.push(presenter);
                        }
                    }
                }

            }


            getKurentoClient(function (error, kurentoClient) {
                if (error) {
                    console.log("error1");
                    stop(sessionId);
                    return callback(error);
                }

                // if (presenter === null) {
                // 	stop(sessionId);
                // 	return callback(noPresenterMessage);
                // }

                kurentoClient.create('MediaPipeline', function (error, pipeline) {

                    console.log("screenshare pipeline is " + JSON.stringify(pipeline));
                    if (error) {
                        console.log("error2");
                        stop(sessionId);
                        return callback(error);
                    }

                    // if (presenter === null) {
                    // 	stop(sessionId);
                    // 	return callback(noPresenterMessage);
                    // }

                    var j = 0;
                    getClasses(j);

                    function getClasses(j) {

                        if (j < classIds.length && classIds[j].classId == classId) {
                            // console.log("ddwd "+JSON.stringify(classIds[j]));
                            classIds[j].pipeline = pipeline;
                            pipeline.create('WebRtcEndpoint', function (error, webRtcEndpoint) {
                                if (error) {
                                    console.log("error3");
                                    stop(sessionId);
                                    return callback(error);
                                }

                                // if (presenter === null) {
                                //     stop(sessionId);
                                //     return callback(noPresenterMessage);
                                // }
                                classIds[j].webRtcEndpoint = webRtcEndpoint;


                                console.log("shifting 1");

                                console.log(classIds[j].candidatesQueue[sessionId]);

                                if (classIds[j].candidatesQueue[sessionId]) {
                                    console.log("shifting 1");

                                    while (classIds[j].candidatesQueue[sessionId].length) {
                                        console.log("shifting 2");

                                        console.log(classIds[j].candidatesQueue);

                                        var candidate = classIds[j].candidatesQueue[sessionId].shift();
                                        console.log("candidate is");
                                        console.log(candidate);
                                        webRtcEndpoint.addIceCandidate(candidate);
                                        setTimeout(callViewersToShareVideo, 6000);

                                    }
                                }

                                webRtcEndpoint.on('OnIceCandidate', function (event) {
                                    var candidate = kurento.getComplexType('IceCandidate')(event.candidate);
                                    ws.send(JSON.stringify({
                                        id: 'iceCandidate',
                                        candidate: candidate
                                    }));
                                });

                                webRtcEndpoint.processOffer(sdpOffer, function (error, sdpAnswer) {
                                    if (error) {
                                        console.log("error4");
                                        stop(sessionId);
                                        return callback(error);
                                    }

                                    // if (presenter === null) {
                                    //     stop(sessionId);
                                    //     return callback(noPresenterMessage);
                                    // }

                                    callback(null, sdpAnswer);
                                });

                                webRtcEndpoint.gatherCandidates(function (error) {
                                    if (error) {
                                        console.log("error5");
                                        stop(sessionId);
                                        return callback(error);
                                    }
                                });
                            });

                        } else {
                            getClasses(j + 1);
                        }
                    }


                });
            });

        }
        catch (exception)
        {
            console.log(exception);
            return;

        }


    }

    function startScreenPresenter(sessionId, ws, sdpOffer,classId, callback) {

        clearCandidatesQueue(sessionId, classId);

       isTeacher = true;
     function callViewersToShareScreen() {
         for (var i = 0; i < classIds.length; i++) {

             // if user is teacher

             if (classIds[i].id == sessionId) {

                 for (var j = 0; j < classIds[i].viewers.length; j++) {

                      console.log("callViewersToShareScreen xxxx");


                     console.log(classIds[i].viewers[j]);
                     var viewer = classIds[i].viewers[j];
                     console.log("viewer is " + classIds[i].viewers[j].ws);
                     if (viewer.ws) {
                         console.log("calling viewers");
                         viewer.ws.send(JSON.stringify({
                             id: 'screenShare'
                         }));
                         console.log("classIds[i].viewers[j] "+classIds[i].viewers.length);
                         classIds[i].viewers[j]="x";
                         console.log("classIds[i].viewers[j] "+classIds[i].viewers.length);
                     }

                     // if(j == classIds[i].viewers.length-1){
                     //     classIds[i].viewers = [];
                     // }

                 }

             }
         }

     }

        try {

            var isClassPresent;
            if (classIds.length == 0) {
                console.log("length us 0");

                presenter = {
                    classId: classId,
                    id: sessionId,
                    pipeline: null,
                    webRtcEndpoint: null,
                    viewers: [],
                    candidatesQueue: []
                }
                classIds.push(presenter);
                console.log(classIds);


            } else {
                for (var i = 0; i < classIds.length; i++) {

                    if (classIds[i].classId == classId) {

                        console.log("class exist");
                        isClassPresent = true;
                    } else {
                        if (i == classIds.length - 1 && isClassPresent == undefined) {
                            presenter = {
                                classId: classId,
                                id: sessionId,
                                pipeline: null,
                                webRtcEndpoint: null,
                                viewers: [],
                                candidatesQueue: []
                            }
                            classIds.push(presenter);
                        }
                    }
                }

            }


            getKurentoClient(function (error, kurentoClient) {
                if (error) {
                    console.log("error1");
                    stop(sessionId);
                    return callback(error);
                }

                // if (presenter === null) {
                // 	stop(sessionId);
                // 	return callback(noPresenterMessage);
                // }

                kurentoClient.create('MediaPipeline', function (error, pipeline) {

                    console.log("screenshare pipeline is " + JSON.stringify(pipeline));
                    if (error) {
                        console.log("error2");
                        stop(sessionId);
                        return callback(error);
                    }

                    // if (presenter === null) {
                    // 	stop(sessionId);
                    // 	return callback(noPresenterMessage);
                    // }

                    var j = 0;
                    getClasses(j);

                    function getClasses(j) {

                        if (j < classIds.length && classIds[j].classId == classId) {
                            // console.log("ddwd "+JSON.stringify(classIds[j]));
                            classIds[j].pipeline = pipeline;
                            pipeline.create('WebRtcEndpoint', function (error, webRtcEndpoint) {
                                if (error) {
                                    console.log("error3");
                                    stop(sessionId);
                                    return callback(error);
                                }

                                // if (presenter === null) {
                                //     stop(sessionId);
                                //     return callback(noPresenterMessage);
                                // }
                                classIds[j].webRtcEndpoint = webRtcEndpoint;


                                console.log("shifting 1");

                                console.log(classIds[j].candidatesQueue[sessionId]);

                                if (classIds[j].candidatesQueue[sessionId]) {
                                    console.log("shifting 1");

                                    while (classIds[j].candidatesQueue[sessionId].length) {
                                        console.log("shifting 2");

                                        console.log(classIds[j].candidatesQueue);

                                        var candidate = classIds[j].candidatesQueue[sessionId].shift();
                                        console.log("candidate is");
                                        console.log(candidate);
                                        webRtcEndpoint.addIceCandidate(candidate);
                                        setTimeout(callViewersToShareScreen, 6000);

                                    }
                                }

                                webRtcEndpoint.on('OnIceCandidate', function (event) {
                                    var candidate = kurento.getComplexType('IceCandidate')(event.candidate);
                                    ws.send(JSON.stringify({
                                        id: 'iceCandidate',
                                        candidate: candidate
                                    }));
                                });

                                webRtcEndpoint.processOffer(sdpOffer, function (error, sdpAnswer) {
                                    if (error) {
                                        console.log("error4");
                                        stop(sessionId);
                                        return callback(error);
                                    }

                                    // if (presenter === null) {
                                    //     stop(sessionId);
                                    //     return callback(noPresenterMessage);
                                    // }

                                    callback(null, sdpAnswer);
                                });

                                webRtcEndpoint.gatherCandidates(function (error) {
                                    if (error) {
                                        console.log("error5");
                                        stop(sessionId);
                                        return callback(error);
                                    }
                                });
                            });

                        } else {
                            getClasses(j + 1);
                        }
                    }


                });
            });

        }
        catch (exception)
        {
            console.log(exception);
            return;

        }


    }
    var recorder;
    function startPresenter(sessionId, ws, sdpOffer,classId, callback) {
        clearCandidatesQueue(sessionId, classId);
        isTeacher = true;

        // if (presenter !== null) {
        // 	stop(sessionId);
        // 	return callback("Another user is currently acting as presenter. Try again later ...");
        // }
        try {

        var isClassPresent;
        if (classIds.length == 0) {
            console.log("length is 0");

            presenter = {
                classId: classId,
                id: sessionId,
                pipeline: null,
                webRtcEndpoint: null,
                viewers: [],
                candidatesQueue: []
            }
            classIds.push(presenter);
            console.log(classIds);


        } else {
            for (var i = 0; i < classIds.length; i++) {

                if (classIds[i].classId == classId) {

                    isClassPresent = true;
                }
                    if (i == classIds.length - 1 && isClassPresent == undefined) {
                        presenter = {
                            classId: classId,
                            id: sessionId,
                            pipeline: null,
                            webRtcEndpoint: null,
                            viewers: [],
                            candidatesQueue: []
                        }
                        classIds.push(presenter);

                }
            }

        }


        getKurentoClient(function (error, kurentoClient) {
            if (error) {
                console.log("error1");
                stop(sessionId);
                return callback(error);
            }

            // if (presenter === null) {
            // 	stop(sessionId);
            // 	return callback(noPresenterMessage);
            // }

            kurentoClient.create('MediaPipeline', function (error, pipeline) {

                console.log("pipeline is " + JSON.stringify(pipeline));
                if (error) {
                    console.log("error2");
                    stop(sessionId);
                    return callback(error);
                }

                // if (presenter === null) {
                // 	stop(sessionId);
                // 	return callback(noPresenterMessage);
                // }

                var j = 0;
                getClasses(j);


                function getClasses(j) {

                    if (j < classIds.length && classIds[j].classId == classId) {
                        // console.log("ddwd "+JSON.stringify(classIds[j]));
                        classIds[j].pipeline = pipeline;
                        var elements =
                            [
                                {type: 'RecorderEndpoint', params: {uri : 'file:///mnt/s3/'+classIds[j].classId+".mp4"}},
                                {type: 'WebRtcEndpoint', params: {}}
                            ]
                        pipeline.create(elements, function (error, elements) {
                            if (error) {
                                console.log("error3");
                                stop(sessionId);
                                return callback(error);
                            }
                             recorder = elements[0]
                            var webRtcEndpoint   = elements[1]
                            // if (presenter === null) {
                            //     stop(sessionId);
                            //     return callback(noPresenterMessage);
                            // }
                            classIds[j].webRtcEndpoint = webRtcEndpoint;


                            console.log("shifting 1");

                            console.log(classIds[j].candidatesQueue[sessionId]);

                            if (classIds[j].candidatesQueue[sessionId]) {
                                console.log("shifting 1");

                                while (classIds[j].candidatesQueue[sessionId].length) {
                                    console.log("shifting 2");

                                    console.log(classIds[j].candidatesQueue);

                                    var candidate = classIds[j].candidatesQueue[sessionId].shift();
                                    console.log("candidate is");
                                    console.log(candidate);
                                    webRtcEndpoint.addIceCandidate(candidate);
                                }
                            }

                            webRtcEndpoint.on('OnIceCandidate', function (event) {
                                var candidate = kurento.getComplexType('IceCandidate')(event.candidate);
                                ws.send(JSON.stringify({
                                    id: 'iceCandidate',
                                    candidate: candidate
                                }));
                            });

                            webRtcEndpoint.processOffer(sdpOffer, function (error, sdpAnswer) {
                                if (error) {
                                    console.log("error4");
                                    stop(sessionId);
                                    return callback(error);
                                }

                                // if (presenter === null) {
                                //     stop(sessionId);
                                //     return callback(noPresenterMessage);
                                // }

                                callback(null, sdpAnswer);
                            });

                            webRtcEndpoint.gatherCandidates(function (error) {
                                if (error) {
                                    console.log("error5");
                                    stop(sessionId);
                                    return callback(error);
                                }
                            });
                            kurentoClient.connect(webRtcEndpoint, webRtcEndpoint, recorder, function(error) {
                                if (error) return onError(error);

                                console.log("Connected");

                                recorder.record(function(error) {
                                    if (error) return onError(error);

                                    console.log("record");
                                });
                            });
                        });

                    } else {
                        getClasses(j + 1);
                    }
                }


            });
        });

    }
    catch (exception)
    {
        console.log(exception);
        return;

    }


    }

    function startViewer(sessionId, ws, sdpOffer,classId, callback) {
        clearCandidatesQueue(sessionId, classId);


        isTeacher =false;
        // if (presenter === null) {
        // 	stop(sessionId);
        // 	return callback(noPresenterMessage);
        // }
        try {
        var i = 0;
        getClassViewers(i);


        function getClassViewers(i) {
            if (i < classIds.length && classIds[i].classId == classId) {
                console.log(classIds[i]);
                classIds[i].pipeline.create('WebRtcEndpoint', function (error, webRtcEndpoint) {
                    if (error) {
                        console.log("error6");
                        stop(sessionId);
                        return callback(error);
                    }


                    // viewers[sessionId] = {
                    //     "webRtcEndpoint" : webRtcEndpoint,
                    //     "ws" : ws
                    // }

                    // if (presenter === null) {
                    //     stop(sessionId);
                    //     return callback(noPresenterMessage);
                    // }
                    var viewers = {
                        "webRtcEndpoint": webRtcEndpoint,
                        "ws": ws,
                        sessionId: sessionId
                    }
                    console.log("pushing");
                    classIds[i].viewers.push(viewers);


                    console.log("shifting 1 xxx");

                    console.log(classIds[i].candidatesQueue[sessionId]);


                    if (classIds[i].candidatesQueue[sessionId]) {
                        console.log("shifting 1");

                        while (classIds[i].candidatesQueue[sessionId].length) {
                            console.log("shifting 2");

                            console.log(classIds[i].candidatesQueue);

                            var candidate = classIds[i].candidatesQueue[sessionId].shift();
                            console.log("candidate is");
                            console.log(candidate);
                            webRtcEndpoint.addIceCandidate(candidate);
                        }
                    }


                    webRtcEndpoint.on('OnIceCandidate', function (event) {
                        var candidate = kurento.getComplexType('IceCandidate')(event.candidate);
                        ws.send(JSON.stringify({
                            id: 'iceCandidate',
                            candidate: candidate
                        }));
                    });

                    webRtcEndpoint.processOffer(sdpOffer, function (error, sdpAnswer) {
                        if (error) {
                            console.log("error8");
                            stop(sessionId);
                            return callback(error);
                        }
                        // if (presenter === null) {
                        //     stop(sessionId);
                        //     return callback(noPresenterMessage);
                        // }

                        classIds[i].webRtcEndpoint.connect(webRtcEndpoint, function (error) {
                            if (error) {
                                console.log("error9");
                                stop(sessionId);
                                return callback(error);
                            }
                            // if (presenter === null) {
                            //     stop(sessionId);
                            //     return callback(noPresenterMessage);
                            // }

                            callback(null, sdpAnswer);
                            webRtcEndpoint.gatherCandidates(function (error) {
                                if (error) {
                                    console.log("error10");
                                    stop(sessionId);
                                    return callback(error);
                                }
                            });
                        });
                    });
                });
            } else {

                getClassViewers(i + 1);
            }
        }
     }   catch (exception) {
            console.log(exception);
            return;
        }



    }

    function clearCandidatesQueue(sessionId,classId) {
        for(var i=0; i<classIds.length; i++){
            if(classIds[i].classId == classId){
                    console.log("clearing");
                console.log(classIds[i].candidatesQueue);
                if (classIds[i].candidatesQueue[sessionId]) {
                    delete classIds[i].candidatesQueue[sessionId];
                    console.log("cleared");
                    console.log(classIds[i].candidatesQueue[sessionId]);
                }

            }
        }
        // if (candidatesQueue[sessionId]) {
        // 	delete candidatesQueue[sessionId];
        // }
    }

    function stop(sessionId) {

        console.log("stopping");
        recorder.stop();
        console.log(classIds[0].viewers.length);
        for(var i=0 ; i<classIds.length; i++) {

            // if user is teacher

            if (classIds[i].id == sessionId) {

                for (var j = 0; j < classIds[i].viewers.length; j++) {

                    // console.log(classIds[i].viewers);


                    console.log("viewers is " + classIds[i].viewers[j]);
                    var viewer = classIds[i].viewers[j];
                    console.log("viewer is " + classIds[i].viewers[j].ws);
                    if (viewer.ws) {
                        viewer.ws.send(JSON.stringify({
                            id: 'stopCommunication'
                        }));
                    }

                    classIds[i].pipeline.release();
                    clearCandidatesQueue(sessionId, classIds[i].classId);
                    // classIds.splice(i, 1);
                     classIds[i].id=null;
                    classIds[i].classId = null;
                    classIds[i].viewers =[];
                }

            // if user is student

            }else{

                for (var j = 0; j < classIds[i].viewers.length; j++) {

                    if (classIds[i].viewers[j].sessionId == sessionId) {

                        classIds[i].viewers[j].webRtcEndpoint.release();
                        classIds[i].viewers.splice(j, 1);
                        clearCandidatesQueue(sessionId, classIds[i].classId);

                    }
                }

            }

                    // presenter = null;
                    // viewers = [];
                    // clearCandidatesQueue(sessionId, classIds[i].classId);
        }

        if(classIds.length == 0){

            console.log('Closing kurento client');
            kurentoClient.close();
            kurentoClient = null;
        }



        //
        // if (presenter !== null && presenter.id == sessionId) {
        // for (var i in viewers) {
        // 	var viewer = viewers[i];
        // 	if (viewer.ws) {
        // 		viewer.ws.send(JSON.stringify({
        // 			id : 'stopCommunication'
        // 		}));
        // 	}
        // }
        // presenter.pipeline.release();
        // presenter = null;
        // viewers = [];
        //
        // } else if (viewers[sessionId]) {
        // viewers[sessionId].webRtcEndpoint.release();
        // delete viewers[sessionId];
        // }
        //
        // clearCandidatesQueue(sessionId);
        //
        // if (viewers.length < 1 && !presenter) {
        //     console.log('Closing kurento client');
        //     kurentoClient.close();
        //     kurentoClient = null;
        // }
    }


    function onIceCandidate(sessionId, _candidate, classId) {
        var candidate = kurento.getComplexType('IceCandidate')(_candidate);

        // console.log("classId is "+JSON.stringify(classIds));
        console.log("sessionid is "+sessionId);
        console.log(classIds);
        var classData = _.find(classIds,["classId",classId]);
        console.log("classData is "+classData);
        // for(var i=0; i<classIds.length; i++) {
        if (_.includes(classData,classId)) {
            console.log(classData.viewers.length);

            //check whether viewers

            console.log("isTeacher");

            console.log(isTeacher);

            // if (classIds[i] && classIds[i].id == sessionId && classIds[i].webRtcEndpoint) {
            //
            //     console.info('Sending presenter candidate');
            //     classIds[i].webRtcEndpoint.addIceCandidate(candidate);
            // } else
            if (classData.viewers.length >= 1 && isTeacher == false && isTeacher != undefined) {
                console.log("else if");
                console.log(classData.viewers.length);
                // for (var j = 0; j < classData.viewers.length; j++) {
                    _.forEach(classData.viewers,function (viewers) {
                        if (viewers.sessionId == sessionId && viewers.webRtcEndpoint) {
                            console.info('Sending viewer candidate');
                            viewers.webRtcEndpoint.addIceCandidate(candidate);
                        }
                    })


                // }
            }
            else {
                console.info('Queueing candidate');
                if (!classData.candidatesQueue[sessionId]) {
                    classData.candidatesQueue[sessionId] = [];
                }
                classData.candidatesQueue[sessionId].push(candidate);
                console.log(classData.candidatesQueue.length);

            }



            // }
        }

    }

    return router;

}