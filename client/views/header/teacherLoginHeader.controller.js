(function(){
    angular.module('seekoApp.teacherLoginHeader', ['ui.router','ui.bootstrap'])
        .controller('teacherLoginHeaderCtrl', ['$http','$scope','$uibModal','$state','$rootScope','$cookies','base64','$state','shareData','$window','localStorageService','$document','$location','$anchorScroll',function($http,$scope,$uibModal,$state,$rootScope,$cookies,base64,$sate,shareData,$window,localStorageService,$document,$location,$anchorScroll) {
            $rootScope.isUserLogin=true;
            // if ($location.hash() !== 'header') {
            //     $location.hash('header');
            // } else {
            //     $anchorScroll();
            // }
            shareData.getClassCategories().success(function(data){
                $scope.Supercategories=data.categories;
                console.log($scope.Supercategories);
                $scope.categoryFlag=false;

            });
            $scope.categoryFlag=false;
console.log("inside teacher login header");
            $scope.uId=shareData.getId();
            $scope.searchList=false;
            $scope.searchMsgLi=false;
            $scope.classHover=$state.current.data.hover;
            $scope.dashboardHover=$state.current.data.dashboardHover;
            $scope.calendarHover=$state.current.data.calendarHover;

            $scope.getCategory=function(){
                if($scope.categoryFlag)
                    $scope.hideMenu(event);
                else
                    $scope.getMenu();
            }

            $scope.getMenu=function(){
                $scope.categoryFlag=true;

            }
            $scope.hideMenu=function(event){
                //$scope.Supercategories=[];
                $scope.categoryFlag=false;
                event.stopPropagation();
            }
            $document.on('click',function(){
                console.log("inside body click");
               // $scope.Supercategories=[];
                $scope.categoryFlag=false;
                $scope.$apply();
            });

            // console.log(authenticated);
            // console.log(profile);
            $scope.ImgSrc=shareData.getProfile().profilePic;
            $scope.userName=shareData.getProfile().firstName;
            $scope.isLearner=shareData.getProfile().userType;
            $rootScope.isALearner=$scope.isLearner;
            $scope.verifyUser=function () {
                return $scope.isLearner;

            };
            $scope.logout=function () {
                $http.post('/api/logout',{"browserToken":shareData.getToken()}).success(function(data){
                    if(data.success==true)
                    localStorageService.set('loginFromSeeko','');
                        $state.go('home');
                });
            };


            $scope.fetchRecordingUrl=function()
            {
                $http.post('/getS3fsContent')
                    .success(function (data) {
                $rootScope.recordings=data.recordingUrl;
                        var modalInstance = $uibModal.open({
                            templateUrl: '../../views/header/recording.html',
                            controller: 'recordingCtrl as recording',
                            backdrop: false
                        });

                    });
            }
            // $scope.joinClass=function() {
            //     var userProfile=shareData.getUserProfile(shareDaeta.getToken());
            //     $http.post('/api/joinClass',userProfile).success(function(data)
            //     {
            //         console.log(data);
            //     });
            // };
            // $scope.startClass=function() {
            //     var userProfile=shareData.getUserProfile(shareData.getToken());
            //     $http.post('/api/startClass',userProfile).success(function(data)
            //     {
            //
            //     });
            // };
            $scope.applyToTeach=function(){
              $state.go($state.current);
                var modalInstance = $uibModal.open({
                    templateUrl: '../../views/learn/applyToTeach.html',
                    controller:'ApplyToTeachCtrl',
                    backdrop: false,
                });

            }


    // search functionality
            $scope.getSearchResult=function(value){
                console.log("inside change data");
                if(value==''){
                    $scope.searchList=false;
                }
                else {
                    $http.post('/api/searchClasses',{key:value}).success(function(data){
                        console.log(data);
                        if(data.success==true) {
                            $scope.searchMsgLi=false;
                            $scope.searchResult = data.classData;
                            $scope.searchList = true;
                        }
                        else {
                            $scope.searchList = true;
                            $scope.searchMsgLi=true;
                            $scope.searchMsg="No Results Found";
                        }
                    })
                }

            }

            $scope.searchByClassName =function (value) {

                var result = {
                    'index':'','type':'categoryName','name':value
                }
                localStorageService.set('searchString',result);
                $state.go('teacherDashboard',{'type':'search'},{reload: true});


            }

            $scope.getExactSearchResult=function(result)
            {
                console.log(result);
                $scope.searchList = false;
                $rootScope.searchString=result.name;
                shareData.setSearchData(result);
                localStorageService.set('searchString',result);
                console.log(result);
                if(result.type=='className'){
                    localStorageService.set('searchFrom', 'searchBox');
                    // $rootScope.searchString=result.name;
                    $state.go('teacherDashboard.classDetails',{},{reload:"teacherDashboard.classDetails"});

                }
                else if(result.type=='grandParentCategoryName' || result.type=='parentCategoryName' || result.type=='categoryName'){
                    $state.go('teacherDashboard',{'type':'search'},{reload: true});
                }

            };
            $scope.getSearchCategory=function(value,type){
                localStorageService.set('topJediStatus','');
                if(value.name=='All Categories'){
                    localStorageService.set('stateReview', '');
                    $state.go('viewall');
                }
                else {
                    localStorageService.set('visitType', 'search');
                    console.log(type, value);
                    var result = {};
                    if (type == 'grandParentCategoryName')
                        result = {value: shareData.getId(), label: value.name, type: "grandParentCategoryName"};
                    else if (type == 'categoryName')
                        result = {value: shareData.getId(), label: value.name, type: "categoryName"};
                    else
                        result = {value: shareData.getId(), label: value.name, type: "parentCategoryName"};
                    console.log(result);
                    localStorageService.set('trendingStatus', result);
                    shareData.setSearchData(result);
                    localStorageService.set('searchString', result);
                    $state.go('teacherDashboard', {'type': 'search'}, {reload: true});
                }

            }

            angular.element(document).on('click', function () {

                $scope.searchList = false;
                $scope.searchMsgLi=false;
                $rootScope.searchString='';
                $scope.$apply();
            });

            // navigation view

            $scope.getMyClassView=function(){
                console.log("inside class view");
                $scope.dashboardHover=false;
                $scope.classHover=true;
                $scope.calendarHover=false;
                localStorageService.set('visitType','login');
                $state.go("teacherDashboard.myClasses");
            }
            $scope.getMyProfile=function(){
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=false;
                localStorageService.set('profile','profile');
                localStorageService.set('visitType','login');
                $state.go("profile",{'id':shareData.getId()});
            }
            $scope.getMyAccount=function(){
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=false;
                localStorageService.set('visitType','login');
                $state.go('teacherDashboard.editAccount');
            }
            $scope.getMyNotifications=function(){
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=false;
                localStorageService.set('visitType','login');
                $state.go('teacherDashboard.notifications');
            }
            $scope.editMyProfile=function(){
                $scope.dashboardHover=false;
                $scope.classHover=false;
                localStorageService.set('visitType','login');
                $state.go('editTeacher');
            }
            $scope.getMyFavouriteClasses=function(){
                console.log("inside my favourite classes");
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=false;
                localStorageService.set('visitType','login');
                $state.go('teacherDashboard.favourites');
            }
            $scope.createNewClass=function(){
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=false;
                localStorageService.set('edit',false);
                localStorageService.set('visitType','login');
                $state.go('teacherDashboard.addClass',{},{reload:"teacherDashboard.addClass"});
            }
            $scope.goToDashboard=function(){
                $scope.dashboardHover=true;
                $scope.classHover=false;
                $scope.calendarHover=false;
                console.log("inside dashboard");
                localStorageService.set('visitType','login');
                var currentType={
                    type:'all'
                }
                localStorageService.set('trendingStatus',currentType);
                $window.location.href='teacherDashboard'
            }
            $scope.getCalendar=function(){
                $scope.dashboardHover=false;
                $scope.classHover=false;
                $scope.calendarHover=true;
                localStorageService.set('visitType','login');
                $state.go('teacherDashboard.calendar');
            }

        }]);
}());
