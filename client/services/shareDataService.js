
angular.module('shareDataService',[])
    .service('shareData', ['$cookies','base64','$q','$http','$rootScope',
        function($cookies,base64,$q,$http,$rootScope){

        this.setPreviousState=function(stateName){
            this.previousStateName=stateName;
        }
        this.getPreviousState=function(){
            return this.previousStateName;
        }
        this.setUserProfile=function(value){
            this.profile=value.data;
            this.id=value.id;
            this.data=value;

        }
        this.getFullData=function(){
            return this.data;
        }
        this.getProfile=function(){
            return this.profile;
        }
        this.getId=function()
        {
            return this.id;
        }
        this.setChatUserData=function(id,mail,name){
            this.chatUser={
                studentName:name,
                 studentId:id,
                studentMail:mail,
                message: ""
            };
        }
        this.getChatUserData=function(){
            console.log(this.chatUser);
            return this.chatUser;
        }

        this.getUserProfile=function()
        {
            if(this.token==undefined)
                return false;
            else{
            var encodedProfile = this.token.split('.')[1];
            var profile =base64.decode(encodedProfile);
            userProfile=(JSON.parse(profile));
            return userProfile;
            }
        }
        this.getCookies=function($timeout,$q)
        {
           // var that=this;
            var seeko=$cookies.get('seeko');
            this.token=seeko;
            if(seeko)
            {
                var encodedProfile = this.token.split('.')[1];
                // var profile =base64.decode(encodedProfile);
                // userProfile=(JSON.parse(profile));
                // $http.post('/api/edit', {email: userProfile.email}).success(function (data) {
                //     that.setUserProfile(data);
                // });

                return true;
            }
            else{


                return false;
            }


        }
        this.getToken=function(){
            return this.token;
        }
        this.setClasses=function(data){
            this.class=data;
        }
        this.getClasses=function(){

            return this.class;
        }
        this.setReoccuringClasses=function(value){
            this.reoccuringClasses=value;
        }
        this.getReoccuringClasses=function(){
            return this.reoccuringClasses;
        }
        this.setStartClassDate=function(month,day,year){
            this.date={
                month:month,
                day:day,
                year:year
            };
        }
        this.getStartClassDate=function(){
            return this.date;
        }
        this.getClassCategories=function()
        {
            this.superCategories=[];
            var response=$http.post('/api/classesCategories');
            return response;
        }
        this.setSearchData=function(value){
            this.searchResult=value;
        }
        this.getSearchData=function(){
            return this.searchResult;
        }
    }]);
