
'use strict';

angular.module('datePickerDirective',[])
    .directive('datePicker', function () {
    return {
        restrict: "E",
        templateUrl: "../../views/uiComponents/datepicker.html",
        controller: function($scope, calendarHelper,shareData,$rootScope) {
            console.log($scope);
            $scope.showDatePicker = false;
            $scope.repeatShadow=false;


            $scope.calendar = {
                year: new Date().getFullYear(),
                month: new Date().getMonth(),
                monthName: calendarHelper.getMonthName(new Date().getMonth()),
                isPreviousYear:false,
                isPreviousMonth:false
            };
            console.log($scope.calendar);

            $scope.days = calendarHelper.getCalendarDays(new Date().getFullYear(), new Date().getMonth());


            $scope.nextMonth = function () {

                calendarHelper.incrementCalendarMonth($scope.calendar);

                $scope.calendar.monthName = calendarHelper.getMonthName($scope.calendar.month);
                $scope.days = calendarHelper.getCalendarDays($scope.calendar.year, $scope.calendar.month);

                if((parseInt($scope.calendar.month)<parseInt(new Date().getMonth())) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){

                    $scope.calendar.isPreviousYear = false;
                }
                else if((parseInt($scope.calendar.month)>=parseInt(new Date().getMonth())) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){

                    $scope.calendar.isPreviousYear = true;
                }

            }

            $scope.previousMonth = function () {

                if(parseInt($scope.calendar.year)==parseInt(new Date().getFullYear())) {

                    if (parseInt($scope.calendar.month) == parseInt(new Date().getMonth()+1)) {
                        $scope.calendar.isPreviousYear = false;
                        $scope.calendar.isPreviousMonth = false;
                    }
                }
                if((parseInt($scope.calendar.month)<parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){

                    $scope.calendar.isPreviousYear = false;
                }
                else if((parseInt($scope.calendar.month)>=parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){
                    $scope.calendar.isPreviousYear = true;
                }
                calendarHelper.decrementCalendarMonth($scope.calendar);
                $scope.calendar.monthName = calendarHelper.getMonthName($scope.calendar.month);
                $scope.days = calendarHelper.getCalendarDays($scope.calendar.year, $scope.calendar.month);

            }

            $scope.nextYear = function () {
                $scope.calendar.isPreviousYear=true;
                $scope.calendar.isPreviousMonth=true;
                $scope.calendar.year++;
                $scope.days = calendarHelper.getCalendarDays($scope.calendar.year, $scope.calendar.month);
            }

            $scope.previousYear = function () {

                $scope.calendar.year--;
                $scope.days = calendarHelper.getCalendarDays($scope.calendar.year, $scope.calendar.month);
                if(parseInt($scope.calendar.year)==parseInt(new Date().getFullYear())){
                    $scope.calendar.isPreviousYear=false;

                    if(parseInt($scope.calendar.month+1)<=parseInt(new Date().getMonth()+1)){
                    $scope.calendar.isPreviousMonth=false;}
                }
                if((parseInt($scope.calendar.month)<parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){

                    $scope.calendar.isPreviousYear = false;
                }
                else if((parseInt($scope.calendar.month)>=parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){
                    $scope.calendar.isPreviousYear = true;
                }
            }

            $scope.selectDate = function (day) {

                $scope.element.val(day + "/" + ($scope.calendar.month + 1) + "/" + + $scope.calendar.year);
                $scope.user.createFromDate=($scope.calendar.month + 1) + "/" + day + "/" + + $scope.calendar.year;
                // $rootScope.showReoccuring=true;
                shareData.setStartClassDate($scope.calendar.month + 1,day,$scope.calendar.year);

                $scope.showDatePicker = false;
            }
            $scope.getPreviousDate=function(day) {
                    if(day=='')
                        return true;
                if(parseInt(new Date().getFullYear())== parseInt($scope.calendar.year)){
                    if (parseInt(new Date().getMonth() + 1) == parseInt($scope.calendar.month + 1)) {
                        if (parseInt(day) > parseInt(new Date().getDate())) {

                            return false;
                        }
                        else if (parseInt(day) < parseInt(new Date().getDate())) {

                            return true;
                        }
                    }
                }

            }


        },
        link: function(scope, element, attrs, controller) {

            var forElement = angular.element(document.getElementById(attrs.for));

            scope.element = forElement;
            forElement.on('click', function() {

                scope.$apply(function() { scope.showDatePicker = true;});
            });


           angular.element(document.getElementsByTagName("body")[0]).on('click', function() {


               scope.$apply( function() { scope.showDatePicker = false; })
           });
            forElement.on('click', function(event) {

                event.stopPropagation();

            });
            angular.element(document.getElementsByClassName("calendar-nav")).on('click', function(event) {

                event.stopPropagation();
            } ); //keep datepicker open
        }
    };
})
    .directive('reoccuringDatepicker', function ($rootScope) {
    return {
        restrict: "E",
        templateUrl: "../../views/uiComponents/datepicker.html",
        controller: function($scope, calendarHelper,shareData) {
            $scope.repeatShadow=true;
            console.log("reoccuring");
            $scope.reoccuringStartClassDate=shareData.getStartClassDate();
            console.log($scope.reoccuringStartClassDate);
            $scope.showDatePicker = false;
            $scope.monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];



            $scope.calendar = {
                year: $scope.reoccuringStartClassDate.year,
                month: $scope.reoccuringStartClassDate.month-1,
                monthName: calendarHelper.getMonthName($scope.reoccuringStartClassDate.month-1),
                isPreviousYear:false,
                isPreviousMonth:false
            };
            console.log($scope.calendar);
            $scope.days = calendarHelper.getCalendarDays($scope.calendar.year, $scope.calendar.month);


            $scope.nextMonth = function () {

                calendarHelper.incrementCalendarMonth($scope.calendar);

                $scope.calendar.monthName = calendarHelper.getMonthName($scope.calendar.month);
                $scope.days = calendarHelper.getCalendarDays($scope.calendar.year, $scope.calendar.month);

                if((parseInt($scope.calendar.month)<parseInt(new Date().getMonth())) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){

                    $scope.calendar.isPreviousYear = false;
                }
                else if((parseInt($scope.calendar.month)>=parseInt(new Date().getMonth())) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){

                    $scope.calendar.isPreviousYear = true;
                }

            }

            $scope.previousMonth = function () {

                if(parseInt($scope.calendar.year)==parseInt(new Date().getFullYear())) {

                    if (parseInt($scope.calendar.month) == parseInt(new Date().getMonth()+1)) {
                        $scope.calendar.isPreviousYear = false;
                        $scope.calendar.isPreviousMonth = false;
                    }
                }
                if((parseInt($scope.calendar.month)<parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){

                    $scope.calendar.isPreviousYear = false;
                }
                else if((parseInt($scope.calendar.month)>=parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){
                    $scope.calendar.isPreviousYear = true;
                }
                calendarHelper.decrementCalendarMonth($scope.calendar);
                $scope.calendar.monthName = calendarHelper.getMonthName($scope.calendar.month);
                $scope.days = calendarHelper.getCalendarDays($scope.calendar.year, $scope.calendar.month);

            }

            $scope.nextYear = function () {
                $scope.calendar.isPreviousYear=true;
                $scope.calendar.isPreviousMonth=true;
                $scope.calendar.year++;
                $scope.days = calendarHelper.getCalendarDays($scope.calendar.year, $scope.calendar.month);
            }

            $scope.previousYear = function () {

                $scope.calendar.year--;
                $scope.days = calendarHelper.getCalendarDays($scope.calendar.year, $scope.calendar.month);
                if(parseInt($scope.calendar.year)==parseInt(new Date().getFullYear())){
                    $scope.calendar.isPreviousYear=false;

                    if(parseInt($scope.calendar.month+1)<=parseInt(new Date().getMonth()+1)){
                        $scope.calendar.isPreviousMonth=false;}
                }
                if((parseInt($scope.calendar.month)<parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){

                    $scope.calendar.isPreviousYear = false;
                }
                else if((parseInt($scope.calendar.month)>=parseInt(new Date().getMonth()+1)) && parseInt($scope.calendar.year)== parseInt(new Date().getFullYear()+1)){
                    $scope.calendar.isPreviousYear = true;
                }
            }

            $scope.selectDate = function (day) {

                $scope.element.val(day + "/" + ($scope.calendar.month + 1) + "/" + + $scope.calendar.year);
                $scope.fromDate=($scope.calendar.month + 1) + "/" + day + "/" + + $scope.calendar.year;
                $scope.untilDate="until "+$scope.monthNames[$scope.calendar.month]+" "+day+" ,"+$scope.calendar.year;
                $rootScope.showReoccuring=true;

                $scope.showDatePicker = false;
            }
            $scope.getPreviousDate=function(day) {
                    if(day=='')
                        return true;
                if(parseInt($scope.reoccuringStartClassDate.year)== parseInt($scope.calendar.year)){
                    if (parseInt($scope.reoccuringStartClassDate.month) == parseInt($scope.calendar.month + 1)) {
                        if (parseInt(day) > parseInt($scope.reoccuringStartClassDate.day)) {

                            return false;
                        }
                        else if (parseInt(day) <=parseInt($scope.reoccuringStartClassDate.day)) {

                            return true;
                        }
                    }
                }

            }


        },
        link: function(scope, element, attrs, controller) {

            var forElement = angular.element(document.getElementById(attrs.for));

            scope.element = forElement;
            forElement.on('focus', function() {

                scope.$apply(function() { scope.showDatePicker = true;});
            });

            angular.element(document.getElementsByTagName("body")[0]).on('click', function() {


                scope.$apply( function() { scope.showDatePicker = false; })
            });
            forElement.on('click', function(event) {

                event.stopPropagation();

            });
            angular.element(document.getElementsByClassName("calendar-nav")).on('click', function(event) {

                event.stopPropagation();
            } ); //keep datepicker open
        }
    };
});
