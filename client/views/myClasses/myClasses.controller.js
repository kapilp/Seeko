(function(){
    angular.module('seekoApp.myClasses', [])
        .controller('myClassesCtrl',['$scope','$rootScope','$http','shareData','$state','localStorageService','screenSize',function($scope,$rootScope,$http,shareData,$state,localStorageService,screenSize) {
           $scope.showspin=true;
            console.log("data from state is"+$state.current.data.hover);
            screenSize.rules = {
                retina: 'only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx)',
                superJumbo: '(max-width: 640px)',

            };
            $scope.currentUserDetail=shareData.getUserProfile();

            $scope.showClasses = true;
            if(screenSize.is('superJumbo'))
            {
                var startTeacherIndex=0;
                var endTeacherIndex=3;
                var startClassIndex=0;
                var endClassIndex=3;
                var joinedStartIndex = 0;
                var joinedEndIndex = 3;
            }
            else {
                var startTeacherIndex = 0;
                var endTeacherIndex = 6;
                var startClassIndex = 0;
                var endClassIndex = 6;
                var joinedStartIndex = 0;
                var joinedEndIndex = 6;
            }
            $scope.showTeacherLiveSection=false;
            $scope.cancelText='Cancel';
            $scope.accountStatus=shareData.getProfile().userType;
            $http.post('/api/myClasses', {id: shareData.getId()}).success(function (data) {
                // console.log(JSON.stringify(data));
                // console.log(JSON.stringify(data.data.myTeachingClassesArray));
                if(data.success==true) {
                         for(var i=0;i<data.data.myJoinClasses.length;i++){
                        var strNum=""+data.data.myJoinClasses[i].classData.avgStarCount;
                         if(strNum!='0'){
                        var index=strNum.indexOf('.');
                        if(index<0){
                            strNum=strNum+'.0';
                            data.data.myJoinClasses[i].classData.avgStarCount=strNum;
                        }
                      }
                    }
                    $scope.classFavouritesData = data.data.myJoinClasses.reverse();
                    $scope.myClasses = $scope.classFavouritesData.slice(startClassIndex, endClassIndex);
                    // console.log(shareData.getProfile().userType);
                    if (shareData.getProfile().userType == true) {
                        // console.log(shareData.getProfile());
                        $scope.myName = shareData.getProfile().firstName;
                        $scope.showTeacherLiveSection = true;

                         for(var i=0;i<data.data.myTeachingClassesArray.length;i++){
                        var strNum=""+data.data.myTeachingClassesArray[i].classData.avgStarCount;
                         if(strNum!='0'){
                        var index=strNum.indexOf('.');
                        if(index<0){
                            strNum=strNum+'.0';
                            data.data.myTeachingClassesArray[i].classData.avgStarCount=strNum;
                        }
                      }
                    }

                        $scope.teacherFavouritesData = data.data.myTeachingClassesArray.reverse();
                        $scope.teacherClasses = $scope.teacherFavouritesData.slice(startTeacherIndex, endTeacherIndex);
                    }


                }
                else
                    $scope.showMsg = true;
                $scope.showspin=false;
            });

            $scope.getNextClasses=function(type)
          {
                if(type== 'teacher') {
                    if( (($scope.teacherFavouritesData.length>endTeacherIndex) && ($scope.teacherFavouritesData.length>5)) || (($scope.teacherFavouritesData.length>endTeacherIndex) && ($scope.teacherFavouritesData.length>2)) ) {
                        if(screenSize.is('superJumbo')){
                            startTeacherIndex += 3;
                            endTeacherIndex += 3;
                        }
                        else {
                            startTeacherIndex += 6;
                            endTeacherIndex += 6;
                        }

                        $scope.teacherClasses = $scope.teacherFavouritesData.slice(startTeacherIndex, endTeacherIndex);
                    }
                }
                else if(type== 'attendedClass') {
                    if( (($scope.classFavouritesData.length>endClassIndex)&& ($scope.classFavouritesData.length>5)) || (($scope.classFavouritesData.length>endClassIndex)&& ($scope.classFavouritesData.length>2))) {
                        if(screenSize.is('superJumbo')){
                            startClassIndex += 3;
                            endClassIndex += 3;
                        }
                        else
                        {
                            startClassIndex += 6;
                            endClassIndex += 6;
                        }

                        $scope.myClasses = $scope.classFavouritesData.slice(startClassIndex, endClassIndex);
                    }
                }
                else {
                    if( (($scope.classFavouritesData.length>joinedEndIndex) && ($scope.classFavouritesData.length>5)) || (($scope.classFavouritesData.length>joinedEndIndex) && ($scope.classFavouritesData.length>2))) {
                        if(screenSize.is('superJumbo')){
                            joinedStartIndex += 3;
                            joinedEndIndex += 3;
                        }
                        else{
                            joinedStartIndex += 6;
                            joinedEndIndex += 6;
                        }

                        $scope.myClasses = $scope.classFavouritesData.slice(joinedStartIndex, joinedEndIndex);
                    }
                }




          }

          $scope.getPrevClasses=function(type) {
              if (type == 'teacher') {
                  if (startTeacherIndex > 0) {
                      if(screenSize.is('superJumbo')){
                          startTeacherIndex -= 3;
                          endTeacherIndex -= 3;
                      }
                      else
                      {
                          startTeacherIndex -= 6;
                          endTeacherIndex -= 6;
                      }

                      $scope.teacherClasses = $scope.teacherFavouritesData.slice(startTeacherIndex, endTeacherIndex);
                  }

              }
              else if (type == 'attendedClass') {
                  if (startClassIndex > 0) {
                      if(screenSize.is('superJumbo')){
                          startClassIndex -= 3;
                          endClassIndex -= 3;
                      }
                      else{
                          startClassIndex -= 6;
                          endClassIndex -= 6;
                      }

                      $scope.myClasses = $scope.classFavouritesData.slice(startClassIndex, endClassIndex);
                  }

              }
              else {
                  if (joinedStartIndex > 0) {
                      if(screenSize.is('superJumbo')){
                          joinedStartIndex -= 3;
                          joinedEndIndex -= 3;
                      }
                      else{
                          joinedStartIndex -= 6;
                          joinedEndIndex -= 6;
                      }

                      $scope.myClasses = $scope.classFavouritesData.slice(joinedStartIndex, joinedEndIndex);

                  }
              }
          }

          $scope.cancelClass=function(cancelClass){
              console.log('id'+cancelClass._id);
              $http.post('/api/cancelClassNew',{classId:cancelClass._id,id:shareData.getId()}).success(function(data){
                  // console.log(data);
                  if(data.success==true)
                  cancelClass.classData.cancelClass=true;
              })
          }

            $scope.viewAllClasses=function () {
                localStorageService.set('stateReview', $scope.teacherFavouritesData);
                $state.go('viewall');
            }

            $scope.changeCssOnHover=function () {
                $scope.textClr= {color:'#479fcd', 'text-decoration': 'underline'};
                $scope.underline={};
            }
            $scope.changeCssOnLeave=function () {
                $scope.textClr={color:' #808080'};
            }
            $scope.viewAllClasses1=function () {
                localStorageService.set('stateReview', $scope.classFavouritesData);
                $state.go('viewall');
            }
            $scope.changeCssOnHover1=function () {
                $scope.textClr1= {color:'#479fcd', 'text-decoration': 'underline'};
                $scope.underline={};
            }
            $scope.changeCssOnLeave1=function () {
                $scope.textClr1={color:' #808080'};
            }
            $scope.viewAllClasses2=function () {
                localStorageService.set('stateReview', $scope.classFavouritesData);
                $state.go('viewall');
            }
            $scope.changeCssOnHover2=function () {
                $scope.textClr2= {color:'#479fcd', 'text-decoration': 'underline'};
                $scope.underline={};
            }
            $scope.changeCssOnLeave2=function () {
                $scope.textClr2={color:' #808080'};
            }

            $scope.createClass = function () {
                localStorageService.set('edit',false);
                $state.go('teacherDashboard.addClass');

            }
            $scope.joinClass = function () {
                localStorageService.set('stateReview', '');
                $state.go('viewall');

            }
            $scope.attendClass = function () {
                localStorageService.set('stateReview', '');
                $state.go('viewall');
            }
            $scope.getClassDetail=function(value){
              var request={"label":value.classData.className,"value":value._id,"teacherId":value.classData.teacherId};

              localStorageService.set('searchString',request);
              localStorageService.set('searchFrom', 'searchBox');
                  // $rootScope.searchString=result.name;
              // if(shareData.getProfile().userType==false){
              //     $state.go('dashboard.classDetails',{},{reload:"dashboard.classDetails"});}
              //     else{
                  $state.go('classDetails',{id:value._id});
                  // }

            }
            $scope.editClassDetail=function(value){
                var request={"name":value.classData.className,"index":value._id,"teacherId":value.classData.teacherId};
                localStorageService.set('edit',request);
                $state.go('teacherDashboard.addClass',{},{reload:"teacherDashboard.addClass"});

            }

        }]);
      }());
