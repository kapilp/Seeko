'use strict';
angular.module('spinnerLoaderDirective', [])
    .directive('spinnerLoader', function ($timeout) {
    return {
        restrict: "E",
        templateUrl: "../../views/uiComponents/cubeSpinner.html",
        scope: {showSpinner: '=ngShow'},
        link: function (scope, elm, attrs, ctrl) {
            console.log(scope);

            scope.$watch('showSpinner', function (newValue, oldValue) {

                if (newValue == "false") {
                    elm.addClass('ng-hide');
                } else {
                    elm.addClass('ng-show');
                }

            });
        }
    };
});