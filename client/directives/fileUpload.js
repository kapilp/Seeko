angular.module('fileUpload',[]).
directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            console.log("inside change directive");
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function(){
                console.log("inside change function");
                scope.$apply(function(){
                    console.log("inside change apply function");
                    modelSetter(scope, element[0].files[0]);
                });
                scope.setimage2();
            });

            scope.setimage2 = function() {
                console.log("inside change image function");
                var file = scope.user.image;
                console.log(file);
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function(e) {
                    console.log("inside change onload function");
                    scope.$apply(function(){
                        scope.fileName=file.name;
                        console.log(scope.fileName);
                        scope.ImageSrc = e.target.result;
                        var img = document.createElement('IMG');
                        img.setAttribute("src", scope.ImageSrc );
                        img.setAttribute("style","height:143px;width:189px;padding-top:3px;padding-left:3px;");
                        console.log(document.getElementById("preview1").childNodes[0]);
                        if(document.getElementById("preview1").childNodes[0]){
                            document.getElementById("preview1").replaceChild(img,document.getElementById("preview1").childNodes[0]);
                        }
                        else
                            document.getElementById("preview1").appendChild(img);

                    });  }
            }
        }
    };
}])
.directive('fileDrag', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            console.log("inside drag directive");
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('dragover', function(event){
                event.stopPropagation();
                event.preventDefault();
            });
            element.bind('drop', function(event){
                event.stopPropagation();
                event.preventDefault();
                var file;
                if(event.dataTransfer.files[0] && event.dataTransfer.files[0].type.match('image.*')) {
                    scope.$apply(function () {
                        console.log("inside drop apply function");
                        // modelSetter(scope, event.dataTransfer.files[0]);

                        scope.user.image = event.dataTransfer.files[0];
                        console.log(scope.user.image);
                        scope.setimage(scope.user.image);
                    });
                }

            });
            scope.setimage = function(file) {
                //var file = scope.myFile;
                console.log("inside drop image function");
                console.log(file);
                var reader = new FileReader();
                // if(file && file.type.match('image.*'))
                    reader.readAsDataURL(file);
                reader.onload = function(e) {
                    scope.$apply(function(){
                        scope.fileName=file.name;
                        console.log(scope.fileName);
                        scope.ImageSrc = e.target.result;
                        var img = document.createElement('IMG');
                        img.setAttribute("src", scope.ImageSrc );
                        img.setAttribute("style","height:143px;width:189px;padding-top:3px;padding-left:3px;");

                        if(document.getElementById("preview1").childNodes[0]){
                            document.getElementById("preview1").replaceChild(img,document.getElementById("preview1").childNodes[0]);
                        }
                        else
                            document.getElementById("preview1").appendChild(img);

                    });  }
            }
        }
    };
}])
    .directive('fileDocument',['$parse', function ($parse) {
        console.log("inisde file document");
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                console.log("inside change directive");
                var model = $parse(attrs.fileDocument);
                var modelSetter = model.assign;
                element.bind('change', function(){
                    console.log("inside change function");
                    scope.$apply(function(){
                        console.log("inside change apply function");
                        modelSetter(scope, element[0].files[0]);
                    });
                    scope.setName();
                });

                scope.setName = function() {
                    console.log("inside document function");
                    var file = scope.user.doc;
                    console.log(file);
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function(e) {
                        console.log("inside change onload function");
                        scope.$apply(function(){
                            scope.user.DocName=file.name;
                            console.log(scope.user.DocName);


                        });  }
                }
            }
        };
    }])
    .directive('imageDocument', ['$parse', function ($parse) {
        console.log("inside image document");
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                scope.fileError=false;
                console.log("inside change directive");
                var model = $parse(attrs.imageDocument);
                var modelSetter = model.assign;
                element.bind('change', function(){
                    console.log("inside change function");
                    scope.$apply(function(){
                        console.log("inside change apply function");
                        modelSetter(scope, element[0].files[0]);
                    });
                    scope.setImageName();
                });

                scope.setImageName = function() {
                    console.log("inside image function");
                    var file = scope.user.image;
                    console.log(file);
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    if(file.size< 5242881)
                    {
                        scope.fileError=false;
                        reader.onload = function (e) {
                            console.log("inside change onload function");
                            scope.$apply(function () {
                                scope.user.DocumentName = file.name;
                                console.log(scope.user.DocName);


                            });
                        }
                    }
                    else
                        scope.fileError=true;
                }
            }
        };
    }]);
