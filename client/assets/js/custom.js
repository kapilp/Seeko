$(document).ready(function() {
		
		if($('#calendar').length){
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '2016-03-12',
			firstDay: 0,
			titleFormat:'MMMM',
			selectable: true,
			selectHelper: true,
			dayNamesShort:['Monday','Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday'],
			select: function(start, end) {
				var title = prompt('Event Title:');
				var eventData;
				if (title) {
					eventData = {
						title: title,
						start: start,
						end: end
					};
					$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
				}
				$('#calendar').fullCalendar('unselect');
			},
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				{
					title: '3',
					start: '2016-03-08',
					 backgroundColor: '#669934',
				},
				{
					title: '1',
					start: '2016-03-11',
					backgroundColor: '#00b4cb'
				},
				{
					title: '2',
					start: '2016-03-24',
					backgroundColor: '#7e0063'
				},
				
			]
		});
			setTimeout(function(){
			$('.fc-title').append('<span>Classes</span>');
		},100)
	}
		
	// POP-UP	 $(".login-btn").click(function(){
    	$(".login-overlay").fadeIn(700);
		}); 
	
	  $(".close").click(function(){
    	$(".login-overlay").fadeOut(700);
	});	
		
	// switcher
	
	$('.switch').click(function() {
			$(this).toggleClass('on').toggleClass('off');
		});	
		
		
	//Select Box

	if ($(".select").length) {
		$(".select").selectbox();
	}
		
	

	});		
	
	// Custon ScrollBar
	
	if ($('.pac-content').length) {
		$(".pac-content").mCustomScrollbar({
			axis : "x",
			 advanced:{autoExpandHorizontalScroll:true}
			
		});
	}

	
	// Menu--Slide 
	if($(window).width() < 767){
	$(".menu-link" ).click(function() {
  		$( ".mega-menu-wrap" ).slideToggle( "slow" );
	});
	
	$(".mega-menu-wrap li" ).click(function() {
  		$(this).find(".sub-menu-block").slideToggle( "slow" );
	});
	}
	
	//Click event to scroll to top
	$(window).scroll(function(){
	if ($(this).scrollTop() > 100) {
	   $('.scroll-top').fadeIn();
	  } else {
	   $('.scroll-top').fadeOut();
	  }
 	 });
	 $('.scroll-top').click(function(){
	  $('html, body').animate({scrollTop : 0},800);
	  return false;
	 });
	
	//
	
	//File Upload
	
	
	
(function($) {

		  // Browser supports HTML5 multiple file?
		  var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
		      isIE = /msie/i.test( navigator.userAgent );

		  $.fn.customFile = function() {

		    return this.each(function() {

		      var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
		          $wrap = $('<div class="file-upload-wrapper">'),
		          $input = $('<input type="text" class="file-upload-input" />'),
		          // Button that will be used in non-IE browsers
		          $button = $('<button type="button" class="file-upload-button">Choose File</button>'),
		          // Hack for IE
		          $label = $('<label class="file-upload-button" for="'+ $file[0].id +'">Select a File</label>');

		      // Hide by shifting to the left so we
		      // can still trigger events
		      $file.css({
		        position: 'absolute',
		        left: '-9999px'
		      });

		      $wrap.insertAfter( $file )
		        .append( $file, $input, ( isIE ? $label : $button ) );

		      // Prevent focus
		      $file.attr('tabIndex', -1);
		      $button.attr('tabIndex', -1);

		      $button.click(function () {
		        $file.focus().click(); // Open dialog
		      });

		      $file.change(function() {

		        var files = [], fileArr, filename;

		        // If multiple is supported then extract
		        // all filenames from the file array
		        if ( multipleSupport ) {
		          fileArr = $file[0].files;
		          for ( var i = 0, len = fileArr.length; i < len; i++ ) {
		            files.push( fileArr[i].name );
		          }
		          filename = files.join(', ');

		        // If not supported then just take the value
		        // and remove the path to just show the filename
		        } else {
		          filename = $file.val().split('\\').pop();
		        }

		        $input.val( filename ) // Set the value
		          .attr('title', filename) // Show filename in title tootlip
		          .focus(); // Regain focus

		      });

		      $input.on({
		        blur: function() { $file.trigger('blur'); },
		        keydown: function( e ) {
		          if ( e.which === 13 ) { // Enter
		            if ( !isIE ) { $file.trigger('click'); }
		          } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
		            // On some browsers the value is read-only
		            // with this trick we remove the old input and add
		            // a clean clone with all the original events attached
		            $file.replaceWith( $file = $file.clone( true ) );
		            $file.trigger('change');
		            $input.val('');
		          } else if ( e.which === 9 ){ // TAB
		            return;
		          } else { // All other keys
		            return false;
		          }
		        }
		      });

		    });

		  };

		  // Old browser fallback
		  if ( !multipleSupport ) {
		    $( document ).on('change', 'input.customfile', function() {

		      var $this = $(this),
		          // Create a unique ID so we
		          // can attach the label to the input
		          uniqId = 'customfile_'+ (new Date()).getTime(),
		          $wrap = $this.parent(),

		          // Filter empty input
		          $inputs = $wrap.siblings().find('.file-upload-input')
		            .filter(function(){ return !this.value }),

		          $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

		      // 1ms timeout so it runs after all other events
		      // that modify the value have triggered
		      setTimeout(function() {
		        // Add a new input
		        if ( $this.val() ) {
		          // Check for empty fields to prevent
		          // creating new inputs when changing files
		          if ( !$inputs.length ) {
		            $wrap.after( $file );
		            $file.customFile();
		          }
		        // Remove and reorganize inputs
		        } else {
		          $inputs.parent().remove();
		          // Move the input so it's always last on the list
		          $wrap.appendTo( $wrap.parent() );
		          $wrap.find('input').focus();
		        }
		      }, 1);

		    });
		  }
		  
		  
		  
		  
		  

}(jQuery));

$('input[type=file]').customFile();

// Datepicker

  $( function() {
    $("#datepicker").datepicker({
    	showOtherMonths:true
    });
    
  } );
  


$( function(){

 $("#slider-range").slider({
   				range : true,
   				min : 00,
   				max : 1000,
   				values : [0, 1000],
   				slide : function(event, ui) {
    				$("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
    				$('.left--value ').text("$" + ui.values[0] );
    				$('.right--value ').text("$" + ui.values[1]);
   				}
  			});
  			$("#slider-range").eq(0).append('<span class="range--value left--value "> </span>');
  			$("#slider-range").eq(0).append('<span class="range--value right--value"> </span>');
  			$('.left--value ').text("$" + 90 );
    		$('.right--value ').text("$" + 100);
  			//=====
  			
  			$( "#tabs" ).tabs();
  			
  			if ($('.tab-right-content').length) {
  				$(".tab-right-content").mCustomScrollbar({
					setHeight : 336,
				});
  			}
  			
  			$( "#accordion" ).accordion();

});

// $(function() {
    // $('#preview1').imagepreview({
        // input: '[name="testimage1"]',
        // reset: '#reset1',
        // preview: '#preview1'
    // });
//     
 // });
 
 $(function() {
 $('.demo').percentcircle({
	animate : true,
	diameter : 50,
	guage: 2,
	coverBg: '#fff',
	bgColor: '#efefef',
	fillColor: '#5c93c8',
	percentSize: '15px',
	percentWeight: 'normal'
	});

 });






