(function () {
    angular.module('seekoApp.content', []).controller('contentCtrl', ['$scope', '$uibModal','$state','$http','$rootScope','localStorageService','shareData', function ($scope, $uibModal,$state,$http, $rootScope,localStorageService,shareData) {
        $http.get('/api/fullClasses').success(function(data){
            $scope.classes=data.classData;
        });

        $http.get('/api/getCount').success(function (data) {
            $scope.count = data;
        })
       localStorageService.set('loginFromSeeko','');
        $scope.addSignUp = function () {
            var modalInstance = $uibModal.open({
                templateUrl: '../../views/signup/signup.html',
                controller: 'signupCtrl as signup',
                animation:true,
                windowClass: 'modal modal-slide-in-right'
            });
        };
        $scope.getClassDetail=function(classValue){
            var request={value: classValue._id, label: classValue.classData.className, teacherId: classValue.classData.teacherId, type: "className"}

            $rootScope.searchString=request.label;
            shareData.setSearchData(request);
            localStorageService.set('searchString',request);
                localStorageService.set('searchFrom', 'searchBox');
                $state.go('classDetails',{id:classValue._id});


        };
        $scope.demoVideo = function() {
            var modalInstance = $uibModal.open({
                templateUrl: '../../views/notification/youtubeDemo.html',
                controller:'youtubedemoCtrl',


            });
        };
        $scope.removeClass=function() {
            $state.go($state.current);
            $uibModalInstance.close('cancel');
        }

    }]);
}());


