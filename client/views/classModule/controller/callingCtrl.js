/**
 * Created by Arpit on 13/04/17.
 */
angular.module("app.callingCtrl", []).controller(
    "callingCtrl", ["$scope", "$parse","$timeout", "$rootScope","$location","$anchorScroll", "shareData", "localStorageService",
    function($scope, $parse,$timeout, $rootScope, $location, $anchorScroll,shareData, localStorageService) {

      console.log("inisde main ggg");
        var webRtcPeer;

        var ws = new WebSocket('wss://' + location.host + '/one2many');
        window.onbeforeunload = function (event) {
            ws.close();
        };

        $scope.isActived = function(selectedIndex) {
            $scope.isActive = selectedIndex;

        }

        $scope.$on('stopCallingController',function (event,arg) {


            console.log("stop calling controller");

            stop();

        })
        $scope.$on('sendClassId',function (data) {

            $scope.teacherClassId=data;
        })
        $scope.barShown=true;
        $scope.$on('showGroupChat',function(event,args){
            if(args.showGroup)
            $scope.showGroupChatEnabled=true;
            else
                $scope.showGroupChatEnabled=false;
        });

            $scope.$on('showParticipant',function(event,args){
                if(args.showParticipant)
                $scope.showParticipantEnabled=true;
                else
                    $scope.showParticipantEnabled=false;
            });
            // $scope.toggleParticipant=function(){
            //     if($scope.showParticipantEnabled){
            //         $scope.showParticipantEnabled=false;
            //         $scope.$emit('ParticipantToggle',{'showParticipant':false});
            //     }
            //     else{
            //         $scope.showParticipantEnabled=true;
            //         $scope.$emit('ParticipantToggle',{'showParticipant':true});
            //     }
            // }

        // $scope.toggleGroupChat=function (value) {
        //         console.log("this is working");
        //     if($scope.showGroupChatEnabled){
        //         console.log($scope.showGroupChatEnabled);
        //         $scope.showGroupChatEnabled=false;
        //         $scope.$emit('GroupChatToggle',{'showGroupChat':false});
        //     }
        //     else{
        //         $scope.showGroupChatEnabled=true;
        //         console.log("else"+$scope.showGroupChatEnabled);
        //         $scope.$emit('GroupChatToggle',{'showGroupChat':true});
        //     }
        //
        // }
        // $scope.presenter();




//screen and video share

        function showSpinner() {
            // for (var i = 0; i < arguments.length; i++) {
            // 	arguments[i].poster = './img/transparent-1px.png';
            // 	arguments[i].style.background = 'center transparent url("./img/spinner.gif") no-repeat';
            // }

        }
        ws.onmessage = function(message) {
            var parsedMessage = JSON.parse(message.data);
            console.info('Received message: ' + message.data);

            switch (parsedMessage.id) {
                case 'presenterResponse':
                    presenterResponse(parsedMessage);
                    break;
                case 'screenPresenterResponse':
                    screenPresenterResponse(parsedMessage);
                    break;
                case 'viewerResponse':
                    viewerResponse(parsedMessage);
                    break;
                case 'screenShare':
                    webRtcPeer = undefined;
                    $scope.viewer();

                    break;
                case 'switchToVideo':
                    webRtcPeer = undefined;
                    $scope.viewer();

                    break;
                case 'stopCommunication':
                    dispose();
                    break;
                case 'iceCandidate':
                    webRtcPeer.addIceCandidate(parsedMessage.candidate)
                    break;
                default:
                    console.error('Unrecognized message', parsedMessage);
            }
        }

        function presenterResponse(message) {
            if (message.response != 'accepted') {
                var errorMsg = message.message ? message.message : 'Unknow error';
                console.warn('Call not accepted for the following reason: ' + errorMsg);
                dispose();
            } else {
                webRtcPeer.processAnswer(message.sdpAnswer);
            }
        }

        function screenPresenterResponse(message) {
            if (message.response != 'accepted') {
                var errorMsg = message.message ? message.message : 'Unknow error';
                console.warn('Call not accepted for the following reason: ' + errorMsg);
                dispose();
            } else {
                webRtcPeer.processAnswer(message.sdpAnswer);
            }
        }
        function viewerResponse(message) {
            if (message.response != 'accepted') {
                var errorMsg = message.message ? message.message : 'Unknow error';
                console.warn('Call not accepted for the following reason: ' + errorMsg);
                dispose();
            } else {
                webRtcPeer.processAnswer(message.sdpAnswer);
            }
        }

       $scope.muteVideo=function(){

           video.muted=!video.muted;
       }
       $scope.raiseHand=function(){
           $scope.$emit('handRaised',{studentId:shareData.getId(),studentMail:shareData.getUserProfile().email,studentName:shareData.getProfile().firstName});
       }

       $scope.switchToVideo = function ( ) {

            console.log("switching to video");
           if (webRtcPeer) {
               showSpinner(video);

               var constraints = {
                   audio: true,
                   video: true
               }

               var options = {
                   localVideo : video,
                   onicecandidate : onIceCandidate,
                   mediaConstraints: constraints
                   // sendSource:"screen"
               }


               webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
                   if(error) return onError(error);

                   this.generateOffer(onOfferScreenToVideo);
               });
           }
       }

        $scope.screenShare=function ()

        {
            // stop();
            //  webRtcPeer =undefined;
            if (webRtcPeer) {
                showSpinner(video);


                var options = {
                    localVideo : video,
                    onicecandidate : onIceCandidate,
                    sendSource:'screen'
                }


                webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
                    if(error) return onError(error);

                    this.generateOffer(onOfferScreenPresenter);
                });
            }
        }


        function onOfferScreenToVideo(error, offerSdp) {
            if (error) return onError(error);

            var message = {
                id : 'switchScreenToVideo',
                sdpOffer : offerSdp,
                classId : $scope.teacherClassId
            };
            sendMessage(message);
        }

        $scope.presenter=function ()

        {
            if (!webRtcPeer) {
                showSpinner(video);

                var constraints = {
                    audio: true,
                    video: true
                }

                var options = {
                    localVideo : video,
                    onicecandidate : onIceCandidate,
                    mediaConstraints: constraints
                    // sendSource:"screen"
                }


                webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
                    if(error) return onError(error);

                    this.generateOffer(onOfferPresenter);
                });
            }
        }



        function onOfferPresenter(error, offerSdp) {
            if (error) return onError(error);

            var message = {
                id : 'presenter',
                sdpOffer : offerSdp,
                classId : $scope.teacherClassId
            };
            sendMessage(message);
        }
        function onOfferScreenPresenter(error, offerSdp) {
            if (error) return onError(error);

            var message = {
                id : 'screenPresenter',
                sdpOffer : offerSdp,
                classId : $scope.teacherClassId
            };
            sendMessage(message);
        }

        function onError(error) {
            console.error(error);
        }

        $scope.viewer = function (){

            console.log("calling viewers");
            console.log(webRtcPeer);
            if (!webRtcPeer) {
                showSpinner(video);

                var options = {
                    remoteVideo: video,
                    onicecandidate : onIceCandidate
                }

                webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
                    if(error) return onError(error);

                    this.generateOffer(onOfferViewer);
                });
            }
        }

        function onOfferViewer(error, offerSdp) {
            if (error) return onError(error)


            console.log("viewer calling ");
            var message = {
                id : 'viewer',
                sdpOffer : offerSdp,
                classId : $scope.teacherClassId
            }
            sendMessage(message);
        }

        function onIceCandidate(candidate) {
            console.log('Local candidate' + JSON.stringify(candidate));

            var message = {
                id : 'onIceCandidate',
                candidate : candidate,
                classId : $scope.teacherClassId
            }
            sendMessage(message);
        }

        function stop() {
            if (webRtcPeer) {
                var message = {
                    id : 'stop'
                }
                sendMessage(message);
                dispose();
            }
        }

        function dispose() {
            if (webRtcPeer) {
                webRtcPeer.dispose();
                webRtcPeer = null;
            }
            hideSpinner(video);
        }

        function sendMessage(message) {
            var jsonMessage = JSON.stringify(message);
            console.log('Senging message: ' + jsonMessage);
            ws.send(jsonMessage);
        }

        function showSpinner() {
            // for (var i = 0; i < arguments.length; i++) {
            // 	arguments[i].poster = './img/transparent-1px.png';
            // 	arguments[i].style.background = 'center transparent url("./img/spinner.gif") no-repeat';
            // }
        }

        function hideSpinner() {
            // for (var i = 0; i < arguments.length; i++) {
            // 	arguments[i].src = '';
            // 	arguments[i].poster = './img/webrtc.png';
            // 	arguments[i].style.background = '';
            // }
        }


        //condition to check whether its user or presenter
        $scope.$on('teacherIdSet',function(event,args){
            $scope.teacherClassId = args.classId;
            console.log(localStorageService.get('teacherId').id);
            if(localStorageService.get('teacherId').id == shareData.getId()){
                $scope.presenter();

            }else{

                $scope.viewer();

            }
        })

    }]).directive("inline", function(){
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            console.log(elem);
            console.log(elem[0].id);
            console.log(attrs.id);
            // element.bind("mouseclick",function(){
            // 	scope.presenter();
            // })

        }
    }
});
