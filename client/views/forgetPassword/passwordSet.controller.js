(function() {
    angular.module('seekoApp.passwordSet',['ui.router'])
        .controller('passwordSetCtrl',['$scope','$http','shareData','Notification','$state',function ($scope,$http,shareData,Notification,$state) {
            $scope.changePassword=function(){
                console.log("inside change password"+shareData.getId());
                console.log("inside change password"+$scope.confirmPassword);
                $http.post('/api/getNewPassword',{id:shareData.getId(),password:$scope.confirmPassword})
                    .success(function (data) {
                        if(data.success==true){
                            if(data.userType == true)
                                $state.go('teacherDashboard');
                            else
                                $state.go('dashboard');
                        }
                    })
            }

        }]);
}());