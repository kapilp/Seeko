(function(){
angular.module('seekoApp.login', ['ui.router'])

    .controller('loginCtrl', ['$scope','$uibModal','$uibModalInstance','$state','$http','$window','$rootScope','$cookies','Alertify','base64','gmailService','$filter','Notification','localStorageService','$timeout','$document',function($scope,$uibModal,$uibModalInstance,$state,$http,$window,$rootScope,$cookies,Alertify,base64,gmailService,$filter,Notification,localStorageService,$timeout,$document) {
        // var body = angular.element(document).find('body');
        $scope.onChange=function () {
                $scope.errorMsg=false;
        }
        var currentType={
            type:'all'
        }
        localStorageService.set('trendingStatus',currentType);
        $scope.$on('event:google-plus-signin-success', function(event, authResult) {
            $scope.googleToken=authResult.access_token;
            gapi.client.load('plus', 'v1', apiClientLoaded)
        });

        function apiClientLoaded() {
            gapi.client.plus.people.get({userId: 'me'}).execute(handleResponse);
        }

        function handleResponse(resp) {
            console.log(resp);
            var imageUrl=resp.image.url.replace("?sz=50", "?sz=250");
            loginCredentials={
                'firstName':resp.name.givenName,
                'lastName':resp.name.familyName,
                'name':resp.displayName,
                'email':resp.emails[0].value,
                'id':resp.id,
                'picture':imageUrl,
                'token':$scope.googleToken,
                'userType':$rootScope.teacher
            };
            console.log(loginCredentials);

            $http.post('/api/googleLogin',loginCredentials)
                .success(function(data){
                    $uibModalInstance.close('ok');
                    localStorageService.set('visitType','login');
                     localStorageService.set('loginFromSeeko','');
                    if(data.userType==false)
                        $state.go('dashboard');
                    else
                        $state.go('teacherDashboard');
                });
        }
        $scope.removeClass=function() {
            $state.go($state.current);
            $uibModalInstance.close('cancel');
        };
        $scope.login=function () {
            $scope.buttonLoader=true;
            this.loginField.email=$filter('lowercase')(this.loginField.email);

            $http.post('/api/login',this.loginField)
                .success(function(data){
                    if(data.success != false){
                        localStorageService.set('visitType','login');
                        Notification.primary({message:"Welcome To Seeko",templateUrl: "../../views/messageNotifications/success.html"});
                        localStorageService.set('loginFromSeeko','loginFromSeeko');
                        if(data.userType==false){
                            $uibModalInstance.close('cancel');
                            $state.go('dashboard');
                        }
                        else{
                            $uibModalInstance.close('cancel');
                            $state.go('teacherDashboard');

                        }

                    }
                    else {
                        this.loginClass=false;
                        this.message=data.message;
                        $scope.errorMsg=true;
                        // $scope.buttonLoader=false;
                        $timeout(function () { $scope.buttonLoader=false },1000);
                        // $uibModalInstance.close('cancel');
                        // Notification.primary({message:this.message,templateUrl: "../../views/messageNotifications/error.html"});
                    }
                });





        };
        $scope.loginWithFacebook=function () {
            $scope.buttonLoaderTwo=true;
            var loginCredentials='';

            if (typeof(FB) == 'undefined'
                 ) {
                $scope.buttonLoaderTwo=false;
                $uibModalInstance.close('ok');
                Notification.primary({message:"Please try to connect internet...",templateUrl: "../../views/messageNotifications/error.html"});
                return ;
            } else {
                FB.getLoginStatus(function(Fbresponse) {
                    console.log(Fbresponse);
                    if(Fbresponse.status==='connected'){
                        console.log("inside if");
                        FB.api('/me','get', {fields: 'id,name,first_name,last_name,gender,email,picture.height(250).width(250)' } , function(response)
                        {
                            console.log("facebook respone is"+JSON.stringify(response));
                            this.loginCredentials={
                                'name':response.name,
                                'firstName':response.first_name,
                                'lastName':response.last_name,
                                'picture':response.picture,
                                'email':response.email,
                                'gender':response.gender,
                                'id':response.id,
                                'token':Fbresponse.authResponse.access_token,
                                'userType':$rootScope.teacher


                            };


                            $http.post('/api/fbLogin',this.loginCredentials)
                                .success(function (data) {
                                    localStorageService.set('visitType','login');
                                   localStorageService.set('loginFromSeeko','');
                                    if(data.userType==false){
                                        $scope.buttonLoaderTwo=false;
                                        $uibModalInstance.close('ok');
                                        $state.go('dashboard');

                                    }
                                    else{
                                        $scope.buttonLoaderTwo=false;
                                        $uibModalInstance.close('ok');
                                        $state.go('teacherDashboard');

                                    }


                                });

                        });
                    }
                    else{
                        console.log("inide else");
                        FB.login(function(response) {
                            if (response.authResponse)
                            {
                                var access_token =   FB.getAuthResponse()['accessToken'];

                                FB.api('/me','get', {fields: 'id,name,first_name,last_name,gender,email,picture.height(250).width(250)' } , function(response)
                                {
                                    console.log("facebook respone is"+JSON.stringify(response));
                                    this.loginCredentials={
                                        'name':response.name,
                                        'firstName':response.first_name,
                                        'lastName':response.last_name,
                                        'picture':response.picture,
                                        'email':response.email,
                                        'id':response.id,
                                        'token':access_token,
                                        'userType':$rootScope.teacher


                                    };


                                    $http.post('/api/fbLogin',this.loginCredentials)
                                        .success(function (data) {
                                            localStorageService.set('visitType','login');
                                           localStorageService.set('loginFromSeeko','');
                                            if(data.userType==false){
                                                $scope.buttonLoaderTwo=false;
                                                $uibModalInstance.close('ok');
                                                $state.go('dashboard');

                                            }
                                            else{
                                                $scope.buttonLoaderTwo=false;
                                                $uibModalInstance.close('ok');
                                                $state.go('teacherDashboard');

                                            }


                                        });

                                });



                            } else {
                                console.log('User cancelled login or did not fully authorize.');
                            }
                        },{scope: 'public_profile,email'});
                    }
                });

            }

        };
        $scope.createAccount=function(){
            $uibModalInstance.close('cancel');
            var modalInstance = $uibModal.open({
                templateUrl: '../../views/signup/signup.html',
                controller:'signupCtrl as signup'

            });
        };
        $scope.resetPassword=function() {
            console.log("inside  click");
             $uibModalInstance.dismiss('ok');
            var modalInstance = $uibModal.open({
                templateUrl: '../../views/login/passwordReset.html',
                controller:'loginCtrl'
            });
            $scope.modal=modalInstance;

        };
        $scope.closeResetPassword=function(){
            $uibModalInstance.dismiss('ok');
        }
        $scope.confirmPasswordReset=function(){
            console.log("resetPassword");
            $uibModalInstance.dismiss('ok');
            $http.post('/api/passwordResetMail',{emailId:$scope.resetPasswordMailId})

                .success(function (data) {
                    if(data.success==true){
                        Notification.primary({message:"Please Check Your Email Id",templateUrl: "../../views/messageNotifications/success.html"});
                        $uibModalInstance.dismiss('ok');
                    }
                    else {
                        Notification.primary({message:data.message,templateUrl: "../../views/messageNotifications/error.html"});
                        $uibModalInstance.dismiss('ok');
                    }
                })
        }
        // $document.on('click',function(){
        //     console.log("m challaa yaaar");
        //     body.css('overflow','auto');
        //
        // })

    }]);
}());


