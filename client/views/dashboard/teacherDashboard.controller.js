(function(){
    angular.module('seekoApp.teacherDashboard', [])
        .controller('myteacherDashboard',['$scope','$rootScope','$http','shareData','$stateParams','localStorageService','$state','screenSize','Socialshare',function($scope,$rootScope,$http,shareData,$stateParams,localStorageService,$state,screenSize,Socialshare) {
            $scope.showspin=true;
            $scope.inviteeEmail='';
            screenSize.rules = {
                superJumbo: '(max-width: 640px)'

            };
            $scope.currentUserDetail=shareData.getUserProfile();
            localStorageService.set('stateReview', '');
            // if(screenSize.is('superJumbo')){
            // }
            $scope.disableJoin=true;
            if(screenSize.is('superJumbo')){
                var startTeacherIndex=0;
                var endTeacherIndex=3;
                var startClassIndex=0;
                var endClassIndex=3;
                var startLiveNow=0;
                var endLiveNow=3;
            }
            else {
                var startTeacherIndex = 0;
                var endTeacherIndex = 6;
                var startClassIndex = 0;
                var endClassIndex = 6;
                var startLiveNow = 0;
                var endLiveNow = 6;
            }


            var firstCondition ;
            $http.get('https://ipinfo.io/json').
            success(function(data) {
          localStorageService.set('country',data);
               }).error(function(data){

              });

           $http.post('api/currentTrending',localStorageService.get('trendingStatus'))
                .success(function (data) {
                    for(var i=0;i<data.data.length;i++){
                        var strNum=""+data.data[i].classData.avgStarCount;
                         if(strNum!='0'){
                        var index=strNum.indexOf('.');
                        if(index<0){
                            strNum=strNum+'.0';
                            data.data[i].classData.avgStarCount=strNum;
                        }
                     }
                    }
                  $scope.trendingData=data.data;
                  $scope.dashboardClassesTrending=$scope.trendingData.slice(startClassIndex, endClassIndex);
                    $scope.showspin=false;
                })

           $http.post('api/currentLive',localStorageService.get('trendingStatus'))
                .success(function (data) {
                       for(var i=0;i<data.data.length;i++){
                        var strNum=""+data.data[i].classData.avgStarCount;
                         if(strNum!='0'){
                        var index=strNum.indexOf('.');
                        if(index<0){
                            strNum=strNum+'.0';
                            data.data[i].classData.avgStarCount=strNum;
                        }
                      }
                    }
                    $scope.liveData=data.data;
                    $scope.dashboardClassesLive=$scope.liveData.slice(startClassIndex, endClassIndex);
             })

            $http.post('api/topJedies',localStorageService.get('trendingStatus'))
                .success(function (data) {
                   $scope.jediesData=data.data;
                    function custom_sort(b, a) {
                        return a.userData.avgStarCount-b.userData.avgStarCount;
                    }
                    $scope.topJediBefore= $scope.jediesData.sort(custom_sort);
                      for(var i=0;i<$scope.topJediBefore.length;i++){
                        var strNum=""+$scope.topJediBefore[i].userData.avgStarCount;
                       if(strNum!='0'){
                        var index=strNum.indexOf('.');
                        if(index<0){
                            strNum=strNum+'.0';
                           $scope.topJediBefore[i].userData.avgStarCount=strNum;
                        }
                       }
                    }
                  $scope.topJedi=  $scope.topJediBefore;
                    $scope.dashboardClasses=$scope.topJedi.slice(startClassIndex, endClassIndex);
                })


        // if(localStorageService.get('visitType')=='login' || localStorageService.get('visitType')==undefined){
        //     $http.post('/api/dashboardClasses',{'index':shareData.getId()}).success(function(data){
        //         $scope.classData = data.data.reverse();
        //         console.log($scope.classData.length);
        //       //  $scope.dashboardClassesTrending=$scope.classData.slice(startClassIndex, endClassIndex);
        //       //   $scope.dashboardClassesLive=$scope.classData.slice(startClassIndex, endClassIndex);
        //        // $scope.dashboardClasses=$scope.classData.slice(startClassIndex, endClassIndex);
        //         $scope.showspin=false;
        //     });
        // }
        // else {
        //     $rootScope.searchString=localStorageService.get('searchString').name;
        //     var requestJson={"index":shareData.getId(),"name":localStorageService.get('searchString').label,"type":localStorageService.get('searchString').type};
        //     $http.post('/api/searchByClassIndex',requestJson).success(function(data){
        //         $scope.classData = data.data.reverse();
        //       //  $scope.dashboardClassesTrending=$scope.classData.slice(startClassIndex, endClassIndex);
        //       //   $scope.dashboardClassesLive=$scope.classData.slice(startClassIndex, endClassIndex);
        //       //  $scope.dashboardClasses=$scope.classData.slice(startClassIndex, endClassIndex);
        //        $scope.showspin=false;
        //
        //     });
        // }

            $scope.getTeacherProfile = function (profileDeatil) {
              // console.log(profileDeatil);
               $state.go('profile',{id:profileDeatil._id});

            }

            $scope.getNextClasses=function(type)
            {
                if(type== 'trending') {
                    console.log("isnide trending");
                    if((($scope.trendingData.length>endClassIndex) && ($scope.trendingData.length>5)) ||  (($scope.trendingData.length>endClassIndex) && ($scope.trendingData.length>2))) {
                        if(screenSize.is('superJumbo')){
                            startClassIndex += 3;
                            endClassIndex += 3;
                        }
                        else{
                            startClassIndex += 6;
                            endClassIndex += 6;
                        }

                        $scope.dashboardClassesTrending=$scope.trendingData.slice(startClassIndex, endClassIndex);
                    }
                }
                else if(type== 'liveNow') {
                    console.log("isnide liveNow");
                    if( (($scope.liveData.length>endLiveNow)&& ($scope.liveData.length>5))||(($scope.liveData.length>endLiveNow) && ($scope.liveData.length>2)) ) {
                        if(screenSize.is('superJumbo')){
                            startLiveNow += 3;
                            endLiveNow += 3;
                        }
                        else{
                            startLiveNow += 6;
                            endLiveNow += 6;
                        }

                        $scope.dashboardClassesLive = $scope.liveData.slice(startLiveNow, endLiveNow);
                    }
                }
                else {
                    console.log("isnide else if");
                    if( (($scope.topJedi.length>endTeacherIndex) && ($scope.topJedi.length>5))||(($scope.topJedi.length>endTeacherIndex) && ($scope.topJedi.length>2)) ) {
                        if(screenSize.is('superJumbo')){
                            startTeacherIndex += 3;
                            endTeacherIndex += 3;
                        }
                        else {
                            startTeacherIndex += 6;
                            endTeacherIndex += 6;
                        }

                        $scope.dashboardClasses = $scope.topJedi.slice(startTeacherIndex, endTeacherIndex);
                    }
                }




            }

            $scope.getPrevClasses=function(type) {
                if (type == 'trending') {
                    if (startClassIndex > 0) {
                        if(screenSize.is('superJumbo')){
                            startClassIndex -= 3;
                            endClassIndex -= 3;
                        }
                        else{
                            startClassIndex -= 6;
                            endClassIndex -= 6;
                        }

                        $scope.dashboardClassesTrending = $scope.trendingData.slice(startClassIndex, endClassIndex);
                    }

                }
                else if (type == 'liveNow') {
                    if (startLiveNow > 0) {
                        if(screenSize.is('superJumbo')){
                            startLiveNow -= 3;
                            endLiveNow -= 3;
                        }
                        else{
                            startLiveNow -= 6;
                            endLiveNow -= 6;
                        }

                        $scope.dashboardClassesLive = $scope.liveData.slice(startLiveNow, endLiveNow);
                    }

                }
                else {
                    if (startTeacherIndex > 0) {
                        if(screenSize.is('superJumbo')){
                            startTeacherIndex -= 3;
                            endTeacherIndex -= 3;
                        }
                        else
                        {
                            startTeacherIndex -= 6;
                            endTeacherIndex -= 6;
                        }

                        $scope.dashboardClasses = $scope.topJedi.slice(startTeacherIndex, endTeacherIndex);

                    }
                }
            }

            $scope.getClassDetail=function(value){
            console.log(value);
            var request={"label":value.classData.className,"value":value._id,"teacherId":value.classData.teacherId};
            localStorageService.set('searchString',request);
            localStorageService.set('searchFrom', 'searchBox');
            $state.go('classDetails', {id:value._id});

         }

             $scope.viewAllJedis=function () {
                console.log("hello");
                localStorageService.set('topJediStatus','fromJedi');
                localStorageService.set('stateReview',  $scope.topJedi);  
                $state.go('viewall'); 
             }

            $scope.viewAllTrending=function () {
                if($scope.trendingData.length>0) {
                    localStorageService.set('topJediStatus','');
                    localStorageService.set('stateReview', $scope.trendingData);
                        $state.go('viewall');
                }
            }

            $scope.viewAllLive=function () {
                if($scope.dashboardClassesLive.length>0) {
                    localStorageService.set('topJediStatus','');
                    localStorageService.set('stateReview', $scope.liveData);
                        $state.go('viewall');
                }
            }

         $scope.changeCssOnHover=function () {
             $scope.textClr= {color:'#479fcd', 'text-decoration': 'underline'};
             $scope.underline={};
           }
            $scope.changeCssOnLeave=function () {
                $scope.textClr={color:' #808080'};
            }

            $scope.changeCssOnHover1=function () {
                $scope.textClr1= {color:'#479fcd', 'text-decoration': 'underline'};
            }
            $scope.changeCssOnLeave1=function () {
                $scope.textClr1={color:' #808080'};
            }

            $scope.changeCssOnHover2=function () {
                $scope.textClr2= {color:'#479fcd', 'text-decoration': 'underline'};
            }
            $scope.changeCssOnLeave2=function () {
                $scope.textClr2={color:' #808080'};
            }

         $scope.joinClass=function(value){
              var data={teacherName:value.profile.userData.profile.firstName,classId:value.data._id,studentId:shareData.getId(),teacherId:value.profile._id,date:value.data.classData.fromDate,className:value.data.classData.className};
        $http.post('/api/joinClass',data).success(function(data){
           value.isJoined=true;

        });
         }

            $scope.getRating=function(rate){
                var starCount=
                {
                    "upperCount":Math.floor(rate),
                    "halfCount":rate-Math.floor(rate),
                    "lowerCount":Math.floor(5-rate)
                };
            }

        }]);
      }());
