/**
 * Created by Prabhakar on 20/03/17.
 */
var mongoose=require('../node_modules/mongoose');
var Schema=mongoose.Schema;

var contactModel = new Schema({

    name:String,
    email:String,
    subject:String,
    message:String

});

module.exports=mongoose.model('contactUsModel',contactModel);
