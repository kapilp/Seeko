(function () {
    //facebook credential
    console.log(window.location.hostname);
    if(window.location.hostname=='localhost')
        appId='276324179413046';
    else
        appId='387587551586187';

    window.fbAsyncInit = function () {
        FB.init({
            appId: appId,//for client
            //appId      : '387587551586187',//for server
            status: true,
            cookie: true,
            xfbml: true,
            version: 'v2.7'
        });
        // angular.bootstrap(document, ['seekoApp']);
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    angular.module('seekoApp', ['ui.router', 'ui.bootstrap','ngAnimate', 'widget.scrollbar',
        'angular-svg-round-progressbar', 'angular-accordion',
        'angularjs-datetime-picker', 'uiSlider', 'ngSanitize',
        'ngDropdowns', 'file-model', '500tech.simple-calendar',
        'duScroll', 'seekoApp.header', 'seekoApp.login',
        'seekoApp.signup', 'seekoApp.controller',
        'seekoApp.teach', 'seekoApp.learn', 'validation.match',
        'signUpService', 'ngCookies', 'Alertify', 'ngMessages',
        'base64Service', 'seekoApp.loginHeader', 'shareDataService',
        'ng.deviceDetector', 'seekoApp.recording', 'seekoApp.editAccount',
        'seekoApp.editStudent', 'seekoApp.editTeacher', 'ngMaterial', 'ngFileUpload',
        'fileUpload', 'seekoApp.teacherProfile', 'seekoApp.StudentProfile',
        'ui-notification', 'seekoApp.addClass', 'seekoApp.favourites', 'seekoApp.classDetails',
        'customDirectives', 'teachSrc', 'LocalStorageModule', 'customCalendar','seekoApp.notification'
        ,'seekoApp.myClasses','calendarHelperService','datePickerDirective',
        'seekoApp.applyToTeach','ui.router.title','seekoApp.teacherLoginHeader'
        ,'seekoApp.passwordSet','seekoApp.repeatClass','seekoApp.teacherDashboard',
        'seekoApp.calendar','seekoApp.footer','nsPopover','matchMedia','youtube-embed',
         'spinnerLoaderDirective','720kb.socialshare','seekoApp.content','seekoApp.contact',
        'ngMap','directive.g+signin','ngScrollbar','autocomplete','app.chatController',
        'app.callingCtrl','ngDragDrop','ngDesktopNotification','AxelSoft', 'seekoApp.viewAllClasses','seekoApp.youTubeDemo','angularTrix'])


        .config(function ($stateProvider, $urlRouterProvider,$locationProvider) {

            $urlRouterProvider.otherwise('/');
            $locationProvider.html5Mode(true);

            $stateProvider
                .state('check', {

                    url: '/',

                    views: {
                        'header@': {

                            controller:function ($state,shareData,$http) {
                                if(shareData.getCookies()) {
                                    console.log("inside check if");
                                    $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                        shareData.setUserProfile(data);
                                        if (shareData.getProfile().userType == false) {
                                            $state.go('dashboard',{type:'login'});
                                        }
                                        else {
                                            $state.go('teacherDashboard',{type:'login'});
                                        }
                                    });

                                }
                                else {
                                    $state.go('home');
                                    console.log("inside else check");
                                }
                            }

                        }
                    },
                    data:{
                        isLogin:
                        {
                            isLogin:false
                        }
                    }
                })



                .state('home', {

                    url: '/home',

                    views: {
                        'content@': {
                            templateUrl: 'views/content/content.html',
                            controller:'contentCtrl'
                        },
                        'header@': {
                            templateUrl: 'views/header/header.html',
                            controller: 'headerCtrl'

                        },
                        'footer@':{
                            templateUrl:'views/footer/footer.html',
                            controller:'footerCtrl'
                        }
                    },
                    data:{
                        isLogin:
                        {
                            isLogin:false
                        }
                    }
                })



                .state('home.learn', {
                    url: '/learn',
                    views: {
                        'content@': {
                            templateUrl: 'views/learn/learn.html',
                            controller: 'learnCtrl'
                        }
                    }

                }).
            state('home.teach', {
                url: '/teach',
                views: {
                    'content@': {
                        templateUrl: 'views/teach/teach.html',
                        controller: 'teachCtrl'
                    }
                }
            }).state('home.contact', {
                    url: '/contact',
                    views: {
                        'content@': {
                            templateUrl: 'views/contact/contact.html',
                            controller: 'contactCtrl'
                        }
                    }
                })
                .state('home.terms&condition', {
                    url: '/terms',
                    views: {
                        'content@': {
                            templateUrl: 'views/uiComponents/terms_and_conditions.html'

                        }
                    }
                })
                .state('home.dashboard', {
                url: '/dashboard',
                views: {
                    'content@': {
                        templateUrl: 'views/dashboard/teacherDashboard.html',
                        controller: 'myteacherDashboard'
                    }
                }
            })

                .state('viewall', {
                    url:'/viewall',
                    views: {
                        'header@':{
                            controller:function($state,shareData,$stateParams,$window){
                                if(shareData.getUserProfile().id)
                                {
                                    if(shareData.getUserProfile().userType){
                                        $state.go('viewall.teacher');
                                    }else{
                                        $state.go('viewall.student');
                                    }
                                   // $window.location.href='/';
                                }
                                else {
                                    $state.go('viewall.visitor');
                                }
                            }
                        },
                        'content@': {
                            templateUrl: 'views/viewClasses/viewClasses.html',
                            controller:'viewClassesDashboard'
                        }
                        ,
                        'footer@':{
                            templateUrl:'views/footer/footer.html',
                            controller:'footerCtrl'
                        }

                    }
                })


                .state('viewall.teacher', {
                    url: '',
                    views: {
                        'header@': {
                            templateUrl: 'views/header/teacherLoginHeader.html',
                            controller: 'teacherLoginHeaderCtrl'

                        }
                    }, data:{
                        hover:false,
                        dashboardHover:true,
                        calendarHover:false,
                        isLogin:{isLogin:true,userType:true}
                    }, resolve: {

                                authenticated: ['$q', 'shareData', function ($q, shareData) {
                                    var deferred = $q.defer();
                                    if (shareData.getCookies() == true) {

                                        deferred.resolve(shareData.getUserProfile());
                                    }
                                    else {
                                        console.log("else");

                                        deferred.reject('Not logged in');

                                    }

                                    return deferred.promise;
                                }],
                                profile: ['$q','shareData','$http',function($q,shareData,$http){
                                    var deferred=$q.defer();
                                    $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                        shareData.setUserProfile(data);
                                        deferred.resolve(data);
                                    }).error(function(data){
                                        deferred.reject(data);
                                    });
                                    return deferred.promise;
                                }]

                            }

                })

                .state('viewall.student', {
                    url: '',
                    views: {
                        'header@': {
                            templateUrl: 'views/header/loginHeader.html',
                            controller: 'loginHeaderCtrl'

                        }
                    }, data:{
                        hover:false,
                        dashboardHover:true,
                        calendarHover:false,
                        isLogin:{isLogin:true,userType:false}
                    } , resolve: {

                        authenticated: ['$q', 'shareData', function ($q, shareData) {
                            var deferred = $q.defer();
                            if (shareData.getCookies() == true) {

                                deferred.resolve(shareData.getUserProfile());
                            }
                            else {
                                console.log("else");

                                deferred.reject('Not logged in');

                            }

                            return deferred.promise;


                        }],
                        profile: ['$q','shareData','$http',function($q,shareData,$http){
                            var deferred=$q.defer();
                            $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                shareData.setUserProfile(data);
                                deferred.resolve(data);
                            }).error(function(data){
                                deferred.reject(data);
                            });
                            return deferred.promise;
                        }]

                    }


                })

                .state('viewall.visitor', {
                    url: '',
                    views: {
                        'header@': {
                            templateUrl: 'views/header/header.html',
                            controller: 'headerCtrl'

                        }
                    }, data:{
                        isLogin:
                            {
                                isLogin:false
                            }
                    }
                })

                .state('changePassword', {
                url: '/changePassword',
                views: {
                    'header@':{
                        templateUrl: 'views/header/header.html',
                        controller: 'headerCtrl'
                    },
                    'content@': {
                        templateUrl: 'views/forgetPassword/passwordSet.html',
                        controller: 'passwordSetCtrl'
                    },
                    'footer@': {
                        templateUrl: 'views/footer/footer.html',
                        controller:'footerCtrl'

                    }

                },resolve: {

                    authenticated: ['$q', 'shareData', function ($q, shareData) {
                        var deferred = $q.defer();
                        if (shareData.getCookies() == true) {

                            deferred.resolve(shareData.getUserProfile());
                        }
                        else {
                            console.log("else");

                            deferred.reject('Not logged in');

                        }

                        return deferred.promise;


                    }]

                }
            })
                .state('dashboard', {
                url: '/dashboard',
                    data:{
                        hover:false,
                        dashboardHover:true,
                        calendarHover:false,
                        isLogin:{isLogin:true,userType:false}
                    },
                views: {
                    'header@': {
                        templateUrl: 'views/header/loginHeader.html',
                        controller: 'loginHeaderCtrl'

                    },
                    'content@': {
                        templateUrl: 'views/dashboard/teacherDashboard.html',
                        controller: 'myteacherDashboard'
                    }
                    ,
                    'footer@':{
                        templateUrl:'views/footer/footer.html',
                        controller:'footerCtrl'
                    }


                }
                , resolve: {

                    authenticated: ['$q', 'shareData', function ($q, shareData) {
                        var deferred = $q.defer();
                        if (shareData.getCookies() == true) {

                            deferred.resolve(shareData.getUserProfile());
                        }
                        else {
                            console.log("else");

                            deferred.reject('Not logged in');

                        }

                        return deferred.promise;


                    }],
                        profile: ['$q','shareData','$http',function($q,shareData,$http){
                        var deferred=$q.defer();
                            $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                shareData.setUserProfile(data);
                                deferred.resolve(data);
                            }).error(function(data){
                                deferred.reject(data);
                            });
                            return deferred.promise;
                        }]
                }

            })
                .state('dashboard.editAccount', {
                    url: '/editAccount',
                    views: {
                        'content@': {
                            templateUrl: 'views/account/editAccount.html',
                            controller: 'editAccountCtrl'
                        }
                    }
                }).
                state('dashboard.calendar', {
                    url: '/calendar',
                    views: {
                        'content@': {
                            templateUrl: 'views/calendar/calendar.html',
                            controller: 'calendarCtrl'
                        }
                    }
                })
                
                .state('dashboard.notifications', {
                    url: '/notification',
                    views: {
                        'content@': {
                            templateUrl: 'views/notification/notification.html',
                            controller: 'notificationCtrl'
                        }
                    }
                })
                .state('dashboard.favourites', {
                    url: '/favourites',
                    views: {
                        'content@': {
                            templateUrl: 'views/favourites/favourites.html',
                            controller: 'favouritesCtrl'
                        }
                    }
                })
                .state('dashboard.myClasses', {
                    url: '/myClasses',
                    data:{
                        hover:true,
                        dashboardHover:false,
                        calendarHover:false
                    },
                    views: {
                        'content@': {
                            templateUrl: 'views/myClasses/myClasses.html',
                            controller: 'myClassesCtrl'
                        }
                    }
                })
                .state('dashboard.contact', {
                    url: '/contact',
                    views: {
                        'content@': {
                            templateUrl: 'views/contact/contact.html',
                            controller: 'contactCtrl'
                        }
                    }
                })
                .state('dashboard.terms&condition', {
                    url: '/terms',
                    views: {
                        'content@': {
                            templateUrl: 'views/uiComponents/terms_and_conditions.html'

                        }
                    }
                })
                .state('teacherDashboard', {
                url: '/teacherDashboard',
                    data:{
                        hover:false,
                        dashboardHover:true,
                        calendarHover:false,
                        isLogin:{isLogin:true,userType:true}
                    },
                views: {
                    'header@': {
                        templateUrl: 'views/header/teacherLoginHeader.html',
                        controller: 'teacherLoginHeaderCtrl'

                    },
                    'content@': {
                        templateUrl: 'views/dashboard/teacherDashboard.html',
                        controller: 'myteacherDashboard'
                    }
                    ,
                    'footer@':{
                        templateUrl:'views/footer/footer.html',
                        controller:'footerCtrl'
                    }

                }
                , resolve: {

                    authenticated: ['$q', 'shareData', function ($q, shareData) {
                        var deferred = $q.defer();
                        if (shareData.getCookies() == true) {

                            deferred.resolve(shareData.getUserProfile());
                        }
                        else {
                            console.log("else");

                            deferred.reject('Not logged in');

                        }

                        return deferred.promise;


                    }],
                        profile: ['$q','shareData','$http',function($q,shareData,$http){
                            var deferred=$q.defer();
                            $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                shareData.setUserProfile(data);
                                deferred.resolve(data);
                            }).error(function(data){
                                deferred.reject(data);
                            });
                            return deferred.promise;
                        }]
                }

            })
                .state('teacherDashboard.editAccount', {
                    url: '/editAccount',
                    views: {
                        'content@': {
                            templateUrl: 'views/account/editAccount.html',
                            controller: 'editAccountCtrl'
                        }
                    }
                })
                .
                state('teacherDashboard.calendar', {
                    url: '/calendar',
                    views: {
                        'content@': {
                            templateUrl: 'views/calendar/calendar.html',
                            controller: 'calendarCtrl'
                        }
                    },
                    data:{
                        hover:false,
                        dashboardHover:false,
                        calendarHover:true
                    }
                })
                .state('teacherDashboard.addClass', {
                    url: '/addClass',
                    views: {
                        'content@': {
                            templateUrl: 'views/teach/addClass.html',
                            controller: 'addClassCtrl'
                        }
                    }
                })
                .state('teacherDashboard.notifications', {
                    url: '/notification',
                    views: {
                        'content@': {
                            templateUrl: 'views/notification/notification.html',
                            controller: 'notificationCtrl'
                        }
                    }
                })
                .state('teacherDashboard.favourites', {
                    url: '/favourites',
                    views: {
                        'content@': {
                            templateUrl: 'views/favourites/favourites.html',
                            controller: 'favouritesCtrl'
                        }
                    }
                })
                .state('teacherDashboard.myClasses', {
                    url: '/myClasses',
                    data:{
                        hover:true,
                        dashboardHover:false,
                        calendarHover:false
                    },
                    views: {
                        'content@': {
                            templateUrl: 'views/myClasses/myClasses.html',
                            controller: 'myClassesCtrl'
                        }
                    }
                })
                .state('teacherDashboard.contact', {
                    url: '/contact',
                    views: {
                        'content@': {
                            templateUrl: 'views/contact/contact.html',
                            controller: 'contactCtrl'
                        }
                    }
                })
                .state('teacherDashboard.terms&condition', {
                    url: '/terms',
                    views: {
                        'content@': {
                            templateUrl: 'views/uiComponents/terms_and_conditions.html'

                        }
                    }
                })

                .state('editStudent', {
                    url: '/editStudent',
                    data:{
                        hover:false,
                        dashboardHover:false,
                        isLogin:{isLogin:true,userType:false}
                    },
                    views: {
                        'header@': {
                            templateUrl: 'views/header/loginHeader.html',
                            controller: 'loginHeaderCtrl'

                        },
                        'content@': {
                            templateUrl: 'views/learn/editStudent.html',
                            controller: 'editStudentCtrl'
                        }
                        ,
                        'footer@':{
                            templateUrl:'views/footer/footer.html',
                            controller:'footerCtrl'
                        }

                    }
                    , resolve: {

                        authenticated: ['$q', 'shareData', function ($q, shareData) {
                            var deferred = $q.defer();
                            if (shareData.getCookies() == true) {

                                deferred.resolve(shareData.getUserProfile());
                            }
                            else {
                                console.log("else");

                                deferred.reject('Not logged in');

                            }

                            return deferred.promise;


                        }],
                        profile: ['$q','shareData','$http',function($q,shareData,$http){
                            var deferred=$q.defer();
                            $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                shareData.setUserProfile(data);
                                deferred.resolve(data);
                            }).error(function(data){
                                deferred.reject(data);
                            });
                            return deferred.promise;
                        }]

                    }
                })
                .state('editTeacher', {
                    url: '/editTeacher',
                    data:{
                        hover:false,
                        dashboardHover:false,
                        isLogin:{isLogin:true,userType:true}
                    },
                    views: {
                        'header@': {
                            templateUrl: 'views/header/teacherLoginHeader.html',
                            controller: 'teacherLoginHeaderCtrl'

                        },
                        'content@': {
                            templateUrl: 'views/teach/editTeacher.html',
                            controller: 'editTeacherCtrl'
                        }
                        ,
                        'footer@':{
                            templateUrl:'views/footer/footer.html',
                            controller:'footerCtrl'
                        }

                    }
                    , resolve: {

                        authenticated: ['$q', 'shareData', function ($q, shareData) {
                            var deferred = $q.defer();
                            if (shareData.getCookies() == true) {

                                deferred.resolve(shareData.getUserProfile());
                            }
                            else {
                                console.log("else");

                                deferred.reject('Not logged in');

                            }

                            return deferred.promise;


                        }],
                        profile: ['$q','shareData','$http',function($q,shareData,$http){
                            var deferred=$q.defer();
                            $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                shareData.setUserProfile(data);
                                deferred.resolve(data);
                            }).error(function(data){
                                deferred.reject(data);
                            });
                            return deferred.promise;
                        }]


                    }
                }).state('startClass',{
                url:'/startClass/:id/:name',
                views:{
                    'content@':{
                        templateUrl:'views/classModule/templates/mainChatWindow.html',
                        controller:'MainCtrl'

                    }
                }
                , resolve: {

                    authenticated: ['$q', 'shareData', function ($q, shareData) {
                        var deferred = $q.defer();
                        if (shareData.getCookies() == true) {

                            deferred.resolve(shareData.getUserProfile());
                        }
                        else {
                            console.log("else");

                            deferred.reject('Not logged in');

                        }

                        return deferred.promise;


                    }]
                    ,
                profile: ['$q','shareData','$http',function($q,shareData,$http){
                    var deferred=$q.defer();
                    $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                        shareData.setUserProfile(data);
                        deferred.resolve(data);
                    }).error(function(data){
                        deferred.reject(data);
                    });
                    return deferred.promise;
                }]

                }
            })


                .state('classDetails', {
                    url: '^/classDetail/:id',

                    views:{
                        'header@': {
                            controller:function ($state,shareData,$http,$stateParams) {
                                console.log($stateParams);
                                if ($stateParams.id === '' || $stateParams.id === undefined || $stateParams.id == ' ')
                                    $state.go('check');
                                else {
                                    if (shareData.getCookies()) {
                                        if (shareData.getUserProfile().userType == false) {
                                            $state.go('classDetails.dashboard');
                                        }
                                        else {
                                            $state.go('classDetails.teacherDashboard');
                                        }

                                }
                                else
                                    $state.go('classDetails.home');
                            }
                            }

                        }




                    },
                    data:{
                        hover:false,
                        dashboardHover:false,
                        calendarHover:false
                    },

                })


                .state('classDetails.home', {
                    url: '/home',
                    views: {
                        'header@': {
                            templateUrl: 'views/header/header.html',
                            controller: 'headerCtrl'

                        }
                        ,
                        'content@': {
                            templateUrl: 'views/classData/classDetails.html',
                            controller: 'classDetailsCtrl'
                        }
                        ,
                        'footer@':{
                            templateUrl:'views/footer/footer.html',
                            controller:'footerCtrl'
                        }
                    }
                })

                .state('classDetails.dashboard', {
                    //url: '/student',
                    url: '',

                    views: {

                        'header@': {
                            templateUrl: 'views/header/loginHeader.html',
                            controller: 'loginHeaderCtrl'

                        }
                        ,
                        'content@': {
                            templateUrl: 'views/classData/classDetails.html',
                            controller: 'classDetailsCtrl'
                        }
                        ,
                        'footer@':{
                            templateUrl:'views/footer/footer.html',
                            controller:'footerCtrl'
                        }

                    }
                    ,resolve: {

                        authenticated: ['$q', 'shareData', function ($q, shareData) {
                            var deferred = $q.defer();
                            if (shareData.getCookies() == true) {

                                deferred.resolve(shareData.getUserProfile());
                            }
                            else {
                                console.log("else");

                                deferred.reject('Not logged in');

                            }

                            return deferred.promise;


                        }]
                        ,
                        profile: ['$q','shareData','$http',function($q,shareData,$http){
                            var deferred=$q.defer();
                            $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                shareData.setUserProfile(data);
                                deferred.resolve(data);
                            }).error(function(data){
                                deferred.reject(data);
                            });
                            return deferred.promise;
                        }]

                    }
                })


                .state('classDetails.teacherDashboard', {
                    //url: '/teacher',
                    url: '',

                    views: {

                        'header@': {
                            templateUrl: 'views/header/teacherLoginHeader.html',
                            controller: 'teacherLoginHeaderCtrl'

                        },
                        'content@': {
                            templateUrl: 'views/classData/classDetails.html',
                            controller: 'classDetailsCtrl'
                        }
                        ,
                        'footer@':{
                            templateUrl:'views/footer/footer.html',
                            controller:'footerCtrl'
                        }
                    }
                    ,resolve: {

                        authenticated: ['$q', 'shareData', function ($q, shareData) {
                            var deferred = $q.defer();
                            if (shareData.getCookies() == true) {

                                deferred.resolve(shareData.getUserProfile());
                            }
                            else {
                                console.log("else");

                                deferred.reject('Not logged in');

                            }

                            return deferred.promise;


                        }]
                        ,
                        profile: ['$q','shareData','$http',function($q,shareData,$http){
                            var deferred=$q.defer();
                            $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                shareData.setUserProfile(data);
                                deferred.resolve(data);
                            }).error(function(data){
                                deferred.reject(data);
                            });
                            return deferred.promise;
                        }]
                    }
                })
                .state('profile', {
                    //url: '/profile/:id',
                    url: '^/profile/:id',
                    views:{
                        'header@': {
                            controller:function ($state,shareData,$http,$stateParams) {
                           console.log("insidee app.js controller"+$stateParams.id);
                                if ($stateParams.id == ''|| $stateParams.id === undefined || $stateParams.id == ' '){
                                    console.log("inside check");
                                    $state.go('check');
                                }
                               else{
                                   if (shareData.getCookies()) {
                                       if (shareData.getUserProfile().userType == false) {
                                           $state.go('profile.dashboard');
                                       }
                                       else {
                                           $state.go('profile.teacherDashboard');
                                       }

                                }
                                else
                                    $state.go('profile.home');
                            }
                            }

                        }



                    },
                    data:{
                        hover:false,
                        dashboardHover:false,
                        calendarHover:false
                    }

                })
                .state('profile.dashboard',{
                    //url:'/student',
                    url:'',
                    views:{
                        'header@':{
                            templateUrl: 'views/header/loginHeader.html',
                            controller: 'loginHeaderCtrl'
                        }
                        ,
                        'content@':{
                            templateUrl: 'views/teach/teacherProfile.html',
                            controller: 'teacherProfileCtrl'
                        }
                        ,
                        'footer@':{
                            templateUrl:'views/footer/footer.html',
                            controller:'footerCtrl'
                        }
                    }
                    ,resolve: {

                        authenticated: ['$q', 'shareData', function ($q, shareData) {
                            var deferred = $q.defer();
                            if (shareData.getCookies() == true) {

                                deferred.resolve(shareData.getUserProfile());
                            }
                            else {
                                console.log("else");

                                deferred.reject('Not logged in');

                            }

                            return deferred.promise;


                        }]
                        ,
                        profile: ['$q','shareData','$http',function($q,shareData,$http){
                            var deferred=$q.defer();
                            $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                shareData.setUserProfile(data);
                                deferred.resolve(data);
                            }).error(function(data){
                                deferred.reject(data);
                            });
                            return deferred.promise;
                        }]
                    }

                })
                .state('profile.teacherDashboard',{
                    //url:'/teacher',
                    url:'',
                    views:{
                        'header@':{
                            templateUrl: 'views/header/teacherLoginHeader.html',
                            controller: 'teacherLoginHeaderCtrl'
                        }
                        ,
                        'content@':{
                            templateUrl: 'views/teach/teacherProfile.html',
                            controller: 'teacherProfileCtrl'
                        }
                        ,
                        'footer@':{
                            templateUrl:'views/footer/footer.html',
                            controller:'footerCtrl'
                        }
                    }
                    ,resolve: {

                        authenticated: ['$q', 'shareData', function ($q, shareData) {
                            var deferred = $q.defer();
                            if (shareData.getCookies() == true) {

                                deferred.resolve(shareData.getUserProfile());
                            }
                            else {
                                console.log("else");

                                deferred.reject('Not logged in');

                            }

                            return deferred.promise;


                        }]
                        ,
                        profile: ['$q','shareData','$http',function($q,shareData,$http){
                            var deferred=$q.defer();
                            $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                                shareData.setUserProfile(data);
                                deferred.resolve(data);
                            }).error(function(data){
                                deferred.reject(data);
                            });
                            return deferred.promise;
                        }]

                    }

                })
                .state('profile.home',{
                    url:'/home',
                    views:{
                        'header@':{
                            templateUrl: 'views/header/header.html',
                            controller: 'headerCtrl'
                        }
                        ,
                        'content@':{
                            templateUrl: 'views/teach/teacherProfile.html',
                            controller: 'teacherProfileCtrl'
                        }
                        ,
                        'footer@':{
                            templateUrl:'views/footer/footer.html',
                            controller:'footerCtrl'
                        }

                    }
                })



        }).run(function ($rootScope, $state, $http,$log,NgMap,$window,shareData,$window,$location,localStorageService) {
        $window.ga('create', 'UA-100961136-1', 'auto');
        $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams, error) {
            // console.log("this is to state"+ JSON.stringify(toState.name));
            // console.log("this is from state"+ JSON.stringify(fromState.name));
            // console.log("thi----------------------------------------------");
            if(shareData.getCookies() && shareData.getProfile()==undefined) {
                //console.log("inside cchange start");
                $http.post('/api/edit', {email: shareData.getUserProfile().email}).success(function (data) {
                    // console.log("this is email api data"+ JSON.stringify(data));
                    shareData.setUserProfile(data);
                    // console.log("this is share api data"+ JSON.stringify(data));
                });

            }
            if (fromState.name != '')
                localStorageService.set('previousState', fromState.name);
        });

        $rootScope.$on('$stateChangeError',
            function (event, toState, toParams, fromState, fromParams, error) {
                $log.debug(error);
                if(toState.name=='profile.teacherDashboard' || toState.name=='profile.dashboard'){
                    // console.log($window.location.host);
                   $window.location.href='profile/'+toParams.id+'/home';
                }
                if(toState.name=='classDetails.teacherDashboard' || toState.name=='classDetails.dashboard'){
                    // console.log($window.location.host);
                    $window.location.href='classDetail/'+toParams.id+'/home';
                }
                $state.go('home');
            });

        NgMap.getMap().then(function(map) {
            $rootScope.map = map;
        });

    });


}());
