(function () {
    angular.module('seekoApp.classDetails', ['ui.router', 'ui.bootstrap'])
        .controller('classDetailsCtrl', ['$sce','$scope', '$rootScope', 'shareData', '$http', '$stateParams', '$window', 'localStorageService', '$state', 'Notification','$exceptionHandler', '$uibModal', 'calendarHelper', '$location', '$anchorScroll','screenSize','$q',function ($sce,$scope, $rootScope, shareData, $http, $stateParams, $window, localStorageService, $state, Notification,$exceptionHandler, $uibModal, calendarHelper, $location, $anchorScroll,screenSize,$q) {

            $scope.startIndex=0;
            $scope.endIndex=10;
            screenSize.rules = {
                superJumbo: '(max-width: 640px)'

            };
            $scope.currentUserDetail=shareData.getUserProfile();
            if(screenSize.is('superJumbo')){
                var startTeacherIndex=0;
                var endTeacherIndex=3;

            }
            else {
                var startTeacherIndex = 0;
                var endTeacherIndex = 6;
            }
            if($stateParams.id==''){
                if(!shareData.getCookies())
                    $state.go('home');
                else {
                    if(shareData.getProfile().userType==true)
                        $state.go('teacherDashboard');
                    else
                        $state.go('dashboard');
                }
            }
            $scope.showspin=true;
            $scope.joinclButton = "Join Class";
            if(shareData.getCookies()){
                $scope.isLogin=true;
            }
            else {
                $scope.isLogin=false;
            }
            if($scope.isLogin){
                $scope.userName = shareData.getProfile().firstName;
                $scope.profilePic = shareData.getProfile().profilePic;
            }
            $scope.postReviewButton=false;
            $scope.disableButton = false;
            $scope.day = moment();


            $scope.start = "Start Class";
            $scope.classReview = {
                review: ''
            };
            $scope.credential = [];
            $scope.onPlayStop=true;
            $scope.class;
            $scope.starArray=[{like:false},{like:false},{like:false},{like:false},{like:false}];
            $scope.stars=$scope.starArray;
            var starCount=0;
            $scope.classYoutubeLink = 'https://www.youtube.com/watch?v=kw4tT7SCmaY';
            $scope.playerVars = {
                controls: 2
            };
            $scope.$on('youtube.player.buffering', function ($event, player) {
                // play it again
                //player.playVideo();
                $scope.onPlayStop=false;
            });
            function playPause() {
                myVideo.play();

            }

            $http.post('/api/getS3fsContent').success(function (data) {
                var recordingClassId=[];
                if (data.recordingUrl.length != '') {
                    $scope.buttonImg=true;
                    var clickBtn=$scope.buttonImg;
                    $scope.dataLength = data.recordingUrl.length;
                    for(var i=0; i<$scope.dataLength; i++) {
                        var item = data.recordingUrl[i].classId;
                        url = $sce.trustAsResourceUrl(data.recordingUrl[i].url);
                        var urlOne=url;
                         var requestJsonTwo = {
                            "type": "className",
                            "classId":data.recordingUrl[i].classId
                        };

                           var promise = abc(requestJsonTwo,recordingClassId,urlOne,clickBtn);
                            promise.then(function (success) {
                                if(success)
                                {
                                    $scope.recordingArray=recordingClassId.reverse();
                                    $scope.recordingClassData=$scope.recordingArray.slice(startTeacherIndex, endTeacherIndex);

                                }
                            })

                    }
                    // $scope.recordingClassData=recordingClassId;


                } else {
                    $scope.showNoRecording = true;
                }

            });

            $scope.viewByCategory= function (name,type) {
                var requestJson={"index":shareData.getId(),"name":name,"type":type};
                $http.post('/api/searchByClassIndex',requestJson).success(function(data){
                    $scope.classData = data.data.reverse();
                            localStorageService.set('stateReview', $scope.classData);
                         $state.go('viewall');
                     });
               }

            $scope.pauseOrPlay = function(ele,record,id){
                // console.log("this is video element"+JSON.stringify(ele));
                $scope.buttonImg=false;
                console.log("#video"+id);
                var video = angular.element("#video"+id);
                video[0].controls=true;
                console.log(video);
                video[0].play(); // video.play()
                record.showPlayButton=false;
            }

            $scope.attendClass = function (id,name) {

                if($scope.cancelClassStatus==true) {

                    $scope.classMessage = "This class has been cancelled";
                    // console.log($scope.classMessage);
                    var modalInstance = $uibModal.open({
                        templateUrl: '../../views/messageNotifications/confirmAttend.html',
                        scope: $scope

                    });
                    $scope.modalInstance = modalInstance;
                }
                    else {
                    $http.post('/api/getStartClassFlag',{id:id}).success(function(data){
                        if(data.success==false) {

                            $scope.classMessage = data.message;
                            // console.log($scope.classMessage);
                            var modalInstance = $uibModal.open({
                                templateUrl: '../../views/messageNotifications/confirmAttend.html',
                                scope: $scope

                            });
                            $scope.modalInstance = modalInstance;
                        }
                        else {
                            try {
                                $window.open('', '_blank').location.href = $window.location.origin + '/startClass/' + id + '/' + name;
                            }
                            catch(e){
                                console.log("inside catch");
                                $scope.classMessage = "Please allow pop-up for seeko to attend class. ";
                                // console.log($scope.classMessage);
                                var modalInstance = $uibModal.open({
                                    templateUrl: '../../views/messageNotifications/confirmAttend.html',
                                    scope: $scope

                                });
                                $scope.modalInstance = modalInstance;
                            }
                        }
                    });
                }

            }


            if (localStorageService.get('searchFrom') == "searchBox") {

                var result = localStorageService.get('searchString');
                var requestJson = {
                    // "index": shareData.getUserProfile().id,
                    "name": result.label,
                    "type": "className",
                    "classId": $stateParams.id
                };

                $http.post('/api/searchByClassIndex', requestJson).success(function (data) {
                    $scope.cancelClassStatus=data.data.data.classData.cancelClass;
                    if(data.success==false){

                        if(!shareData.getCookies())
                            $state.go('home');
                        else {
                            if(shareData.getProfile().userType==true)
                                $state.go('teacherDashboard');
                            else
                                $state.go('dashboard');
                        }
                    }

                    $scope.class = data;
                    if(shareData.getCookies()){

                    $http.post('/api/getIsJoinedStatus',{userId:shareData.getUserProfile().id,classId:data.data.data._id})
                        .success(function(joinedData){
                            console.log(joinedData);
                            if($scope.class.data.profile.userData.profile.teacherProfile.experience!='0/0' && $scope.class.data.profile.userData.profile.teacherProfile.experience!=undefined)
                                $scope.teacherExperience=$scope.class.data.profile.userData.profile.teacherProfile.experience.split('/')[0]+'.'+$scope.class.data.profile.userData.profile.teacherProfile.experience.split('/')[1];
                            else
                                $scope.teacherExperience='0/0';

                            if ((shareData.getProfile().userType == true) && (data.data.profile._id == shareData.getUserProfile().id)) {

                                $scope.startButton = true;
                            }
                            else {

                                if (joinedData.isAttend) {
                                    $scope.startButton = false;
                                    $scope.isAttend = true;
                                }
                                else {
                                    $scope.startButton = false;
                                    $scope.isAttend = false;
                                }
                            }
                            if (joinedData.isJoined == false)
                                $scope.JoinButton = "Sign Up";
                            else
                                $scope.isAttend = true;



                            if(joinedData.isFavourite ==false)
                                $scope.favouriteText = "Favorite";
                            else {
                                $scope.favouriteText = "Added as favourite";
                            }

                            $scope.reviewsCount = $scope.class.data.data.classData.reviews.reverse();
                            $scope.reviews=[];
                            if($scope.reviewsCount.length<=10){
                                for(i=0;i<$scope.reviewsCount.length;i++){
                                    $scope.reviews.push( $scope.reviewsCount[i]);
                                }
                            }
                            else{
                                for(i=0;i<10;i++){
                                    $scope.reviews.push( $scope.reviewsCount[i]);
                                }
                            }
                            

                            if ($scope.class.data.data.classData.classDocument.length > 0) {

                                for (i = 0; i < $scope.class.data.data.classData.classDocument.length; i++) {
                                    var credentialDetails = {
                                        classDocName: '',
                                        classDocUrl: ''
                                    };
                                    credentialDetails.classDocName = $scope.class.data.data.classData.classDocument[i].classDocName;
                                    credentialDetails.classDocUrl = $scope.class.data.data.classData.classDocument[i].classDocUrl;
                                    $scope.credential.push(credentialDetails);


                                }

                            }

                            if ($scope.class.data.data.classData.classImageVideo.length > 0) {

                                for (i = 0; i < $scope.class.data.data.classData.classImageVideo.length; i++) {
                                    var credentialDetails = {
                                        classDocName: '',
                                        classDocUrl: ''
                                    };
                                    credentialDetails.classDocName = $scope.class.data.data.classData.classImageVideo[i].classDocName;
                                    credentialDetails.classDocUrl = $scope.class.data.data.classData.classImageVideo[i].classDocUrl;
                                    $scope.credential.push(credentialDetails);
                                }


                            }





                            if (data.data.profile._id == shareData.getUserProfile().id) {
                                $scope.isMyClass = true;
                            }
                            else {
                                $scope.isMyClass = false;
                            }


                            $scope.calendar = {
                                year: new Date($scope.class.data.data.classData.fromDate).getFullYear(),
                                month: new Date($scope.class.data.data.classData.fromDate).getMonth(),
                                monthName: calendarHelper.getMonthName(new Date($scope.class.data.data.classData.fromDate).getMonth()),
                                isPreviousYear: false,
                                isPreviousMonth: false
                            };

                            $scope.days = getCalendarDays(new Date().getFullYear(), new Date().getMonth());
                            $scope.showspin=false;
                            localStorageService.set('documentUploaded',$scope.credential);

                    });
                    }
                    else{
                        $scope.startButton=false;
                        $scope.isAttend=false;
                        if($scope.class.data.profile.userData.profile.teacherProfile.experience!='0/0' && $scope.class.data.profile.userData.profile.teacherProfile.experience!=undefined)
                            $scope.teacherExperience=$scope.class.data.profile.userData.profile.teacherProfile.experience.split('/')[0]+'.'+$scope.class.data.profile.userData.profile.teacherProfile.experience.split('/')[1];
                        else
                            $scope.teacherExperience='0/0';

                            $scope.JoinButton = "Join Class";

                        $scope.reviews = $scope.class.data.data.classData.reviews.reverse();

                        if ($scope.class.data.data.classData.classDocument.length > 0) {

                            for (i = 0; i < $scope.class.data.data.classData.classDocument.length; i++) {
                                var credentialDetails = {
                                    classDocName: '',
                                    classDocUrl: ''
                                };
                                credentialDetails.classDocName = $scope.class.data.data.classData.classDocument[i].classDocName;
                                credentialDetails.classDocUrl = $scope.class.data.data.classData.classDocument[i].classDocUrl;
                                $scope.credential.push(credentialDetails);

                            }


                        }

                        if ($scope.class.data.data.classData.classImageVideo.length > 0) {

                            for (i = 0; i < $scope.class.data.data.classData.classImageVideo.length; i++) {
                                var credentialDetails = {
                                    classDocName: '',
                                    classDocUrl: ''
                                };
                                credentialDetails.classDocName = $scope.class.data.data.classData.classImageVideo[i].classDocName;
                                credentialDetails.classDocUrl = $scope.class.data.data.classData.classImageVideo[i].classDocUrl;
                                $scope.credential.push(credentialDetails);
                            }


                        }





                        if (data.data.profile._id == shareData.getUserProfile().id) {
                            $scope.isMyClass = true;
                        }
                        else {
                            $scope.isMyClass = false;
                        }


                        $scope.calendar = {
                            year: new Date($scope.class.data.data.classData.fromDate).getFullYear(),
                            month: new Date($scope.class.data.data.classData.fromDate).getMonth(),
                            monthName: calendarHelper.getMonthName(new Date($scope.class.data.data.classData.fromDate).getMonth()),
                            isPreviousYear: false,
                            isPreviousMonth: false
                        };

                        $scope.days = getCalendarDays(new Date().getFullYear(), new Date().getMonth());
                        $scope.showspin=false;
                    }



                });

            }
            else {
                $scope.class = localStorageService.get('class');

            }

            $scope.getCredential = function (value) {

                $window.open(value)
            }

            $scope.setFavourites = function () {
                $scope.message = "Would you like to add this class to your favorites list?";
                var modalInstance = $uibModal.open({
                    templateUrl: '../../views/messageNotifications/confirm.html',
                    scope: $scope,
                    backdrop: false,
                });
                $scope.modalInstance = modalInstance;
            }

            $scope.confirm = function () {

                $http.post("/api/createFavourites", {
                    id: shareData.getUserProfile().id,
                    classId: $scope.class.data.data._id,
                    teacherId: ''
                })
                    .success(function (data) {

                        $scope.modalInstance.close('ok');
                        $scope.favouriteText = "Added as favourite";
                        Notification.primary({message:"Added to your favorites list.",templateUrl: "../../views/messageNotifications/success.html"});
                    })
            };


            $scope.cancel = function () {
                $scope.modalInstance.close('ok');
            };

            $scope.joinClass = function (classValue) {
                if($scope.isLogin) {

                    var result = {
                        teacherName: classValue.data.profile.userData.profile.firstName,
                        classId: classValue.data.data._id,
                        studentId: shareData.getUserProfile().id,
                        teacherId: classValue.data.profile._id,
                        date: classValue.data.data.classData.fromDate,
                        className: classValue.data.data.classData.className
                    };

                    $http.post('/api/joinClass', result).success(function (data) {
                        $scope.isAttend = true;

                    });
                }
                else{
                    var modalInstance = $uibModal.open({
                        templateUrl: '../../views/login/login.html',
                        controller:'loginCtrl',
                        animation:true,
                        windowClass: 'modal modal-slide-in-right'

                    });
                }
            }

            $scope.getTeacherProfile = function (id) {
                $state.go('profile',{id:id});

            }
            $scope.startClass = function (id,name) {
                var currentDate=""+new Date();
                
              $http.post('/api/startClassApi',{id:id,cDate:currentDate}).success(function(data){
                    // if(data.success==false){
                    //     // console.log("class is not started yet");
                    //         console.log("inside catch");
                    //         $scope.classMessage = "Class is not started yet.";
                    //         // console.log($scope.classMessage);
                    //         var modalInstance = $uibModal.open({
                    //             templateUrl: '../../views/messageNotifications/confirmAttend.html',
                    //             scope: $scope
                    //
                    //         });
                    //         $scope.modalInstance = modalInstance;
                    //
                    //     // Notification.primary({message:"Class is not started yet...",templateUrl: "../../views/messageNotifications/error.html"});
                    //
                    //     // newWin.close();
                    // }else{
                        // console.log(newWin);

                        $window.open('').location.href=$window.location.origin+'/startClass/'+id+'/'+name;
                        $http.post('/api/tempStartClass',{id:id,flag:'start'}).success(function(data){
                            console.log(data);
                        });
                    // }
                });
            }


            $scope.postReview = function (classId, teacherId, className) {

                if($scope.classReview.review.length>0){
                    $scope.postReviewButton=true;

                $http.post('/api/reviews', {
                    "teacherId": teacherId,
                    "userId": shareData.getUserProfile().id,
                    "classId": classId,
                    "comment": $scope.classReview.review,
                    "date": new Date(),
                    "starCount": starCount,
                    "name": $scope.userName,
                    "profilePic": shareData.getProfile().profilePic,
                    "className": className,
                    "userType": shareData.getProfile().userType
                })
                    .success(function (data) {
                                                 var count;
                        for (i = 0; i < $scope.reviews.length; i++) {
                            if ($scope.reviews[i].userId == shareData.getUserProfile().id) {
                                $scope.reviews[i].count = data.count;
                            }
                        }
                        var review = {
                            starCount: starCount,
                            comment: $scope.classReview.review,
                            date: new Date(),
                            profilePic: shareData.getProfile().profilePic,
                            userName: shareData.getProfile().firstName,
                            count: data.count,
                            userId: shareData.getUserProfile().id

                        }
                        $scope.class.data.data.classData.avgStarCount=data.avgStarCount;
                        $scope.classReview.review = "";
                        $scope.starArray = [{like: false}, {like: false}, {like: false}, {like: false}, {like: false}];
                        $scope.stars = $scope.starArray;
                        $scope.reviews.splice(0, 0, review);
                        starCount = 0;
                        $scope.postReviewButton=false
                        $scope.reviewsCount.length=$scope.reviewsCount.length+1;

                    });
            }
            }



            $scope.getClassTime = function (value) {
                var time;
                var startTime;
                var stopTime;

                var beginTime = value.split(':')[0];
                var endTime = value.split(':')[1].split('-')[1];


                if (parseInt(beginTime) <= 11) {

                    if (parseInt(beginTime) == 0) {
                        startTime = 12 + ":" + value.split(':')[1].split('-')[0] + "AM to ";
                        if (parseInt(endTime) <= 11) {
                            stopTime = parseInt(endTime) + ":" + value.split(':')[2] + "AM";
                        }
                        else {
                            if (parseInt(endTime) - 12 == 0 || parseInt(endTime) - 12 == -12)
                                stopTime = 12 + ":" + value.split(':')[2] + "PM";
                            else
                                stopTime = parseInt(endTime) - 12 + ":" + value.split(':')[2] + "PM";
                        }

                    }
                    else {

                        startTime = parseInt(beginTime) + ":" + value.split(':')[1].split('-')[0] + "AM to ";
                        if (parseInt(endTime) <= 11) {
                            stopTime = parseInt(endTime) + ":" + value.split(':')[2] + "AM";
                        }
                        else {
                            if (parseInt(endTime) - 12 == 0 || parseInt(endTime) - 12 == -12)
                                stopTime = 12 + ":" + value.split(':')[2] + "AM";
                            else
                                stopTime = parseInt(endTime) - 12 + ":" + value.split(':')[2] + "PM";
                        }
                    }

                    time = startTime + stopTime;
                }
                else {

                    if (parseInt(beginTime) - 12 == 0) {
                        startTime = 12 + ":" + value.split(':')[1].split('-')[0] + "PM to ";
                        if (parseInt(endTime) <= 11) {
                            if (parseInt(endTime) - 12 == 0 || parseInt(endTime) - 12 == -12)
                                stopTime = 12 + ":" + value.split(':')[2] + "AM";
                            else
                                stopTime = parseInt(endTime) + ":" + value.split(':')[2] + "AM";
                        }
                        else {
                            stopTime = parseInt(endTime) - 12 + ":" + value.split(':')[2] + "PM";
                        }

                    }
                    else {
                        startTime = parseInt(beginTime) - 12 + ":" + value.split(':')[1].split('-')[0] + "PM to ";
                        if (parseInt(endTime) <= 11) {
                            if (parseInt(endTime) - 12 == 0 || parseInt(endTime) - 12 == -12)
                                stopTime = 12 + ":" + value.split(':')[2] + "AM";
                            else
                                stopTime = parseInt(endTime) + ":" + value.split(':')[2] + "AM";
                        }
                        else {

                            stopTime = parseInt(endTime) - 12 + ":" + value.split(':')[2] + "PM";
                        }
                    }

                    time = startTime + stopTime;
                }

                return time;
            }

            // calendar integration


            $scope.nextMonth = function () {

                calendarHelper.incrementCalendarMonth($scope.calendar);
                $scope.calendar.monthName = calendarHelper.getMonthName($scope.calendar.month);
                $scope.days = getCalendarDays($scope.calendar.year, $scope.calendar.month);

                if ((parseInt($scope.calendar.month) < parseInt(new Date().getMonth())) && parseInt($scope.calendar.year) == parseInt(new Date().getFullYear() + 1)) {

                    $scope.calendar.isPreviousYear = false;
                }
                else if ((parseInt($scope.calendar.month) >= parseInt(new Date().getMonth()))) {

                    $scope.calendar.isPreviousYear = true;
                }

            }

            $scope.previousMonth = function () {
                if (parseInt($scope.calendar.year) == parseInt(new Date().getFullYear())) {
                    if (parseInt($scope.calendar.month) == parseInt(new Date().getMonth() + 1)) {
                        $scope.calendar.isPreviousYear = false;
                        $scope.calendar.isPreviousMonth = false;
                    }
                }
                if ((parseInt($scope.calendar.month) < parseInt(new Date().getMonth() + 1)) && parseInt($scope.calendar.year) == parseInt(new Date().getFullYear())) {

                    $scope.calendar.isPreviousYear = false;
                }
                else if ((parseInt($scope.calendar.month) >= parseInt(new Date().getMonth() + 1)) && parseInt($scope.calendar.year) == parseInt(new Date().getFullYear() + 1)) {
                    $scope.calendar.isPreviousYear = true;
                }
                calendarHelper.decrementCalendarMonth($scope.calendar);
                $scope.calendar.monthName = calendarHelper.getMonthName($scope.calendar.month);
                $scope.days = getCalendarDays($scope.calendar.year, $scope.calendar.month);

            }


            $scope.getPreviousDate = function (day) {
                if (parseInt(new Date().getFullYear()) == parseInt($scope.calendar.year)) {
                    if (parseInt(new Date().getMonth() + 1) == parseInt($scope.calendar.month + 1)) {
                        if (parseInt(day) > parseInt(new Date().getDate())) {

                            return false;
                        }
                        else if (parseInt(day) < parseInt(new Date().getDate())) {

                            return true;
                        }
                    }
                }

            }


            function getCalendarDays(year, month) {

                var monthStartDate = new Date(year, month, 1);

                var days = [];
                var count = 0;
                var totalCell = 42;
                var cellCount = 1;
                var previousTotalMonthDays = 0;


                for (var idx = 1; idx <= new Date(year, month, 0).getDate(); idx++) {

                    previousTotalMonthDays++;
                }

                for (var idx = 0; idx < monthStartDate.getDay(); idx++) {
                    count++;
                }

                if (count > 0) {
                    var previousMonthDays = previousTotalMonthDays - (count - 1);


                    for (var idx = 0; idx < count; idx++) {
                        var dayJson = {};
                        dayJson.day = previousTotalMonthDays - (count - 1);
                        dayJson.isDisabled = true;
                        dayJson.isSelected = false;
                        days.push(dayJson);
                        previousTotalMonthDays++;
                        totalCell--;
                        cellCount++;

                    }

                }
                for (var idx = 1; idx <= new Date(year, month + 1, 0).getDate(); idx++) {
                    var dayJson = {};
                    if (new Date($scope.class.data.data.classData.fromDate).getDate() == idx && new Date($scope.class.data.data.classData.fromDate).getMonth() == $scope.calendar.month && new Date($scope.class.data.data.classData.fromDate).getFullYear() == $scope.calendar.year) {
                        dayJson.isSelected = true;
                        dayJson.summary = $scope.class.data.data.classData.summary;
                    }

                    dayJson.day = idx;
                    dayJson.isDisabled = false;

                    days.push(dayJson);
                    totalCell--;
                    cellCount++;
                }

                for (var idx = 1; idx <= totalCell; idx++) {
                    var dayJson = {};
                    dayJson.day = idx;
                    dayJson.isDisabled = true;

                    days.push(dayJson);
                    cellCount++;

                }



                return days;


            }

            $scope.getUserReviews = function (review) {

                (function (review) {

                    $http.post('/api/getUserReviews', {id: review.userId}).success(function (data) {
                        count = data.count;
                        review.count = count;
                    });

                })(review);

            }
            $scope.getStarCount=function(count){

                starCount=0;
                for(i=0;i<count+1;i++){
                    $scope.starArray[i].like=true;
                    starCount++;
                }
                for(i=count+1;i<6;i++)
                    $scope.starArray[i].like=false;

                $scope.stars=$scope.starArray;

            }

            $scope.getReviewedUser=function(id,userType){
                $state.go('profile',{id:id});
            }


            $scope.selectInvite = function () {
                $http.post('/api/invite',shareData).success(function (data) {

                })

            }

            $scope.loadMore = function () {
                console.log('hi i am load more function');
                $scope.startIndex=0;
                // console.log("this is $scope.startIndex value"+$scope.startIndex);
                $scope.endIndex=$scope.endIndex+5;
                var index = {
                    startIndex:$scope.startIndex ,
                    endIndex: $scope.endIndex,
                    classId: $stateParams.id
                };
                $http.post('/api/loadMore',index).success(function (data) {
                    $scope.reviews=data.classData;
                    // $scope.reviewsCount=
                    // console.log("this is remaining data"+ JSON.stringify(data));
                });
            }

            $scope.getNextClasses=function()
            {
                    console.log("isnide trending");
                    if((($scope.recordingArray.length>endTeacherIndex) && ($scope.recordingArray.length>5)) ||  (($scope.recordingArray.length>endTeacherIndex) && ($scope.recordingArray.length>2))) {
                        if(screenSize.is('superJumbo')){
                            startTeacherIndex += 3;
                            endTeacherIndex += 3;
                        }
                        else{
                            startTeacherIndex += 6;
                            endTeacherIndex += 6;
                        }

                        $scope.recordingClassData=$scope.recordingArray.slice(startTeacherIndex, endTeacherIndex);
                    }


            }

            $scope.getPrevClasses=function(type) {
                    if (startTeacherIndex > 0) {
                        if(screenSize.is('superJumbo')){
                            startTeacherIndex -= 3;
                            endTeacherIndex -= 3;
                        }
                        else{
                            startTeacherIndex -= 6;
                            endTeacherIndex -= 6;
                        }

                        $scope.recordingClassData = $scope.recordingArray.slice(startTeacherIndex, endTeacherIndex);
                    }
            }

            function abc(requestJsonTwo ,recordingClassId,urlOne,clickBtn) {
                return $q(function (resolve,reject) {
                    (function (requestJsonTwo) {
                        $http.post('/api/searchByClassIndex',requestJsonTwo).success(function (dataRes) {

                            var recordResult={
                                classDate:dataRes.data.data.classData.summary,
                                classTeachName:dataRes.data.data.classData.teacherName,
                                attendingClass:dataRes.data.data.classData.studentId.length,
                                starCount:dataRes.data.data.classData.avgStarCount,
                                classImgUrl:dataRes.data.data.classData.classImageVideo[0].classDocUrl,
                                btnClick:clickBtn,
                                showPlayButton:true,
                                urlLink:urlOne

                            }

                            recordingClassId.push(recordResult);
                            resolve(true);

                        });

                    })(requestJsonTwo);
                })



            }

        }])

}());
