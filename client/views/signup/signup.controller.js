(function(){
angular.module('seekoApp.signup', [])
    .controller('signupCtrl', ['$scope','$rootScope','$uibModalInstance','facebookService','gmailService','$window','$cookies','$http','Alertify','base64','$state','shareData','deviceDetector','$filter','Notification','localStorageService','$timeout','$document',function($scope,$rootScope,$uibModalInstance,facebookService,gmailService,$window,$cookies,$http,Alertify,base64,$state,shareData,deviceDetector,$filter,Notification,localStorageService,$timeout,$document) {

        
        this.signUpClass=false;
        this.userProfile="";
            this.closeSignUp=function(){
                $uibModalInstance.dismiss('ok');

            };
        var currentType={
            type:'all'
        }
        localStorageService.set('trendingStatus',currentType);
        $scope.onChange=function () {
            $scope.errorMsg=false;
        }
        $scope.$on('event:google-plus-signin-success', function(event, authResult) {
            $scope.googleToken=authResult.access_token;
            gapi.client.load('plus', 'v1', apiClientLoaded)
        });
        $scope.$on('event:google-plus-signin-failure', function(event, error) {

        });

        function apiClientLoaded() {
            gapi.client.plus.people.get({userId: 'me'}).execute(handleResponse);
        }

        function handleResponse(resp) {
            var imageUrl=resp.image.url.replace("?sz=50", "?sz=250");
            console.log(imageUrl);
            loginCredentials={
                'firstName':resp.name.givenName,
                'lastName':resp.name.familyName,
                'name':resp.displayName,
                'email':resp.emails[0].value,
                'id':resp.id,
                'picture':imageUrl,
                'token':$scope.googleToken,
                'userType':$rootScope.teacher
            };
            console.log(loginCredentials);
            $http.post('/api/googleLogin',loginCredentials)
                .success(function(data){
                    localStorageService.set('visitType','login');
                    localStorageService.set('loginFromSeeko','');
                    $uibModalInstance.dismiss('ok');
                    if(data.userType==false)
                        $state.go('dashboard');
                    else
                        $state.go('teacherDashboard');
                });
        }

        //facebook signup function
        this.fbSignUp=function () {

            $uibModalInstance.dismiss('ok');
            var loginCredentials='';

            if (typeof(FB) == 'undefined'
                && FB == null ) {
                Notification.primary({message:"Please try to connect internet...",templateUrl: "../../views/messageNotifications/error.html"});
                return ;
            } else {
                FB.getLoginStatus(function(Fbresponse) {
                    if(Fbresponse.status==='connected'){
                        console.log("inside if");
                        FB.api('/me','get', {fields: 'id,name,first_name,last_name,gender,email,picture.height(250).width(250)' } , function(response)
                        {
                            console.log("facebook respone is"+JSON.stringify(response));
                            this.loginCredentials={
                                'name':response.name,
                                'firstName':response.first_name,
                                'lastName':response.last_name,
                                'picture':response.picture,
                                'email':response.email,
                                'id':response.id,
                                'token':Fbresponse.authResponse.access_token,
                                'userType':$rootScope.teacher


                            };


                            $http.post('/api/fbLogin',this.loginCredentials)
                                .success(function (data) {
                                   localStorageService.set('loginFromSeeko','');
                                    localStorageService.set('visitType','login');
                                    if(data.userType==false){

                                        $state.go('dashboard');
                                    }
                                    else{

                                        $state.go('teacherDashboard');
                                    }


                                });

                        });
                    }
                    else{
                        console.log("inide else");
                        FB.login(function(response) {
                            if (response.authResponse)
                            {
                                var access_token =   FB.getAuthResponse()['accessToken'];

                                FB.api('/me','get', {fields: 'id,name,first_name,last_name,gender,email,picture.height(250).width(250)' } , function(response)
                                {
                                    console.log("facebook respone is"+JSON.stringify(response));
                                    this.loginCredentials={
                                        'name':response.name,
                                        'firstName':response.first_name,
                                        'lastName':response.last_name,
                                        'picture':response.picture,
                                        'email':response.email,
                                        'id':response.id,
                                        'token':access_token,
                                        'userType':$rootScope.teacher


                                    };


                                    $http.post('/api/fbLogin',this.loginCredentials)
                                        .success(function (data) {
                                            localStorageService.set('visitType','login');
                                             localStorageService.set('loginFromSeeko','');                                           
                                            if(data.userType==false){

                                                $state.go('dashboard');
                                            }
                                            else{

                                                $state.go('teacherDashboard');
                                            }


                                        });

                                });



                            } else {
                                console.log('User cancelled login or did not fully authorize.');
                            }
                        },{scope: 'public_profile,email'});
                    }
                });

            }


        };

        //simple user signup
        this.userSignUp=function(){
            $scope.buttonLoader=true;
            console.log(this.signUp);
            this.userData={
                'emailId':$filter('lowercase')(this.signUp.emailId),
                'password':this.signUp.password,
                'userType':$rootScope.teacher,
                'userName':this.signUp.name
            };



            $http.post('/api/userSignUp',this.userData)
                .success(function (data) {

                    if(!data.message){
                      console.log("success");
                        $uibModalInstance.dismiss('ok');
                        $scope.buttonLoader=false;
                        localStorageService.set('loginFromSeeko','loginFromSeeko');
                        Notification.primary({message:"Please check your email for complete verification",templateUrl: "../../views/messageNotifications/success.html"});
                    }
                    else{
                        $timeout(function () { $scope.buttonLoader=true});
                        $scope.errorMsg=true;
                        // Notification.primary({message:"Email already exists",templateUrl: "../../views/messageNotifications/error.html"});

                    }
                });
        };

    }]);
}());


